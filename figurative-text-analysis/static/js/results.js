/**
 * Created by maria on 4/26/15.
 */

function getResults(){
    var options;
    //var csrftoken = getCookie('csrftoken');
    var classifier = $( "#cls option:selected" ).text();
    var discretization = $( "#discr option:selected" ).text();
    var sel_fea = $( "#fea option:selected");
    var sel_morph = $( "#morph option:selected");
    var sel_fig = $( "#fig option:selected");
    var sel_sim = $( "#sim option:selected");
    var sel_corpus = $( "#corpus option:selected").text();
    var selected_features = [];
    var spinner = '<div class="spinner_container" id="spinner_box"><i class="fa fa-terminal fa-2x">Processing...</i>' +
                    '<i class="fa fa-refresh fa-2x fa-spin spinner"></i></div>';

    $('body').prepend(spinner);

    $.each(sel_fea, function(k, v){
       selected_features.push($(v).text());
    });
    $.each(sel_morph, function(k, v){
       selected_features.push($(v).text());
    });
    $.each(sel_fig, function(k, v){
       selected_features.push($(v).text());
    });
    $.each(sel_sim, function(k, v){
       selected_features.push($(v).text());
    });

    options = {
        "classifier": classifier,
        "discretization": discretization,
        "selected_features": selected_features,
        "corpus": sel_corpus
    };

    var data = JSON.stringify(options);

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });

    $.ajax({url: "../dashboard/get_results",
        method: "POST",
        async: true,
        data: options,
        "beforeSend": function(xhr, settings) {
            $.ajaxSettings.beforeSend(xhr, settings);
        },
        success: function(result){
            $('#trial_results').html(result);
            $("#spinner_box").remove();
    },
    error: function(xhr,status,error){
        showalert(error, "danger");
        $("#spinner_box").remove();
    }
    });
};

//todo: create ajaxsetup.js
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}