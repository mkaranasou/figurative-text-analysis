/**
 * Created by maria on 1/11/15.
 */
$(document).ready(function(){
    var menus = $('#menus');
    var navbar = $('.navbar');
    var navbar_top = $('#navbar_top');
    var sidebar = $('.sidebar');
    var status_menu = $('#status_menu');
    var navbar_brand = $('.navbar-brand');
    var error_message = $('#error_message');
    var settings_right = $('#settings_right');
    var diagrams_width = $("#diagrams_container").css("width").replace("px","")-100;

    // BASICS
    error_message.css('width', '100%');
    error_message.css('left', '10%');
    error_message.hide();

    // NAV BAR TOP
    navbar.css('background-color', '#2980b9');
    navbar.css('box-shadow', '0px 0px 10px grey');
    navbar_top.css('opacity', '0.8');

    navbar_top.find('a').css('color', 'whitesmoke');

    //navbar_top.find('li').mouseover(function(e){
    //    $(this).css('height', '60px');
    //    $(this).css('border-radius', '5%');
    //    $(this).css('box-shadow', '0px 0px 20px grey');
    //    $(e.target).css('color', 'turquoise');
    //});
    //navbar_top.find('li').mouseout(function(e){
    //    $(this).css('height', '50px');
    //    $(this).css('background-color', 'rgb(66, 139, 202)');
    //    $(this).css('box-shadow', '0px 0px 0px grey');
    //    //$(e.target).css('color', '#9d9d9d');
    //    $(e.target).css('color', 'whitesmoke');
    //});
    navbar_top.mouseover(function(e){
        navbar.css('background-color', '#428bca');
        navbar.find('li').css('background-color', '#428bca');
        navbar_top.css('height', '50px');
        navbar_top.css('opacity', '1.0');
        navbar.css('box-shadow', '0px 0px 20px black');
    });
    navbar_top.mouseout(function(e) {
        navbar.css('background-color', '#2980b9');
        navbar.find('li').css('background-color', '#2980b9');
        navbar_top.css('height', '25px');
        navbar_top.css('opacity', '0.8');
        navbar.css('box-shadow', '0px 0px 10px grey');
    });

    navbar_brand.css('color', 'white');
    navbar_brand.css('width', '350px');

    //menus.css('width', '145px');

    //menus.css('background-color', 'transparent');
    menus.mouseover(function(e){
        menus.css('color', 'whitesmoke');
    });
    menus.mouseout(function(e){
        menus.css('color', '#428bca');
    });

    sidebar.css('background-color', 'whitesmoke');
    sidebar.css('top', '50px');
    status_menu.find('li').on('click', function(e){
        $('#dashboard_title').text(($(e.target).text()));
        showalert('testing', 'info', 5000);
    });

    $(".chosen-select").chosen({max_selected_options: 5});

    if(window.location.hash) {
            // Fragment exists
            changeMenuItem($(window.location.hash));

        } else {
          // Fragment doesn't exist
        }
    //d3Diagram(diagrams_width);
    //d3Test(diagrams_width)
});

function d3Diagram(diagrams_width) {


			//Width and height
			var w = diagrams_width;
			var h = 500;

			var dataset = [ 5, 10, 13, 19, 21, 25, 22, 18, 15, 13, 77, 81, 65, 60, 59,
							11, 12, 15, 20, 18, 17, 16, 18, 23, 25, 7, 70, 80, 12, 23, 55,
                            23, 25, 7, 70, 80, 12, 23, 55  ];

			var xScale = d3.scale.ordinal()
							.domain(d3.range(dataset.length))
							.rangeRoundBands([0, w], 0.05);

			var yScale = d3.scale.linear()
							.domain([0, d3.max(dataset)])
							.range([0, h]);

			//Create SVG element
			var svg = d3.select("#diagrams_container")
						.append("svg")
						.attr("width", w)
						.attr("height", h);

			//Create bars
			svg.selectAll("rect")
			   .data(dataset)
			   .enter()
			   .append("rect")
			   .attr("x", function(d, i) {
			   		return xScale(i);
			   })
			   .attr("y", function(d) {
			   		return h - yScale(d);
			   })
			   .attr("width", xScale.rangeBand())
			   .attr("height", function(d) {
			   		return yScale(d);
			   })
			   .attr("fill", function(d) {
					return "rgb(0, 0, " + (d * 10) + ")";
			   })
			   .on("mouseover", function(d) {

					//Get this bar's x/y values, then augment for the tooltip
					var xPosition = parseFloat(d3.select(this).attr("x")) + xScale.rangeBand() / 2;
					var yPosition = parseFloat(d3.select(this).attr("y")) + 14;

					//Create the tooltip label
					svg.append("text")
					   .attr("id", "tooltip")
					   .attr("x", xPosition)
					   .attr("y", yPosition)
					   .attr("text-anchor", "middle")
					   .attr("font-family", "sans-serif")
					   .attr("font-size", "11px")
					   .attr("font-weight", "bold")
					   .attr("fill", "black")
					   .text(d);

			   })
			   .on("mouseout", function() {

					//Remove the tooltip
					d3.select("#tooltip").remove();

			   })
			   .on("click", function() {
			   		sortBars();
			   });

			//Define sort order flag
			var sortOrder = false;

			//Define sort function
			var sortBars = function() {

				//Flip value of sortOrder
			   	sortOrder = !sortOrder;

				svg.selectAll("rect")
				   .sort(function(a, b) {
				   		if (sortOrder) {
					   		return d3.ascending(a, b);
				   		} else {
					   		return d3.descending(a, b);
				   		}
				   	})
				   .transition()
				   .delay(function(d, i) {
					   return i * 50;
				   })
				   .duration(1000)
				   .attr("x", function(d, i) {
				   		return xScale(i);
				   });

			};
}

function d3Test(diagrams_width){
    var n = 40,
        random = d3.random.normal(0, .2),
        data = d3.range(n).map(random);

    var margin = {top: 20, right: 20, bottom: 20, left: 40},
        width = diagrams_width,//960 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

    var x = d3.scale.linear()
        .domain([0, n - 1])
        .range([0, width]);

    var y = d3.scale.linear()
        .domain([-1, 1])
        .range([height, 0]);

    var line = d3.svg.line()
        .x(function(d, i) { return x(i); })
        .y(function(d, i) { return y(d); });

    var svg = d3.select("#diagrams_container").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg.append("defs").append("clipPath")
        .attr("id", "clip")
      .append("rect")
        .attr("width", width)
        .attr("height", height);

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + y(0) + ")")
        .call(d3.svg.axis().scale(x).orient("bottom"));

    svg.append("g")
        .attr("class", "y axis")
        .call(d3.svg.axis().scale(y).orient("left"));

    var path = svg.append("g")
        .attr("clip-path", "url(#clip)")
      .append("path")
        .datum(data)
        .attr("class", "line")
        .attr("d", line);

    tick();

    function tick() {

      // push a new data point onto the back
      data.push(random());

      // redraw the line, and slide it to the left
      path
          .attr("d", line)
          .attr("transform", null)
        .transition()
          .duration(500)
          .ease("linear")
          .attr("transform", "translate(" + x(-1) + ",0)")
          .each("end", tick);

      // pop the old data point off the front
      data.shift();

    }
}

function changeMenuItem(e){
    var $trg = $(e);
    var $side_menu = $("#master_side_menu").css("display") == "block"
                                                ? $("#master_side_menu")
                                                : $("#master_side_menu_short");

    var spinner = '<div class="spinner_container" id="spinner_box"><i class="fa fa-refresh fa-2x fa-spin spinner"></i></div>';

    $('body').prepend(spinner);
    $.each($side_menu.find('li'), function(k,v){
       $(v).removeClass("active");
    });

    $trg.addClass("active");

    $.ajax({url: "../dashboard/"+ $trg.attr("id"),
        async: true,
        success: function(result){
        $("#content").html(result);
    },
    error: function(xhr,status,error){
       showalert(error, "danger");
    }
    });

    $("#spinner_box").remove();
}

function toggelSideMenu(el){

    console.log(el)
    console.log($(el).hasClass("fa-2x"))
    var $side_menu = $("#menus");
    var $master_side_menu =  $("#master_side_menu");
    var $master_side_menu_short =  $("#master_side_menu_short");
    var $content =  $("#content");

    if($(el).hasClass("fa-2x")) {
        $side_menu.css("width", "140px");
        $master_side_menu.css("display", "block");
        $master_side_menu_short.css("display", "none");
        $content.removeClass("col-lg-11");
        $content.addClass("col-lg-10");
        $content.css("left", "15%");

    }
    else {

        $side_menu.css("width", "40px");
        $master_side_menu.css("display", "none");
        $master_side_menu_short.css("display", "block");
        $content.removeClass("col-lg-10");
        $content.addClass("col-lg-11");
        $content.css("left", "6%");

    }



}

function showalert(message,alerttype, timeOut) {
    
    $('#alert_placeholder').append('<div id="alertdiv" class="alert alert-' +  alerttype + '"><a class="close" data-dismiss="alert" onclick="$(\'#alertdiv\').remove();">X</a><span>'+message+'</span></div>')

    setTimeout(function() { // this will automatically close the alert and remove this if the users doesnt close it in timeOut secs
    $("#alertdiv").remove();

    }, timeOut==undefined?5000:timeOut);
};