from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ui.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'FigurativeTextAnalysisUI.views.home', name='home'),
    url(r'^login/$', 'FigurativeTextAnalysisUI.views.login', name='login'),
    url(r'^register/$', 'FigurativeTextAnalysisUI.views.register', name='register'),
    url(r'^about/$', 'FigurativeTextAnalysisUI.views.about', name='about'),
    url(r'^dashboard/$', 'FigurativeTextAnalysisUI.views.dashboard', name='dashboard'),
    url(r'^dashboard/task_info$', 'FigurativeTextAnalysisUI.views.task_info', name='task_info'),
    url(r'^dashboard/data_sets$', 'FigurativeTextAnalysisUI.views.data_sets', name='data_sets'),
    url(r'^dashboard/results$', 'FigurativeTextAnalysisUI.views.results', name='results'),
    url(r'^dashboard/trial$', 'FigurativeTextAnalysisUI.views.trial', name='trial'),
    url(r'^dashboard/start_trial$', 'FigurativeTextAnalysisUI.views.start_trial', name='start_trial'),
    url(r'^dashboard/get_results', 'FigurativeTextAnalysisUI.views.get_results', name='get_results'),
    url(r'^dashboard/tweet_utils', 'FigurativeTextAnalysisUI.views.tweet_utils', name='tweet_utils'),
    # url(r'^dashboard/send_endpoint', 'FigurativeTextAnalysisUI.views.send_endpoint', name='send_endpoint'),
    url(r'^dashboard/long_polling_endpoint', 'FigurativeTextAnalysisUI.views.long_polling_endpoint', name='long_polling_endpoint'),
)
