import copy
import json
import traceback

import datetime
import datetime
import redis
from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from FigurativeTextAnalysis.models.Config import TrialConfig
from models import TweetTestData, TweetFinalTestData, CommonResponse
from FigurativeTextAnalysis.helpers.globals import g
from FigurativeTextAnalysis.models.Trial import Trial
from django.views.decorators.csrf import ensure_csrf_cookie

# Create your views here.


cls_proper_names = {
    g.CLASSIFIER_TYPE.NBayes: "Naive Bayes",
    g.CLASSIFIER_TYPE.SVMStandalone: "Linear SVM",
    g.CLASSIFIER_TYPE.DecisionTree: "Decision Tree",
    g.CLASSIFIER_TYPE.SVM: "SVM",
    g.CLASSIFIER_TYPE.SVR: "SVR",
    g.CLASSIFIER_TYPE.NuSVR: "NuSVR",
    g.CLASSIFIER_TYPE.RandomForestRegressor: "Random Forest Regressor",
    g.CLASSIFIER_TYPE.SGD: "SGD - Stochastic Gradient Descend",
    g.CLASSIFIER_TYPE.SVC: "SVC",
    g.CLASSIFIER_TYPE.Perceptron: "Perceptron",
}
vec_proper_names = {
    g.VECTORIZER.Dict: "DictVectorizer",
    g.VECTORIZER.Count: "Count Vectorizer (BoW)",
}
vec_types = [g.VECTORIZER.Dict, g.VECTORIZER.Count]

cls_proper_names_list = [
             cls_proper_names[g.CLASSIFIER_TYPE.NBayes],
             cls_proper_names[g.CLASSIFIER_TYPE.SVM],
             cls_proper_names[g.CLASSIFIER_TYPE.DecisionTree],
             cls_proper_names[g.CLASSIFIER_TYPE.SVMStandalone],
             cls_proper_names[g.CLASSIFIER_TYPE.SVR],
             cls_proper_names[g.CLASSIFIER_TYPE.NuSVR],
             cls_proper_names[g.CLASSIFIER_TYPE.RandomForestRegressor],
             cls_proper_names[g.CLASSIFIER_TYPE.SGD],
             cls_proper_names[g.CLASSIFIER_TYPE.SVC],
             cls_proper_names[g.CLASSIFIER_TYPE.Perceptron]
]

cls_types = [g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.NBayes],
             g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.SVM],
             g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.DecisionTree],
             g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.SVMStandalone],
             g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.SVR],
             g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.NuSVR],
             g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.RandomForestRegressor],
             g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.SGD],
             g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.SVC],
             g.CLASSIFIER_TYPE.name[g.CLASSIFIER_TYPE.Perceptron]]


selected_features ={
                    'OH_SO':'__OH_SO__',
                    'DONT_YOU':'__DONT_YOU__',
                    'AS_GROUND_AS_VEHICLE':'__AS_GROUND_AS_VEHICLE__',
                    'CAPITAL':'__CAPITAL__',                # * // <<   +
                    'HT':'__HT__',                     # * <<      +
                    'HT_POS':'__HT_POS__',               # * // <<   +
                    'HT_NEG':'__HT_NEG__',               # * // <<   +
                    'LINK':'__LINK__',               # //
                    'POS_SMILEY':'__POS_SMILEY__',           # * // << +
                    'NEG_SMILEY':'__NEG_SMILEY__',           # * // << +
                    'NEGATION':'__NEGATION__',             # * // << +
                    'REFERENCE':'__REFERENCE__',            # * // << +
                    'questionmark':'__questionmark__',         # * // << +
                    'exclamation':'__exclamation__',          # * // << +
                    'fullstop':'__fullstop__',
                    'polarity':'__polarity__',           #
                    'RT':'__RT__',                 # //
                    'LAUGH':'__LAUGH__',              # //
                    'LOVE':'__LOVE__',               # //
                    'Part-of-Speech Tags':'__postags__',              # * // << +
                    'words':'__words__',              #
                    'SentiWordNet Total':'__swn_score__',
                    'SentiWordNet for each word':'__s_word__',               # * // << +
                    't_similarity':'__t_similarity__',
                    'Resnik':'__res__',                  # * <<
                    'Lin':'__lin__',
                    'Wu-Palmer':'__wup__',
                    'Shortest Path':'__path__',               # //
                    # 'contains_':'__contains_'
}
figurative_features = [ 'OH_SO',
                        'DONT_YOU',
                        'AS_GROUND_AS_VEHICLE',
                        ]
morphological_features=['CAPITAL',
                        'HT',
                        'HT_POS',
                        'HT_NEG',
                        'LINK',
                        'POS_SMILEY',
                        'NEG_SMILEY',
                        'NEGATION',
                        'REFERENCE',
                        'questionmark',
                        'exclamation',
                        'fullstop',
                        'RT',
                        'LAUGH',
                        # 'LOVE'
                        ]
text_similarity_features = [
                        'Resnik',
                        'Lin',
                        'Wu-Palmer',
                        'Shortest Path', ]
features = ['Part-of-Speech Tags',
           # 'words',
           'SentiWordNet Total',
           'SentiWordNet for each word',
           # 'contains_'
           ]


def home(request):
    return render_to_response('index.html')


def login(request):
    return render_to_response('login.html')


def register(request):
    return render_to_response('register.html')


def about(request):
    return render_to_response('about.html')


def dashboard(request):
    return render_to_response('dashboard.html')


def task_info(request):
    return render_to_response('task_info.html')


def tweet_utils(request):
    cleaning_options = {
        "remove_non_ascii":True,
        "remove_rt" : True,
        "remove_laugh": True,
        "split_sentences": True,
        "remove_negations": True,
        "remove_urls": True,
        "remove_emoticons": True,
        "remove_reference": True,
        "remove_special_characters": True,
        "fix_space": True,
        "split_words": True,
        "convert_to_lower": True,
        "remove_multiples": True,
        # self.spellcheck = True
        "remove_stop_words": True
    }
    options = {"cleaning_options": cleaning_options}

    return render_to_response('tweet_utils.html', options)


def data_sets(request):
    test_data_test = TweetTestData.objects.filter(train=0).count()
    test_data_trial = TweetTestData.objects.filter(train=1).count()
    final_data = TweetFinalTestData.objects.count()
    final = {"DataSets":{
            "TweetsPerDataset":[{"key": "test set", "val": test_data_test},
                              {"key": "trial set", "val": test_data_trial},
                              {"key": "final test data", "val": final_data},
                              {"key": "final trial data", "val": test_data_test+test_data_trial}],
            "TestSet":{
                        "train":{
                                "positive": 578,
                                "negative": 6545,
                                "neutral": 483
                                },
                        "test":{
                                "positive": 122,
                                "negative": 719,
                                "neutral": 82
                                }

                        },
            "Final":{
                        "train":{
                                "positive": 578 + 122,
                                "negative": 6545 + 719,
                                "neutral":82 + 483
                                },
                        "test":{
                                "positive": 640,
                                "negative": 3062,
                                "neutral":298
                                }

                        }
                 }
             }

    return render_to_response('data_sets.html', final)


@ensure_csrf_cookie
def results(request):
    discretization = ['1.0', '0.5', '0.2']
    context = RequestContext(request)
    options = {"classifiers": cls_proper_names_list,
               "morphological": morphological_features,
               "figurative": figurative_features,
               "similarity": text_similarity_features,
               "features": features,
               "discretization": discretization,
               "corpuses" : ["Test", "Final"],
               "csrf_token": context}

    q = "SELECT * FROM SentiFeed.TrialScores inner join SentiFeed.Trial on SentiFeed.TrialScores.trial_id = Trial.id " \
        "inner join SelectedFeatures on Trial.selected_features_id = SelectedFeatures.id " \
        "where SelectedFeatures.AS_GROUND_AS_VEHICLE=1 limit 10000; "



    # data = g.mysql_conn.execute_query(q)

    return render_to_response('results.html', options)


@ensure_csrf_cookie
def trial(request):
    discretization = ['1.0', '0.5', '0.2']
    context = RequestContext(request)
    options = {"classifiers": cls_proper_names_list,
               "morphological": morphological_features,
               "figurative": figurative_features,
               "similarity": text_similarity_features,
               "features": features,
               "discretization": discretization,
               "vectorizers": list(vec_proper_names.values()),
               "corpuses" : ["Test", "Final"],
               "csrf_token": context}

    return render_to_response('trial.html', options)


@ensure_csrf_cookie
def start_trial(request):
    response = CommonResponse()
    try:
        print datetime.datetime.now().time()
        g.redis_conn.send_message("Checking configuration -- " + str(datetime.datetime.now().time()))
        cls = request.POST.get('classifier')
        discretization = request.POST.get('discretization')
        vec_opts = request.POST.get('vectorizer_opts')
        vectorizer = transform_to_enum_values(request.POST.get('vectorizer'), vec_proper_names)
        corpus = transform_corpus_to_enum(request.POST.get('corpus'))
        selected_features = get_proper_feature_names(request.POST.getlist(u'selected_features[]'))
        cls_type = transform_to_enum_values(cls, cls_proper_names)
        dscr_type = transform_discretization_to_enum_values(discretization)
        post_process = request.POST.get('post_process') == "true"
        tf = request.POST.get('tf') == "true"
        pwc = request.POST.get('pwc') == "true"

        tst = "Configuration: {0} :: {1} :: {2} -- ".format(cls, discretization, request.POST.get('corpus'))
        g.redis_conn.send_message(str(tst))
        print cls, dscr_type, selected_features, corpus, vectorizer
        trial = None
        g.redis_conn.send_message("Set up Trial -- " + str(datetime.datetime.now().time()))
        try:
            trial_config = TrialConfig(selected_features, ds=corpus,discretization=dscr_type, feedback=True)
            trial_config.post_process=post_process
            trial_config.cls_cfg.use_cosine = pwc
            trial_config.cls_cfg.use_tf = tf
            trial_config.cls_cfg.cls_type = cls_type
            trial_config.cls_cfg.vec_type = vectorizer
            trial_config.cls_cfg.vec_opts = vec_opts
            trial = Trial(trial_config)
            g.redis_conn.send_message("Label range -- {0}".format(str(",".join(trial.tweet_processor.discrete_labels.keys()))))
            trial.classify()
            g.redis_conn.send_message("Classification is finished -- " + str(datetime.datetime.now().time()))
            trial.save_results()
            g.redis_conn.send_message("Results are saved -- " + str(datetime.datetime.now().time()))
        except:
            traceback.print_exc()
            g.redis_conn.send_message("ERROR -- {0}".format(traceback.format_exc()))
        test_set = trial.tweet_processor.test_set
        test_initial_scores = trial.y_test
        predicted_scores = trial.predictions
        prediction_per_initial_agree = []
        prediction_per_initial_disagree = []

        g.redis_conn.send_message("Preparing results -- " + str(datetime.datetime.now().time()))
        for i in range(0, len(test_initial_scores)):

            temp = predicted_scores[i]

            if test_initial_scores[i] > 0:
                if temp > 0:
                    prediction_per_initial_agree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "success"])
                else:
                    prediction_per_initial_disagree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "success"])
            elif test_initial_scores[i] == 0:
                if temp == 0:
                    prediction_per_initial_agree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "info"])
                else:
                    prediction_per_initial_disagree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "info"])
            elif test_initial_scores[i] < 0:

                if temp < 0:
                    prediction_per_initial_agree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "danger"])
                else:
                    prediction_per_initial_disagree.append([test_set[i].text.replace("\n", " "), test_initial_scores[i], temp, "danger"])

        context = RequestContext(request)

        response.data = {
                    "CosineResult": round(trial.cosine_similarity, 3),
                    "csrf_token": context,
                    "Results": {
                      "agree": prediction_per_initial_agree,
                      "disagree": prediction_per_initial_disagree
                    },
                    "basic_measures": {
                                        "accuracy":round(trial.basic_measures.accuracy, 3),
                                        "precision":round(trial.basic_measures.precision, 3),
                                        "recall":round(trial.basic_measures.recall, 3),
                                        "f_score":round(trial.basic_measures.f_score, 3),
                                        "cosine_similarity":round(trial.basic_measures.cosine_similarity, 3),
                                        "mse":round(trial.basic_measures.mse, 3),
                                        "top_features":trial.tweet_processor.classifier.top_features,
                                        "classification_report":trial.basic_measures.classification_report_as_list
                                       }
                  }

        print "end:", datetime.datetime.now().time()
        response.success = True
        return render_to_response('start_trial.html', {"response": response.__dict__(), "csrf_token": context})
    except:
        traceback.print_exc()
        response.message = traceback.format_exc()
        return render_to_response('start_trial.html', {"response": response.__dict__(), "csrf_token": context})


def get_results(request):
    cls = request.POST.get('classifier')
    discretization = request.POST.get('discretization')
    corpus = request.POST.get('corpus')
    selected_features = request.POST.getlist(u'selected_features[]')
    cls_type = transform_to_enum_values(cls, cls_proper_names)
    dscr_type = transform_discretization_to_enum_values(discretization)

    return HttpResponse('')


def transform_to_enum_values(input, dict_to_look_in):

    for k, v in dict_to_look_in.items():
        if v == input:
            return k


def transform_discretization_to_enum_values(dscr):
    if dscr == "1.0":
        return g.DISCRETIZATION.ONE
    elif dscr == "0.5":
        return g.DISCRETIZATION.HALF
    elif dscr == "0.2":
        return g.DISCRETIZATION.ZERO_POINT_TWO
    else:  # default is no descretization
        return g.DISCRETIZATION.ONE


def transform_corpus_to_enum(corpus):
    if corpus == g.DS_TYPE.name[g.DS_TYPE.Final]:
        return g.DS_TYPE.Final
    return g.DS_TYPE.Test


def get_proper_feature_names(selected_features_initial):
    tmp_feat_list = []
    for feat in selected_features_initial:
        tmp_feat_list.append(selected_features[feat])
    return tmp_feat_list


def send_endpoint(request):
    print request.POST
    message = request.POST.get('message')
    print message
    g.redis_conn.send_message(message)
    # return long_polling_endpoint(request)
    return HttpResponse(message.encode('utf8'), content_type='text/plain: charset-utf-8')


def long_polling_endpoint(request):
    response = CommonResponse()
    try:
        response.message = g.redis_conn.receive_message()
        response.success = True
        return HttpResponse(json.dumps({"response": response.__dict__()}), content_type='text/plain: charset-utf-8')
    except:
        traceback.print_exc()
        response.message = "Redis is not available"
        return HttpResponse(json.dumps({"response": response.__dict__()}), content_type='text/plain: charset-utf-8')