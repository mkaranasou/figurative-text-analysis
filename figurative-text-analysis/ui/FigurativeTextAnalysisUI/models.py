# Create your models here.
# https://docs.djangoproject.com/en/1.8/howto/legacy-databases/

# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'p [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class CommonResponse(object):
    def __init__(self):
        self.success = False
        self.message = ""
        self.data = None

    def __dict__(self):
        return { "success": self.success, "message": self.message, "data": self.data}

class Abbreviation(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    abbreviationtext = models.CharField(db_column='AbbreviationText', max_length=45)  # Field name made lowercase.
    descr = models.CharField(db_column='Descr', max_length=45, blank=True)  # Field name made lowercase.
    byhandevaluation = models.FloatField(db_column='ByHandEvaluation', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Abbreviation'


class BookReviewsNeg(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    review = models.CharField(db_column='Review', max_length=1000, blank=True)  # Field name made lowercase.
    cleanreview = models.CharField(db_column='CleanReview', max_length=1000, blank=True)  # Field name made lowercase.
    evaluationt = models.CharField(db_column='EvaluationT', max_length=45, blank=True)  # Field name made lowercase.
    sumn = models.FloatField(db_column='SumN', blank=True, null=True)  # Field name made lowercase.
    navrn = models.FloatField(db_column='NAVRN', blank=True, null=True)  # Field name made lowercase.
    navrcombin = models.FloatField(db_column='NAVRCombiN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BookReviewsNeg'


class BookReviewsNeu(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    review = models.CharField(db_column='Review', max_length=1000, blank=True)  # Field name made lowercase.
    cleanreview = models.CharField(db_column='CleanReview', max_length=1000, blank=True)  # Field name made lowercase.
    evaluationt = models.CharField(db_column='EvaluationT', max_length=45, blank=True)  # Field name made lowercase.
    sumn = models.FloatField(db_column='SumN', blank=True, null=True)  # Field name made lowercase.
    navrn = models.FloatField(db_column='NAVRN', blank=True, null=True)  # Field name made lowercase.
    navrcombin = models.FloatField(db_column='NAVRCombiN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BookReviewsNeu'


class BookReviewsPos(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    review = models.CharField(db_column='Review', max_length=1000, blank=True)  # Field name made lowercase.
    cleanreview = models.CharField(db_column='CleanReview', max_length=1000, blank=True)  # Field name made lowercase.
    evaluationt = models.CharField(db_column='EvaluationT', max_length=45, blank=True)  # Field name made lowercase.
    sumn = models.FloatField(db_column='SumN', blank=True, null=True)  # Field name made lowercase.
    navrn = models.FloatField(db_column='NAVRN', blank=True, null=True)  # Field name made lowercase.
    navrcombin = models.FloatField(db_column='NAVRCombiN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BookReviewsPos'


class CalculationMethod(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    name = models.CharField(db_column='Name', max_length=45)  # Field name made lowercase.
    descr = models.CharField(db_column='Descr', max_length=100)  # Field name made lowercase.
    type = models.IntegerField(db_column='Type')  # Field name made lowercase.
    datecreated = models.DateField(db_column='DateCreated', blank=True, null=True)  # Field name made lowercase.
    accuracy = models.FloatField(db_column='Accuracy')  # Field name made lowercase.
    combinedaccuracy = models.FloatField(db_column='CombinedAccuracy', blank=True, null=True)  # Field name made lowercase.
    aspectaccuracy = models.FloatField(db_column='AspectAccuracy', blank=True, null=True)  # Field name made lowercase.
    aspectscoreaccuracy = models.FloatField(db_column='AspectScoreAccuracy', blank=True, null=True)  # Field name made lowercase.
    datelastevaluated = models.DateTimeField(db_column='DateLastEvaluated', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CalculationMethod'


class FigurativeTrainingData(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    tweetid = models.BigIntegerField(db_column='TweetId')  # Field name made lowercase.
    tweettext = models.CharField(db_column='TweetText', max_length=140)  # Field name made lowercase.
    createdate = models.CharField(db_column='CreateDate', max_length=150)  # Field name made lowercase.
    emotionn = models.FloatField(db_column='EmotionN', blank=True, null=True)  # Field name made lowercase.
    emotiont = models.CharField(db_column='EmotionT', max_length=45, blank=True)  # Field name made lowercase.
    taggedtext = models.CharField(db_column='TaggedText', max_length=250, blank=True)  # Field name made lowercase.
    query = models.CharField(db_column='Query', max_length=150, blank=True)  # Field name made lowercase.
    cleanedtext = models.CharField(db_column='CleanedText', max_length=150, blank=True)  # Field name made lowercase.
    tags = models.CharField(db_column='Tags', max_length=500, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'FigurativeTrainingData'


class Figurativetrialdata(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    tweetid = models.BigIntegerField(db_column='TweetId')  # Field name made lowercase.
    tweettext = models.CharField(db_column='TweetText', max_length=140)  # Field name made lowercase.
    createdate = models.CharField(db_column='CreateDate', max_length=150)  # Field name made lowercase.
    emotionn = models.FloatField(db_column='EmotionN', blank=True, null=True)  # Field name made lowercase.
    emotiont = models.CharField(db_column='EmotionT', max_length=45, blank=True)  # Field name made lowercase.
    taggedtext = models.CharField(db_column='TaggedText', max_length=250, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'FigurativeTrialData'


class ManualTwitterCorpus(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    tweetid = models.BigIntegerField(db_column='TweetId')  # Field name made lowercase.
    tweettext = models.CharField(db_column='TweetText', max_length=140)  # Field name made lowercase.
    createdate = models.CharField(db_column='CreateDate', max_length=150)  # Field name made lowercase.
    query = models.CharField(db_column='Query', max_length=100)  # Field name made lowercase.
    user = models.CharField(db_column='User', max_length=100)  # Field name made lowercase.
    emotionn = models.FloatField(db_column='EmotionN', blank=True, null=True)  # Field name made lowercase.
    emotiont = models.CharField(db_column='EmotionT', max_length=45, blank=True)  # Field name made lowercase.
    taggedtext = models.CharField(db_column='TaggedText', max_length=250, blank=True)  # Field name made lowercase.
    cleanedtext = models.CharField(db_column='CleanedText', max_length=150, blank=True)  # Field name made lowercase.
    tags = models.CharField(db_column='Tags', max_length=500, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ManualTwitterCorpus'


class ProcessedTweets(models.Model):
    id = models.AutoField(primary_key=True)
    tweetid = models.BigIntegerField(db_column='TweetId')  # Field name made lowercase.
    tweetinitial = models.CharField(db_column='TweetInitial', max_length=150, blank=True)  # Field name made lowercase.
    tweetprocessed = models.CharField(db_column='TweetProcessed', max_length=150, blank=True)  # Field name made lowercase.
    hashtags = models.CharField(db_column='Hashtags', max_length=150, blank=True)  # Field name made lowercase.
    capitals = models.CharField(db_column='Capitals', max_length=150, blank=True)  # Field name made lowercase.
    negations = models.CharField(db_column='Negations', max_length=150, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ProcessedTweets'


class SwnNegative(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    category = models.CharField(db_column='Category', max_length=5)  # Field name made lowercase.
    sentiwordnettempid = models.CharField(db_column='SentiWordNetTempId', max_length=45)  # Field name made lowercase.
    pos = models.FloatField(db_column='Pos')  # Field name made lowercase.
    neg = models.FloatField(db_column='Neg')  # Field name made lowercase.
    systerms = models.TextField(db_column='SysTerms')  # Field name made lowercase.
    description = models.CharField(db_column='Description', max_length=1000, blank=True)  # Field name made lowercase.
    sentimentassesment = models.FloatField(db_column='SentimentAssesment', blank=True, null=True)  # Field name made lowercase.
    sentimentassesmenttext = models.CharField(db_column='SentimentAssesmentText', max_length=50, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SWNNegative'


class SwnPositive(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    category = models.CharField(db_column='Category', max_length=5)  # Field name made lowercase.
    sentiwordnettempid = models.CharField(db_column='SentiWordNetTempId', max_length=45)  # Field name made lowercase.
    pos = models.FloatField(db_column='Pos')  # Field name made lowercase.
    neg = models.FloatField(db_column='Neg')  # Field name made lowercase.
    systerms = models.TextField(db_column='SysTerms')  # Field name made lowercase.
    description = models.CharField(db_column='Description', max_length=1000, blank=True)  # Field name made lowercase.
    sentimentassesment = models.FloatField(db_column='SentimentAssesment', blank=True, null=True)  # Field name made lowercase.
    sentimentassesmenttext = models.CharField(db_column='SentimentAssesmentText', max_length=50, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SWNPositive'


class SampleTweets(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    tweetid = models.TextField(db_column='TweetId')  # Field name made lowercase.
    userid = models.CharField(db_column='UserId', max_length=250)  # Field name made lowercase.
    text = models.TextField(db_column='Text')  # Field name made lowercase.
    source = models.CharField(db_column='Source', max_length=250)  # Field name made lowercase.
    datecreated = models.CharField(db_column='DateCreated', max_length=100)  # Field name made lowercase.
    lat = models.FloatField(blank=True, null=True)
    longt = models.FloatField(blank=True, null=True)
    placelong = models.CharField(db_column='PlaceLong', max_length=250, blank=True)  # Field name made lowercase.
    placeshort = models.CharField(db_column='PlaceShort', max_length=150, blank=True)  # Field name made lowercase.
    dateadded = models.CharField(db_column='DateAdded', max_length=100)  # Field name made lowercase.
    sentimenteval = models.CharField(db_column='SentimentEval', max_length=45, blank=True)  # Field name made lowercase.
    processedtweet = models.CharField(db_column='ProcessedTweet', max_length=150, blank=True)  # Field name made lowercase.
    emoticons = models.CharField(db_column='Emoticons', max_length=150, blank=True)  # Field name made lowercase.
    textscore = models.FloatField(db_column='TextScore', blank=True, null=True)  # Field name made lowercase.
    emoticonsscore = models.FloatField(db_column='EmoticonsScore', blank=True, null=True)  # Field name made lowercase.
    combinedscore = models.FloatField(db_column='CombinedScore', blank=True, null=True)  # Field name made lowercase.
    aspect = models.CharField(db_column='Aspect', max_length=150, blank=True)  # Field name made lowercase.
    references = models.CharField(db_column='References', max_length=150, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SampleTweets'


class Score(models.Model):
    id = models.IntegerField(primary_key=True)
    twittercorpusid = models.BigIntegerField(db_column='TwitterCorpusId')  # Field name made lowercase.
    methodid = models.IntegerField(db_column='MethodId')  # Field name made lowercase.
    score = models.FloatField(db_column='Score')  # Field name made lowercase.
    combinedscore = models.FloatField(db_column='CombinedScore')  # Field name made lowercase.
    emoticonsscore = models.FloatField(db_column='EmoticonsScore', blank=True, null=True)  # Field name made lowercase.
    aspectslist = models.CharField(db_column='AspectsList', max_length=100, blank=True)  # Field name made lowercase.
    aspectsscorelist = models.CharField(db_column='AspectsScoreList', max_length=100, blank=True)  # Field name made lowercase.
    dateupdated = models.CharField(db_column='DateUpdated', max_length=100, blank=True)  # Field name made lowercase.
    source = models.IntegerField(db_column='Source', blank=True, null=True)  # Field name made lowercase.
    processedtweet = models.CharField(db_column='ProcessedTweet', max_length=250, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Score'


class SelectedFeatures(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    oh_so = models.IntegerField(db_column='OH_SO', blank=True, null=True)  # Field name made lowercase.
    dont_you = models.IntegerField(db_column='DONT_YOU', blank=True, null=True)  # Field name made lowercase.
    as_ground_as_vehicle = models.IntegerField(db_column='AS_GROUND_AS_VEHICLE', blank=True, null=True)  # Field name made lowercase.
    capital = models.IntegerField(db_column='CAPITAL', blank=True, null=True)  # Field name made lowercase.
    ht = models.IntegerField(db_column='HT', blank=True, null=True)  # Field name made lowercase.
    ht_pos = models.IntegerField(db_column='HT_POS', blank=True, null=True)  # Field name made lowercase.
    ht_neg = models.IntegerField(db_column='HT_NEG', blank=True, null=True)  # Field name made lowercase.
    link = models.IntegerField(db_column='LINK', blank=True, null=True)  # Field name made lowercase.
    pos_smiley = models.IntegerField(db_column='POS_SMILEY', blank=True, null=True)  # Field name made lowercase.
    neg_smiley = models.IntegerField(db_column='NEG_SMILEY', blank=True, null=True)  # Field name made lowercase.
    negation = models.IntegerField(db_column='NEGATION', blank=True, null=True)  # Field name made lowercase.
    reference = models.IntegerField(db_column='REFERENCE', blank=True, null=True)  # Field name made lowercase.
    questionmark = models.IntegerField(blank=True, null=True)
    exclamation = models.IntegerField(blank=True, null=True)
    fullstop = models.IntegerField(blank=True, null=True)
    polarity = models.IntegerField(blank=True, null=True)
    rt = models.IntegerField(db_column='RT', blank=True, null=True)  # Field name made lowercase.
    laugh = models.IntegerField(db_column='LAUGH', blank=True, null=True)  # Field name made lowercase.
    love = models.IntegerField(db_column='LOVE', blank=True, null=True)  # Field name made lowercase.
    postags = models.IntegerField(blank=True, null=True)
    words = models.IntegerField(blank=True, null=True)
    swn_score = models.IntegerField(blank=True, null=True)
    s_word = models.IntegerField(blank=True, null=True)
    t_similarity = models.IntegerField(blank=True, null=True)
    res = models.IntegerField(blank=True, null=True)
    lin = models.IntegerField(blank=True, null=True)
    wup = models.IntegerField(blank=True, null=True)
    path = models.IntegerField(blank=True, null=True)
    contains_field = models.IntegerField(db_column='contains_', blank=True, null=True)  # Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'SelectedFeatures'


class SentifeedResults(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    tweetid = models.BigIntegerField(db_column='TweetId', unique=True)  # Field name made lowercase.
    tweettext = models.CharField(db_column='TweetText', max_length=150)  # Field name made lowercase.
    tweettextafter = models.CharField(db_column='TweetTextAfter', max_length=150, blank=True)  # Field name made lowercase.
    handcodedt = models.CharField(db_column='HandcodedT', max_length=100, blank=True)  # Field name made lowercase.
    handcodednum = models.FloatField(db_column='HandcodedNum', blank=True, null=True)  # Field name made lowercase.
    sumn = models.FloatField(db_column='SumN', blank=True, null=True)  # Field name made lowercase.
    emoticonsn = models.FloatField(db_column='EmoticonsN', blank=True, null=True)  # Field name made lowercase.
    combinedn = models.FloatField(db_column='CombinedN', blank=True, null=True)  # Field name made lowercase.
    combinedt = models.CharField(db_column='CombinedT', max_length=100, blank=True)  # Field name made lowercase.
    nvar = models.FloatField(db_column='NVAR', blank=True, null=True)  # Field name made lowercase.
    nvarothersn = models.FloatField(db_column='NVAROthersN', blank=True, null=True)  # Field name made lowercase.
    nvarcombn = models.CharField(db_column='NVARCombN', max_length=45, blank=True)  # Field name made lowercase.
    lang = models.CharField(db_column='Lang', max_length=45, blank=True)  # Field name made lowercase.
    emoticonst = models.CharField(db_column='EmoticonsT', max_length=150, blank=True)  # Field name made lowercase.
    spellingcorrection = models.CharField(db_column='SpellingCorrection', max_length=250, blank=True)  # Field name made lowercase.
    aspect = models.CharField(db_column='Aspect', max_length=250, blank=True)  # Field name made lowercase.
    referencest = models.CharField(db_column='ReferencesT', max_length=150, blank=True)  # Field name made lowercase.
    hashlist = models.CharField(db_column='Hashlist', max_length=150, blank=True)  # Field name made lowercase.
    hashlistprocessed = models.CharField(db_column='HashlistProcessed', max_length=150, blank=True)  # Field name made lowercase.
    myeval = models.FloatField(db_column='MyEval', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SentiFeedResults'


class SentiWordNet(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    category = models.CharField(db_column='Category', max_length=5)  # Field name made lowercase.
    sentiwordnettempid = models.CharField(db_column='SentiWordNetTempId', max_length=45)  # Field name made lowercase.
    pos = models.FloatField(db_column='Pos')  # Field name made lowercase.
    neg = models.FloatField(db_column='Neg')  # Field name made lowercase.
    systerms = models.TextField(db_column='SysTerms')  # Field name made lowercase.
    description = models.CharField(db_column='Description', max_length=1000, blank=True)  # Field name made lowercase.
    sentiment_assesment = models.FloatField(db_column='SentimentAssesment', blank=True, null=True)  # Field name made lowercase.
    sentiment_assesmenttext = models.CharField(db_column='SentimentAssesmentText', max_length=50, blank=True)  # Field name made lowercase.
    converted_scale = models.FloatField(db_column='ConvertedScale', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SentiWordNet'


class Sentiwordnettemp(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    category = models.CharField(db_column='Category', max_length=5)  # Field name made lowercase.
    sentiwordnettempid = models.CharField(db_column='SentiWordNetTempId', max_length=45)  # Field name made lowercase.
    pos = models.FloatField(db_column='Pos')  # Field name made lowercase.
    neg = models.FloatField(db_column='Neg')  # Field name made lowercase.
    systerms = models.TextField(db_column='SysTerms')  # Field name made lowercase.
    description = models.CharField(db_column='Description', max_length=1000, blank=True)  # Field name made lowercase.
    sentimentassesment = models.FloatField(db_column='SentimentAssesment', blank=True, null=True)  # Field name made lowercase.
    sentimentassesmenttext = models.CharField(db_column='SentimentAssesmentText', max_length=50, blank=True)  # Field name made lowercase.
    convertedscale = models.FloatField(db_column='ConvertedScale', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SentiWordNetTemp'


class SlangTranslation(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True)  # Field name made lowercase.
    slang_term = models.CharField(db_column='SlangTerm', max_length=150)  # Field name made lowercase.
    slang_translation = models.CharField(db_column='SlangTranslation', max_length=500, blank=True)  # Field name made lowercase.
    slang_meaning = models.CharField(db_column='SlangMeaning', max_length=1000, blank=True)  # Field name made lowercase.
    slang_score = models.FloatField(db_column='SlangScore', blank=True, null=True)  # Field name made lowercase.
    source = models.CharField(db_column='Source', max_length=150, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SlangTranslation'


class Source(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    source_table = models.CharField(db_column='SourceTable', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Source'


class StopwordsSample(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    stopword = models.CharField(db_column='StopWord', max_length=100)  # Field name made lowercase.
    source = models.CharField(db_column='Source', max_length=150, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'StopWordsSample'


class TempCalculation(models.Model):
    id = models.IntegerField(primary_key=True)
    text = models.TextField(db_column='Text')  # Field name made lowercase.
    sentimenteval = models.CharField(db_column='SentimentEval', max_length=45, blank=True)  # Field name made lowercase.
    textscore = models.FloatField(db_column='TextScore', blank=True, null=True)  # Field name made lowercase.
    personalevaluationscore = models.FloatField(db_column='PersonalEvaluationScore', blank=True, null=True)  # Field name made lowercase.
    combined = models.FloatField(db_column='Combined', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TempCalculation'


class TempCalculation2(models.Model):
    id = models.IntegerField(primary_key=True)
    text = models.TextField(db_column='Text')  # Field name made lowercase.
    emoticons = models.CharField(db_column='Emoticons', max_length=150, blank=True)  # Field name made lowercase.
    sentimenteval = models.CharField(db_column='SentimentEval', max_length=45, blank=True)  # Field name made lowercase.
    textscore = models.FloatField(db_column='TextScore', blank=True, null=True)  # Field name made lowercase.
    personalevaluationscore = models.FloatField(db_column='PersonalEvaluationScore', blank=True, null=True)  # Field name made lowercase.
    combined = models.FloatField(db_column='Combined', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TempCalculation2'


class Top20Emoticons(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    emoticon = models.TextField(db_column='Emoticon')  # Field name made lowercase.
    times_used = models.CharField(db_column='TimesUsed', max_length=45)  # Field name made lowercase.
    percent = models.CharField(db_column='Percent', max_length=45, blank=True)  # Field name made lowercase.
    notes = models.CharField(db_column='Notes', max_length=45, blank=True)  # Field name made lowercase.
    personal_evaluations_core = models.FloatField(db_column='PersonalEvaluationScore', blank=True, null=True)  # Field name made lowercase.
    personal_evaluation_text = models.CharField(db_column='PersonalEvaluationText', max_length=45, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Top20Emoticons'


class Trial(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    selected_features_id = models.IntegerField(blank=True, null=True)
    discretization = models.FloatField(blank=True, null=True)
    main_classifier = models.CharField(max_length=100, blank=True)
    helper_classifier = models.CharField(max_length=100, blank=True)
    trial_set_len = models.IntegerField(blank=True, null=True)
    test_set_len = models.IntegerField(blank=True, null=True)
    matched = models.IntegerField(blank=True, null=True)
    correct = models.FloatField(blank=True, null=True)
    precision_field = models.FloatField(db_column='precision_', blank=True, null=True)  # Field renamed because it ended with '_'.
    recall = models.FloatField(blank=True, null=True)
    f_measure = models.FloatField(blank=True, null=True)
    cosine_similarity = models.FloatField(blank=True, null=True)
    labels_range = models.CharField(max_length=1000, blank=True)
    date_created = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Trial'


class Trialscores(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    trial_id = models.IntegerField(blank=True, null=True)
    tweet_id = models.BigIntegerField(blank=True, null=True)
    predicted = models.FloatField(blank=True, null=True)
    initial_score = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'TrialScores'


class TweetBOOld(models.Model):
    id = models.IntegerField(primary_key=True)
    text = models.CharField(max_length=150, blank=True)
    clean_text = models.CharField(max_length=250, blank=True)
    tagged_text = models.CharField(max_length=250, blank=True)
    pos_tagged_text = models.CharField(max_length=250, blank=True)
    tags = models.CharField(max_length=250, blank=True)
    initial_score = models.FloatField(blank=True, null=True)
    sentences = models.CharField(max_length=250, blank=True)
    words = models.CharField(max_length=250, blank=True)
    links = models.CharField(max_length=250, blank=True)
    corrected_words = models.CharField(max_length=150, blank=True)
    smileys_per_sentence = models.CharField(max_length=150, blank=True)
    uppercase_words_per_sentence = models.CharField(max_length=150, blank=True)
    reference = models.CharField(max_length=150, blank=True)
    hash_list = models.CharField(max_length=150, blank=True)
    non_word_chars_removed = models.CharField(max_length=150, blank=True)
    negations = models.CharField(max_length=150, blank=True)
    swn_score_dict = models.CharField(max_length=250, blank=True)
    swn_score = models.FloatField(blank=True, null=True)
    feature_dict = models.CharField(max_length=1000, blank=True)
    score = models.FloatField(blank=True, null=True)
    train = models.TextField(blank=True)  # This field type is a guess.
    pnn = models.CharField(max_length=45, blank=True)
    tweetboold = models.BigIntegerField(db_column='TweetBOOld', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TweetBOOld'


class TweetFinalTestData(models.Model):
    id = models.BigIntegerField(primary_key=True)
    text = models.CharField(max_length=150, blank=True)
    clean_text = models.CharField(max_length=250, blank=True)
    tagged_text = models.CharField(max_length=250, blank=True)
    pos_tagged_text = models.CharField(max_length=500, blank=True)
    tags = models.CharField(max_length=1000, blank=True)
    initial_score = models.FloatField(blank=True, null=True)
    sentences = models.CharField(max_length=250, blank=True)
    words = models.CharField(max_length=500, blank=True)
    links = models.CharField(max_length=250, blank=True)
    corrected_words = models.CharField(max_length=150, blank=True)
    smileys_per_sentence = models.CharField(max_length=150, blank=True)
    uppercase_words_per_sentence = models.CharField(max_length=150, blank=True)
    reference = models.CharField(max_length=150, blank=True)
    hash_list = models.CharField(max_length=150, blank=True)
    non_word_chars_removed = models.CharField(max_length=250, blank=True)
    negations = models.CharField(max_length=150, blank=True)
    swn_score_dict = models.CharField(max_length=1000, blank=True)
    swn_score = models.FloatField(blank=True, null=True)
    feature_dict = models.TextField(blank=True)
    score = models.FloatField(blank=True, null=True)
    train = models.TextField(blank=True)  # This field type is a guess.
    round_score0 = models.FloatField(blank=True, null=True)
    round_score1 = models.FloatField(blank=True, null=True)
    round_score2 = models.FloatField(blank=True, null=True)
    pnn = models.CharField(max_length=45, blank=True)
    modified = models.CharField(max_length=45, blank=True)
    polarity = models.CharField(max_length=45, blank=True)
    words_to_swn_score_dict = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'TweetFinalTestData'


class TweetTestData(models.Model):
    id = models.BigIntegerField(primary_key=True)
    text = models.CharField(max_length=150, blank=True)
    clean_text = models.CharField(max_length=250, blank=True)
    tagged_text = models.CharField(max_length=250, blank=True)
    pos_tagged_text = models.CharField(max_length=500, blank=True)
    tags = models.CharField(max_length=1000, blank=True)
    initial_score = models.FloatField(blank=True, null=True)
    sentences = models.CharField(max_length=250, blank=True)
    words = models.CharField(max_length=500, blank=True)
    links = models.CharField(max_length=250, blank=True)
    corrected_words = models.CharField(max_length=150, blank=True)
    smileys_per_sentence = models.CharField(max_length=150, blank=True)
    uppercase_words_per_sentence = models.CharField(max_length=150, blank=True)
    reference = models.CharField(max_length=150, blank=True)
    hash_list = models.CharField(max_length=150, blank=True)
    non_word_chars_removed = models.CharField(max_length=250, blank=True)
    negations = models.CharField(max_length=150, blank=True)
    swn_score_dict = models.CharField(max_length=1000, blank=True)
    swn_score = models.FloatField(blank=True, null=True)
    feature_dict = models.TextField(blank=True)
    score = models.FloatField(blank=True, null=True)
    train = models.TextField(blank=True)  # This field type is a guess.
    round_score0 = models.FloatField(blank=True, null=True)
    round_score1 = models.FloatField(blank=True, null=True)
    round_score2 = models.FloatField(blank=True, null=True)
    pnn = models.CharField(max_length=45, blank=True)
    modified = models.CharField(max_length=45, blank=True)
    polarity = models.CharField(max_length=45, blank=True)
    words_to_swn_score_dict = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'TweetTestData'


class TwitterCorpus(models.Model):
    tweetid = models.BigIntegerField(db_column='TweetId', primary_key=True)  # Field name made lowercase.
    tweettext = models.CharField(db_column='TweetText', max_length=150, blank=True)  # Field name made lowercase.
    entities = models.CharField(db_column='Entities', max_length=150, blank=True)  # Field name made lowercase.
    dateadded = models.CharField(db_column='DateAdded', max_length=45, blank=True)  # Field name made lowercase.
    userid = models.CharField(db_column='UserId', max_length=150, blank=True)  # Field name made lowercase.
    username = models.CharField(db_column='UserName', max_length=150, blank=True)  # Field name made lowercase.
    aspect = models.CharField(db_column='Aspect', max_length=100, blank=True)  # Field name made lowercase.
    emotiont = models.CharField(db_column='EmotionT', max_length=45, blank=True)  # Field name made lowercase.
    textemotioneval = models.FloatField(db_column='TextEmotionEval', blank=True, null=True)  # Field name made lowercase.
    emoticonemotioneval = models.FloatField(db_column='EmoticonEmotionEval', blank=True, null=True)  # Field name made lowercase.
    combinedscore = models.FloatField(db_column='CombinedScore', blank=True, null=True)  # Field name made lowercase.
    emoticons = models.CharField(db_column='Emoticons', max_length=150, blank=True)  # Field name made lowercase.
    extractedaspect = models.CharField(db_column='ExtractedAspect', max_length=150, blank=True)  # Field name made lowercase.
    textemotion = models.CharField(db_column='TextEmotion', max_length=45, blank=True)  # Field name made lowercase.
    processedtweet = models.CharField(db_column='ProcessedTweet', max_length=150, blank=True)  # Field name made lowercase.
    nvartext = models.CharField(db_column='NVARText', max_length=100, blank=True)  # Field name made lowercase.
    nvarscore = models.FloatField(db_column='NVARScore', blank=True, null=True)  # Field name made lowercase.
    nvarcombi = models.FloatField(db_column='NVARCombi', blank=True, null=True)  # Field name made lowercase.
    incr_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'TwitterCorpus'


class TwitterCorpusNoEmoticons(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    tweetid = models.BigIntegerField(db_column='TweetId')  # Field name made lowercase.
    tweettext = models.CharField(db_column='TweetText', max_length=140)  # Field name made lowercase.
    createdate = models.CharField(db_column='CreateDate', max_length=150)  # Field name made lowercase.
    query = models.CharField(db_column='Query', max_length=100)  # Field name made lowercase.
    user = models.CharField(db_column='User', max_length=100)  # Field name made lowercase.
    emotionn = models.FloatField(db_column='EmotionN', blank=True, null=True)  # Field name made lowercase.
    emotiont = models.CharField(db_column='EmotionT', max_length=45, blank=True)  # Field name made lowercase.
    taggedtext = models.CharField(db_column='TaggedText', max_length=250, blank=True)  # Field name made lowercase.
    cleanedtext = models.CharField(db_column='CleanedText', max_length=150, blank=True)  # Field name made lowercase.
    tags = models.CharField(db_column='Tags', max_length=500, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TwitterCorpusNoEmoticons'


class TwitterCorpusv2(models.Model):
    id = models.IntegerField(primary_key=True)
    tweetid = models.BigIntegerField(db_column='TweetId')  # Field name made lowercase.
    tweettext = models.CharField(db_column='TweetText', max_length=150)  # Field name made lowercase.
    tweettextafter = models.CharField(db_column='TweetTextAfter', max_length=150, blank=True)  # Field name made lowercase.
    emotiont = models.CharField(db_column='EmotionT', max_length=45, blank=True)  # Field name made lowercase.
    emotionn = models.FloatField(db_column='EmotionN', blank=True, null=True)  # Field name made lowercase.
    scoreid = models.IntegerField(db_column='ScoreId', blank=True, null=True)  # Field name made lowercase.
    lang = models.CharField(db_column='Lang', max_length=45, blank=True)  # Field name made lowercase.
    emoticons = models.CharField(db_column='Emoticons', max_length=150, blank=True)  # Field name made lowercase.
    datecreated = models.CharField(db_column='DateCreated', max_length=50, blank=True)  # Field name made lowercase.
    dateupdated = models.CharField(db_column='DateUpdated', max_length=50, blank=True)  # Field name made lowercase.
    query = models.CharField(db_column='Query', max_length=50, blank=True)  # Field name made lowercase.
    aspect = models.CharField(db_column='Aspect', max_length=50, blank=True)  # Field name made lowercase.
    taggedtext = models.CharField(db_column='TaggedText', max_length=250, blank=True)  # Field name made lowercase.
    cleanedtext = models.CharField(db_column='CleanedText', max_length=150, blank=True)  # Field name made lowercase.
    tags = models.CharField(db_column='Tags', max_length=500, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TwitterCorpusV2'


class ToDelete(models.Model):
    id_to_delte = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'to_delete'


class TweetBOttt(models.Model):
    id = models.BigIntegerField(primary_key=True)
    text = models.CharField(max_length=150, blank=True)
    clean_text = models.CharField(max_length=250, blank=True)
    tagged_text = models.CharField(max_length=250, blank=True)
    pos_tagged_text = models.CharField(max_length=500, blank=True)
    tags = models.CharField(max_length=1000, blank=True)
    initial_score = models.FloatField(blank=True, null=True)
    sentences = models.CharField(max_length=250, blank=True)
    words = models.CharField(max_length=500, blank=True)
    links = models.CharField(max_length=250, blank=True)
    corrected_words = models.CharField(max_length=150, blank=True)
    smileys_per_sentence = models.CharField(max_length=150, blank=True)
    uppercase_words_per_sentence = models.CharField(max_length=150, blank=True)
    reference = models.CharField(max_length=150, blank=True)
    hash_list = models.CharField(max_length=150, blank=True)
    non_word_chars_removed = models.CharField(max_length=250, blank=True)
    negations = models.CharField(max_length=150, blank=True)
    swn_score_dict = models.CharField(max_length=1000, blank=True)
    swn_score = models.FloatField(blank=True, null=True)
    feature_dict = models.TextField(blank=True)
    score = models.FloatField(blank=True, null=True)
    train = models.TextField(blank=True)  # This field type is a guess.
    round_score0 = models.FloatField(blank=True, null=True)
    round_score1 = models.FloatField(blank=True, null=True)
    round_score2 = models.FloatField(blank=True, null=True)
    pnn = models.CharField(max_length=45, blank=True)
    modified = models.CharField(max_length=45, blank=True)
    polarity = models.CharField(max_length=45, blank=True)

    class Meta:
        managed = False
        db_table = 'tweetboTTT'
