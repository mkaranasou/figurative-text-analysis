/**
 * Created by maria on 4/18/15.
 * ajax POST calls
 * http://coreymaynard.com/blog/performing-ajax-post-requests-in-django/
 *
 */

$(document).ready(function(){
    var count_vec_options =  $("#count-options-row");
    count_vec_options.hide();

    $("#vec").on('change', function(e){
        console.log(e, e.target)
        if($( "#vec option:selected" ).text() != "DictVectorizer"){
            count_vec_options.slideDown();
            $("#features").slideUp();
            $("#post-processing").bootstrapToggle('off');
            $("#post-processing").bootstrapToggle('disable');
        }
        else{
            if(!count_vec_options.hasClass("hidden")){
                count_vec_options.slideUp();
                $("#features").slideDown();
                $("#post-processing").bootstrapToggle('enable');
                $("#post-processing").bootstrapToggle('on');
            }
        }
    });


    setTimeout(function(){
        google.load('visualization', '1.0', {'callback':drawGoogleCharts,'packages':["annotationchart","corechart", "geochart"]});
    }, 2000);
});

var allDone = false;
function startTrial(btn){
    var options;
    var $btn = $(btn);
    var csrftoken = getCookie('csrftoken');
    var classifier = $( "#cls option:selected" ).text();
    var discretization = $( "#discr option:selected" ).text();
    var vectorizer = $( "#vec option:selected" ).text();
    var vectorizer_opts = $( "#corpus-options").find("input[checked]").attr("id");
    var sel_fea = $( "#fea option:selected");
    var sel_morph = $( "#morph option:selected");
    var sel_fig = $( "#fig option:selected");
    var sel_sim = $( "#sim option:selected");
    var sel_corpus = $( "#corpus option:selected").text();
    var post_process = $("#post-processing").prop('checked')
    var tf = $("#tf-transformer").prop('checked')
    var pwc = $("#pairwise-cosine").prop('checked')
    var selected_features = [];

    console.log(post_process, tf, pwc)
    var spinner = '<div class="spinner_container" id="spinner_box" style="z-index:10;">' +
                    '<div class="feedback" id="feedback"><i class="fa fa-terminal fa-2x"></i>' +
                                    '<ul id="notifications">'+
                                    '</ul>'+
                                '</div>'+
                    '<i class="fa fa-refresh fa-2x fa-spin spinner" style="left: 50%;"></i>' +

                    '</div>';

    $('body').prepend(spinner);

    $.each(sel_fea, function(k, v){
       selected_features.push($(v).text());
    });
    $.each(sel_morph, function(k, v){
       selected_features.push($(v).text());
    });
    $.each(sel_fig, function(k, v){
       selected_features.push($(v).text());
    });

    var default_sim =" -- None -- ";

    $.each(sel_sim, function(k, v){

        var val = $(v).text();
        if(val!=default_sim) {
            selected_features.push(val);
        }
    });
    console.log(selected_features)

    options = {
        "classifier": classifier,
        "discretization": discretization,
        "vectorizer": vectorizer,
        "vectorizer_opts": vectorizer_opts,
        "selected_features": selected_features,
        "corpus": sel_corpus,
        "post_process":post_process,
        "tf":tf,
        "pwc":pwc
    };

    var data = JSON.stringify(options);

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });

    listen();
    allDone = false;
    $.ajax({url: "../dashboard/start_trial",
        method: "POST",
        async: true,
        data: options,
        "beforeSend": function(xhr, settings) {
            $.ajaxSettings.beforeSend(xhr, settings);
        },
        success: function(result){
            $('#trial_results').html(result);
            allDone = true;
            $("#spinner_box").remove();
            cummulativeLineChart(trial_results_agree, "chart_agree");
            cummulativeLineChart(trial_results_disagree, "chart_disagree");
            polarityBarChart(trial_results_agree.length, trial_results_disagree.length);
            basicMetricsChart("basic_metrics");
            mseChart("mse");
            classificationReportChart("classif_report");
            featureImportance("feat_importance");
    },
    error: function(xhr,status,error){
            showalert(error, "danger");
            allDone = true;
            $("#spinner_box").remove();
        }
    });
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function cummulativeLineChart(data, containerId){
    var y_axis = ['y', -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5];
    var data_len = ['x'];
    var data_agree_initial = ['initial'];
    var data_text = [];
    var data_agree_predicted = ['predicted'];
    data_len[0] = "x";

    for(var i=1; i <= data.length; i++){
        data_len.push(i);
    }

    console.log(data_text);
    $.each(data, function(k,v){
        data_agree_initial.push(parseInt(v[1]));
        data_agree_predicted.push(parseInt(v[2]));
        data_text.push(v[0]);
    });

    var chart_agree = c3.generate({
        bindto: "#"+containerId,
        data: {
            x: 'x',
    //        xFormat: '%Y%m%d', // 'xFormat' can be used as custom format of 'x'
            columns: [
                data_len,
                data_agree_initial,
                data_agree_predicted
            ]
        },
        type:'area',
        //types: {
        //    initial: 'spline',
        //    predicted: 'area'
        //},
        axis: {
            x: {
                //type: 'area',

                tick: {
                    fit:false
                    //format: '%Y-%m-%d'
                }
            },
            y: {
                max: 5,
                min: -5,
                label: { // ADD
                    text: 'Score',
                    position: 'outer-middle'
                }
            }
        }
    });

}

function polarityBarChart(len_agree, len_disagree){

    var chart = c3.generate({
    data: {
        bindto:'#chart',
        // iris data from R
        columns: [
            ['agree on polarity', len_agree],
            ['disagree on polarity', len_disagree],
        ],
        type : 'pie',
        onclick: function (d, i) { console.log("onclick", d, i); },
        onmouseover: function (d, i) { console.log("onmouseover", d, i); },
        onmouseout: function (d, i) { console.log("onmouseout", d, i); }
    }
});
}

function basicMetricsChart(containerId){
    var data = google.visualization.arrayToDataTable([
        ["Measure", "Score", { role: "style" } ],
        ["Cosine Similariy", basic_measures["cosine_similarity"], "#1dd2af"],
        ["Accuracy", basic_measures["accuracy"], "#2ecc71"],
        ["Precision", basic_measures["precision"], "#3498db"],
        ["Recall", basic_measures["recall"], "#9b59b6"],
        ["F-Score", basic_measures["f_score"], "#34495e"]
      ]);
      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Basic Measures for Classification Evaluation",
        width: 600,
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById(containerId));
      chart.draw(view, options);
}

function mseChart(containerId){
    var data = google.visualization.arrayToDataTable([
        ["Measure", "Score", { role: "style" } ],
        ["MSE", basic_measures["mse"], "#c0392b"]
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Mean Squared Error",
        width: 600,
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById(containerId));
      chart.draw(view, options);
}

function classificationReportChart(containerId){
    var classif_report = basic_measures["classification_report"];
        var data = new google.visualization.DataTable();
        data.addColumn('number', 'Class');
        data.addColumn('number', 'Precision');
        data.addColumn('number', 'Recall');
        data.addColumn('number', 'F-Score');
        data.addColumn('number', 'Support');
        data.addRows(
          classif_report
        );

        var table = new google.visualization.Table(document.getElementById(containerId));

        table.draw(data, {showRowNumber: false, width: '100%', height: '50%'});
}

function featureImportance(containerId){
    var top_features = basic_measures["top_features"];
    var data = google.visualization.arrayToDataTable([
        ["Feature", "Coefficient", { role: "style" } ],
        [top_features[0][1], parseFloat(parseFloat(top_features[0][0]).toFixed(2)), "#3498db"],
        [top_features[1][1], parseFloat(parseFloat(top_features[1][0]).toFixed(2)), "#3498db"],
        [top_features[2][1], parseFloat(parseFloat(top_features[2][0]).toFixed(2)), "#3498db"],
        [top_features[3][1], parseFloat(parseFloat(top_features[3][0]).toFixed(2)), "#3498db"],
        [top_features[4][1], parseFloat(parseFloat(top_features[4][0]).toFixed(2)), "#3498db"],
        [top_features[5][1], parseFloat(parseFloat(top_features[5][0]).toFixed(2)), "#3498db"],
        [top_features[6][1], parseFloat(parseFloat(top_features[6][0]).toFixed(2)), "#3498db"],
        [top_features[7][1], parseFloat(parseFloat(top_features[7][0]).toFixed(2)), "#3498db"],
        [top_features[8][1], parseFloat(parseFloat(top_features[8][0]).toFixed(2)), "#3498db"],
        [top_features[9][1], parseFloat(parseFloat(top_features[9][0]).toFixed(2)), "#3498db"],
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Top 10 Feature Importances",
        width: 600,
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById(containerId));
      chart.draw(view, options);
}

function drawGoogleCharts(){

}

function listen(){
    $.ajax({url: "../dashboard/long_polling_endpoint/",
        success: function(result){
            try{
                console.log(result)
                 var jsonRes = JSON.parse(result);
                $("<li><span> " +jsonRes.response.message+"</span></li>").hide().prependTo('#notifications').slideDown();
            }
            catch(ex){
                console.log(ex);
                //$("<li class='btn-default'><span> " +result+"</span></li>").prependTo('#notifications');
            }

            if(!allDone){
                listen();
            }
    },
    error: function(xhr,status,error){
       showalert(error, "danger");
        console.log("ERROR:", xhr.responseText)
        allDone = true;
        //if($("#spinner_box").get(0)!=null){
        //    $("#spinner_box").remove();
        //}
    }
    });
}