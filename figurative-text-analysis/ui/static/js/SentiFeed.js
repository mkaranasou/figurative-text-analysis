/**
 * Created by maria on 1/10/15.
 */
var $ = jQuery;

var SentiFeed = function(){

};

SentiFeed.prototype.init = function(){

};

$(document).ready(function(){

    applyStyle();

    registerEvents();


});

function registerEvents(){
    //$("#info_icon").click(function(e){
    //    e.stopPropagation();
    //    e.preventDefault();
    //    showAlert('#content', 'some information blah blah', 'info', 5000);
    //});
}

function applyStyle(){
    $('.main_menu').on('mouseover', function() {

        if($(this).hasClass('shrink')){
            $(this).removeClass('shrink');
        }

    });

    $('.main_menu').on('mouseout', function(){

        if($('#togglemenu').prop('checked')===false) {

            if(!$(this).hasClass('shrink')){
                $(this).addClass('shrink');
            }
            $('.main_menu').css('transition', '600ms ease all');
        }
        else{
            $('.main_menu').hover();
        }
    });

    $('#info_icon').on('mouseover', function(e){
        e.stopPropagation();
       $(this).removeClass('fa-2x');
       $(this).addClass('fa-4x');
        $(this).css('top', '85%');
        $(this).css('color', '#1abc9c');
        $(this).css('transition', '300ms ease all');
    });

    $('#info_icon').on('mouseout', function(e){
        e.stopPropagation();
       $(this).removeClass('fa-4x');
       $(this).addClass('fa-2x');
        $(this).css('top', '90%');
        $(this).css('color', '#2c3e50');
        $(this).css('transition', '300ms ease all');
    });

}

function showAlert(container, message,alerttype, timeOut) {

    $(container).append('<div id="alertdiv" class="alert alert-' +  alerttype + '"><a class="close" data-dismiss="alert" onclick="$(\'#alertdiv\').remove();">X</a><span>'+message+'</span></div>')

    setTimeout(function() { // this will automatically close the alert and remove this if the users doesnt close it in timeOut secs

        $("#alertdiv").remove();

    }, timeOut==undefined?5000:timeOut);
};