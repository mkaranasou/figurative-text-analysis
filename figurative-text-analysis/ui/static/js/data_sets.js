/**
 * Created by maria on 4/18/15.
 */
var data;
$(document).ready(function(){
    data = JSON.parse(ds) ||[10, 44, 25, 1];
    c3mainChart(data.TweetsPerDataset);

    setTimeout(function(){
        google.load("visualization", "1", {'callback':drawCharts, packages:["corechart"]});
    }, 200);

});

function c3mainChart(dataset){
    var available_datasets = ['x'];
    var tweets_per_dataset = ['#tweets_per_dataset'];

    $.each(dataset, function(k, v){
        available_datasets.push(v["key"]);
        tweets_per_dataset.push(parseInt(v["val"]));
    });

    var chart = c3.generate({
        data: {
            bindto: "#chart_how_many_per_ds",
            x: 'x',
            columns: [
                available_datasets,
                tweets_per_dataset
            ],
            type: 'bar'
        },
        axis: {
            x: {
                type: 'category',
                tick: {
                    rotate: 75,
                    multiline: false
                },
                height: 50
            }
        }
    });
}

function drawCharts(){
    googleDonut(data.TestSet.train, "donut-chart-test", "Train Set: Tweets per class");
    googleDonut(data.TestSet.test, "donut-chart-train", "Test Set: Tweets per class");
    googleDonut(data.Final.train, "donut-chart-final-test", "Final Train Set: Tweets per class");
    googleDonut(data.Final.test, "donut-chart-final-train", "Final Test Set: Tweets per class");
}

function googleDonut(data, container, title){
    var info =[];

    $.each(data, function(k, v){
        info.push([k,v]);
    });
    console.log(info, info[0])
    var dataAll = google.visualization.arrayToDataTable([
          ['Class', '#tweets'],
          info[0],
          info[1],
          info[2]
        ]);

        var options = {
            title: title,
            pieHole: 0.4,
            colors: ['green', 'blue', 'red'],
        };

        var chart = new google.visualization.PieChart(document.getElementById(container));
        chart.draw(dataAll, options);
}

function d3Vertical(dataset){
    //Width and height
    var dataset = JSON.parse(dataset);
    var margin = {top: 30, right: 20, bottom: 30, left: 40};
    var width = 400 - margin.left - margin.right;
    var height= 400 - margin.top - margin.bottom + 10;
    var w = width;
    var h = height;

    var xScale = d3.scale.ordinal()
    .domain(dataset.map(function (d) {return d["key"]; }))
    .rangeRoundBands([margin.left, width], 0.05);

    var xAxis = d3.svg.axis().scale(xScale).orient("bottom");

    var yScale = d3.scale.linear()
        .domain([0, d3.max(dataset, function(d) {return d["val"]; })])
        .range([h,0]);

    var yAxis = d3.svg.axis().scale(yScale).orient("bottom");

    //Create SVG element
    var svg = d3.select(".chart_d3")
        .append("svg")
        .attr("width", w)
        .attr("height", h);
    //Create bars
    var rect = svg.selectAll("rect")
        .data(dataset)
        .enter().append("rect")
        .attr("x", function(d, i) {
            return xScale(d["key"]);
        })
        .attr("y", function(d) {
            return yScale(d["val"]);
        })
        .attr("width", xScale.rangeBand())
        .attr("height", function(d) {
            return h - yScale(d["val"]);
        })
        .attr("fill", function(d) {
            return "rgb(0, 0, " + (d["val"]*2) + ")";
        });

    svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + 0 + ")")
    .call(xAxis);

     svg.append("g")
    .attr("class", "y axis")
    .attr("transform", "translate(25," + 0 + ") rotate(90)")
    .call(yAxis);

    svg.append("text")
        .attr("x", (width / 2))
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "bottom")
        .style("font-size", "16px")
        .text(dataset.map(function (d) {return d["val"]; }));
}

function addD3VerticalChart() {
    var width = 960,
    height = 500;

    var y = d3.scale.linear()
        .range([height, 0]);

    var chart = d3.select(".chart")
        .attr("width", width)
        .attr("height", height);

    y.domain([0, d3.max(data, function(d) { return d.value; })]);

    var barWidth = width / data.length;

    var bar = chart.selectAll("g")
      .data(data)
      .enter().append("g")
      .attr("transform", function(d, i) { return "translate(" + i * barWidth + ",0)"; });

    bar.append("rect")
      .attr("y", function(d) { return y(d.value); })
      .attr("height", function(d) { return height - y(d.value); })
      .attr("width", barWidth - 1);

    bar.append("text")
      .attr("x", barWidth / 2)
      .attr("y", function(d) { return y(d.value) + 3; })
      .attr("dy", ".75em")
      .text(function(d) { return d.value; });
};

function type(d) {
  d.value = +d.value; // coerce to number
  return d;
}
