__author__ = 'maria'

import re
from streamparse.bolt import Bolt
from FigurativeTextAnalysis.models import TextCleaner, TextTagger, HashTagHandler


class PreprocessingBolt(Bolt):

    def process(self, tup):
        sentence = tup.values[0]  # extract the sentence
        sentence = re.sub(r"[,.;!\?]", "", sentence)  # get rid of punctuation
        words = [[word.strip()] for word in sentence.split(" ") if word.strip()]
        if not words:
            # no words to process in the sentence, fail the tuple
            self.fail(tup)
            return

        self.emit_many(words)
        self.ack(tup)  # tell Storm the tuple has been processed successfully

if __name__ == '__main__':
    PreprocessingBolt().run()