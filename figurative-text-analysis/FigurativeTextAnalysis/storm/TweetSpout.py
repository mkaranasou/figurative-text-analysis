__author__ = 'maria'


#src/KafkaSpout.py

from streamparse.spout import Spout
from kafka.client import KafkaClient
from kafka.consumer import SimpleConsumer


class TweetSpout(Spout):

    def initialize(self, stormconf, context):
        self.kafka = KafkaClient('127.0.0.1:9092')
        self.consumer = SimpleConsumer(self.kafka, "SpoutTopologyGroup", "Tweets")

    def next_tuple(self):
        for message in self.consumer:
                self.emit([message])

if __name__ == '__main__':
        TweetSpout().run()