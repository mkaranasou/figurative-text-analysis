__author__ = 'maria'

from collections import Counter
from collections import defaultdict
from streamparse.bolt import Bolt
from websocket import create_connection

import json
import time

class Classifier(Bolt):

    def initialize(self, conf, ctx):
        self.counts = defaultdict(int)
        self.results = []
        self.ws = create_connection("ws://localhost:2546")

    def process(self, tup):
        tweet_trial_list = tup.values[0]
        tweet_test_list = tup.values[1]
        # self.counts[word] += 1
        self.ws.send(json.dumps(self.results))
        # self.emit([word, self.counts[word]])


if __name__ == '__main__':
    Classifier().run()