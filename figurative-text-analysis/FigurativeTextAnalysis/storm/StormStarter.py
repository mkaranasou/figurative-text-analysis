__author__ = 'maria'

import itertools

from streamparse.spout import Spout


class StormStarterSpout(Spout):

    def initialize(self, stormconf, context):
        """
        Get tweets from database and disperse them along with configuration for processing
        :param stormconf:
        :type stormconf:
        :param context:
        :type context:
        :return:
        :rtype:
        """
        self.sentences = [
            "She advised him to take a long holiday, so he immediately quit work and took a trip around the world",
            "I was very glad to get a present from her",
            "He will be here in half an hour",
            "She saw him eating a sandwich",
        ]
        self.sentences = itertools.cycle(self.sentences)

    def next_tuple(self):
        sentence = next(self.sentences)
        self.emit([sentence])

    def ack(self, tup_id):
        pass  # if a tuple is processed properly, do nothing

    def fail(self, tup_id):
        pass  # if a tuple fails to process, do nothing


if __name__ == '__main__':
    StormStarterSpout().run()