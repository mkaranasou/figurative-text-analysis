from FigurativeTextAnalysis.helpers.globals import g
from FigurativeTextAnalysis.models.Config import TrialConfig
from FigurativeTextAnalysis.models.Trial import Trial

selected_features =[
                        '__OH_SO__',                  # * // <<   +
                        '__DONT_YOU__',               # * // <<   +
                        '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                        '__CAPITAL__',                # * // <<   +
                        '__HT__',                     # * <<      +
                        '__HT_POS__',                 # * // <<   +
                        '__HT_NEG__',                 # * // <<   +
                        # # '__LINK__',               # //
                        '__POS_SMILEY__',             # * // << +
                        '__NEG_SMILEY__',             # * // << +
                        '__NEGATION__',               # * // << +
                        '__REFERENCE__',            # * // << +
                        '__questionmark__',           # * // << +
                        '__exclamation__',            # * // << +
                        # '__fullstop__',
                        # # # '__RT__',                 # //
                        # # # '__LAUGH__',              # //
                        '__postags__',              # * // << +
                        # # # '__words__',              #
                        # '__swn_score__',
                        '__s_word__',               # * // << +
                        '__res__',                  # * <<
                        # '__lin__',
                        # '__wup__',
                        #  '__path__',               # //
                        # '__contains_'
                        ]


def different_cls_tests(cls):
    with open("TFTests.txt", "ab") as ct:
        ct.write("####################################################################################################")
        trial_config = TrialConfig(selected_features, ds=g.DS_TYPE.Test)
        trial_config.discretization = g.DISCRETIZATION.ONE
        trial_config.post_process=True
        trial_config.cls_cfg.cls_type = cls
        trial_config.cls_cfg.vec_type = g.VECTORIZER.Dict
        trial_config.cls_cfg.use_cosine = False
        trial_config.cls_cfg.use_tf = False
        trial = Trial(trial_config)
        trial.classify()
        trial.save_results()

        ct.write("=================={0} Test use_tf = False=================\n".format(g.CLASSIFIER_TYPE.name[cls]))
        ct.write(str(trial.basic_measures.cosine_similarity)+"\n")
        ct.write(str(trial.basic_measures.accuracy)+"\n")
        ct.write(str(trial.basic_measures.mse)+"\n")

        trial_config = TrialConfig(selected_features, ds=g.DS_TYPE.Test)
        trial_config.discretization = g.DISCRETIZATION.ONE
        trial_config.post_process=True
        trial_config.cls_cfg.cls_type = cls
        trial_config.cls_cfg.vec_type = g.VECTORIZER.Dict
        trial_config.cls_cfg.use_cosine = False
        trial_config.cls_cfg.use_tf = True
        trial = Trial(trial_config)
        trial.classify()
        trial.save_results()

        ct.write("=================={0} Test use_tf = True=================\n".format(g.CLASSIFIER_TYPE.name[cls]))
        ct.write(str(trial.basic_measures.cosine_similarity)+"\n")
        ct.write(str(trial.basic_measures.accuracy)+"\n")
        ct.write(str(trial.basic_measures.mse)+"\n")

        trial_config = TrialConfig(selected_features, ds=g.DS_TYPE.Final)
        trial_config.discretization = g.DISCRETIZATION.ONE
        trial_config.post_process=True
        trial_config.cls_cfg.cls_type = cls
        trial_config.cls_cfg.vec_type = g.VECTORIZER.Dict
        trial_config.cls_cfg.use_cosine = False
        trial_config.cls_cfg.use_tf = False
        trial = Trial(trial_config)
        trial.classify()
        trial.save_results()
        ct.write("=================={0} Final use_tf = False=================\n".format(g.CLASSIFIER_TYPE.name[cls]))
        ct.write(str(trial.basic_measures.cosine_similarity)+"\n")
        ct.write(str(trial.basic_measures.accuracy)+"\n")
        ct.write(str(trial.basic_measures.mse)+"\n")

        trial_config = TrialConfig(selected_features, ds=g.DS_TYPE.Final)
        trial_config.discretization = g.DISCRETIZATION.ONE
        trial_config.post_process=True
        trial_config.cls_cfg.cls_type = cls
        trial_config.cls_cfg.vec_type = g.VECTORIZER.Dict
        trial_config.cls_cfg.use_cosine = False
        trial_config.cls_cfg.use_tf = True
        trial = Trial(trial_config)
        trial.classify()
        trial.save_results()
        ct.write("=================={0} Final use_tf = True=================\n".format(g.CLASSIFIER_TYPE.name[cls]))
        ct.write(str(trial.basic_measures.cosine_similarity)+"\n")
        ct.write(str(trial.basic_measures.accuracy)+"\n")
        ct.write(str(trial.basic_measures.mse)+"\n")


if __name__ == "__main__":
    for cls in [
                g.CLASSIFIER_TYPE.SVMStandalone,
                g.CLASSIFIER_TYPE.NBayes,
                # g.CLASSIFIER_TYPE.DecisionTree,
                # g.CLASSIFIER_TYPE.SVMRbf,
                # g.CLASSIFIER_TYPE.SVR,
                # g.CLASSIFIER_TYPE.SGD,
                # g.CLASSIFIER_TYPE.Perceptron,
                ]:
        different_cls_tests(cls)