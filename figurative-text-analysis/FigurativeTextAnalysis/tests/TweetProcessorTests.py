import ast
import re

from FigurativeTextAnalysis.models.Config import TrialConfig
from FigurativeTextAnalysis.processors.TweetProcessor import TweetProcessor

__author__ = 'maria'
from FigurativeTextAnalysis.helpers.globals import g
import unittest

selected_features_new =[
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                    '__CAPITAL__',                # * // <<   +
                    '__HT__',                     # * <<      +
                    '__HT_POS__',                 # * // <<   +
                    '__HT_NEG__',                 # * // <<   +
                    '__LINK__',               # //
                    '__POS_SMILEY__',             # * // << +
                    '__NEG_SMILEY__',             # * // << +
                    '__NEGATION__',               # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',           # * // << +
                    '__exclamation__',            # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    '__postags__',              # * // << +
                    '__swn_score__',
                    '__s_word__',               # * // << +
                    '__res__',                  # * <<
                    '__lin__',
                    '__wup__',
                     '__path__',               # //
                    '__contains_'
                    ]
selected_features_old =[
                    'OH_SO',
                    'DONT_YOU',
                    'AS_GROUND_AS_VEHICLE',
                    'CAPITAL',
                    'HT',
                    'HT_POS',
                    'HT_NEG',
                    'LINK',
                    'POS_SMILEY',
                    'NEG_SMILEY',
                    'NEGATION',
                    'REFERENCE',
                    'questionmark',
                    'exclamation',
                    'fullstop',
                    'RT',
                    'LAUGH',
                    'postags',
                    'swn_score',
                    's_word',
                    'res',
                    'lin',
                    'wup',
                    'path',
                    'contains_'
                    ]


class TweetProcessorTests(unittest.TestCase):

    def test_excluded_last_four(self):
        test_feat = selected_features_new[:-4]  # exclude  'lin', 'wup', 'path', 'contains_'
        trial_config = TrialConfig(test_feat, ds=g.DS_TYPE.Test)
        trial_config.post_process=True
        trial_config.cls_cfg.cls_type = g.CLASSIFIER_TYPE.SVMStandalone
        trial_config.cls_cfg.use_cosine = False
        trial_config.cls_cfg.use_tf = True
        tweet_processor = TweetProcessor(trial_config)
        tweet_processor.get_test_set_from_db()
        tweet_processor.get_train_set_from_db()
        tweet_processor._get_feature_testset()

        for each in test_feat:
            if '__s_word__' not in each and each != "__postags__":
                print each
                print tweet_processor.feature_list_test[0].keys()
                self.assertTrue(each.replace("__", "") in tweet_processor.feature_list_test[0].keys())

        self.assertTrue('lin' not in tweet_processor.feature_list_test[0].keys())
        self.assertTrue('wup' not in tweet_processor.feature_list_test[0].keys())
        self.assertTrue('path' not in tweet_processor.feature_list_test[0].keys())
        self.assertTrue('contains_' not in tweet_processor.feature_list_test[0].keys())

    def test_excluded_pos_tags(self):
        selected_features_new.remove("__postags__")  # exclude __postags__
        test_feat = selected_features_new
        trial_config = TrialConfig(test_feat, ds=g.DS_TYPE.Test)
        trial_config.post_process=True
        trial_config.cls_cfg.cls_type = g.CLASSIFIER_TYPE.SVMStandalone
        trial_config.cls_cfg.use_cosine = False
        trial_config.cls_cfg.use_tf = True
        tweet_processor = TweetProcessor(trial_config)
        tweet_processor.get_test_set_from_db()
        tweet_processor.get_train_set_from_db()
        tweet_processor._get_feature_testset()

        print test_feat
        for each in test_feat:
            if '__s_word__' not in each and each != "__contains_":
                print each
                print tweet_processor.feature_list_test[0].keys()
                self.assertTrue(each.replace("__", "") in tweet_processor.feature_list_test[0].keys())

        self.assertTrue(tweet_processor.feature_list_test[0].values() not in g.ADJECTIVES + g.ADVERBS + g.NOUNS + g.VERBS)

    def test_excluded_swn(self):
        selected_features_new.remove("__s_word__")  # exclude __s_word__
        test_feat = selected_features_new
        trial_config = TrialConfig(test_feat, ds=g.DS_TYPE.Test)
        trial_config.post_process=True
        trial_config.cls_cfg.cls_type = g.CLASSIFIER_TYPE.SVMStandalone
        trial_config.cls_cfg.use_cosine = False
        trial_config.cls_cfg.use_tf = True
        tweet_processor = TweetProcessor(trial_config)
        tweet_processor.get_test_set_from_db()
        tweet_processor.get_train_set_from_db()
        tweet_processor._get_feature_testset()

        print test_feat
        for each in test_feat:
            if '__s_word__' not in each and each not in ["__contains_", "__postags__"]:
                print each
                print tweet_processor.feature_list_test[0].keys()
                self.assertTrue(each.replace("__", "") in tweet_processor.feature_list_test[0].keys())

        for each in tweet_processor.feature_list_test[0].keys():
            self.assertTrue("s_word" not in each)


if __name__ == '__main__':
    unittest.main()
