from itertools import izip, product
from nltk.corpus import wordnet, wordnet_ic
from FigurativeTextAnalysis.models.SemanticAnalyser import SemanticAnalyzer
brown_ic = wordnet_ic.ic('ic-brown.dat')
semcor_ic = wordnet_ic.ic('ic-semcor.dat')

__author__ = 'maria'

import unittest


class SemanticAnalyserTestCase(unittest.TestCase):
    def test_path_similarity(self):
        semantic_analyser = SemanticAnalyzer()
        text = iter("This is as grey as the pavement".split(' '))
        for worda, wordb in izip(text, text):
            print worda, wordb
            print semantic_analyser.path_similarity(worda, wordb)

        #self.assertEqual(True, False)

    def test_wup_similarity(self):
        semantic_analyser = SemanticAnalyzer()
        text = iter("This is as grey as the pavement".split(' '))
        for worda, wordb in izip(text, text):
            print(worda, wordb)
            print semantic_analyser.wup_similarity(worda, wordb)

        #self.assertEqual(True, False)

    def test_resnik_similarity(self):
        semantic_analyser = SemanticAnalyzer()
        text= ['gift', 'present']
        a = wordnet.synsets(text[0])
        b = wordnet.synsets(text[1])
        print max(s1.res_similarity(s2, brown_ic) for (s1, s2) in product(a, b))

        #self.assertEqual(True, False)

    def test_lin_similarity(self):
        semantic_analyser = SemanticAnalyzer()
        text = iter("This is as grey as the pavement".split(' '))
        for worda, wordb in izip(text, text):
            print(worda, wordb)
            print semantic_analyser.lin_similarity(worda, wordb)

        #self.assertEqual(True, False)


if __name__ == '__main__':
    unittest.main()
