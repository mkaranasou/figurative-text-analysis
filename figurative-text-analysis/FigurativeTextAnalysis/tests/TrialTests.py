import copy
import unittest
from itertools import combinations
from FigurativeTextAnalysis.helpers.globals import g
from FigurativeTextAnalysis.models.Config import TrialConfig
from FigurativeTextAnalysis.models.Trial import Trial

__author__ = 'maria'
#
# selected_features =[
#                     'OH_SO',                # * // <<   +
#                     'DONT_YOU',             # * // <<   +
#                     'AS_GROUND_AS_VEHICLE', # * // <<    +
#                     'CAPITAL',              # * // <<   +
#                     'HT',                   # <<   +
#                     'HT_POS',               # // <<   +
#                     'HT_NEG',               # // <<   +
#                     'LINK',               # //
#                     'POS_SMILEY',           # * // <<  +
#                     'NEG_SMILEY',           # * // <<  +
#                     'NEGATION',             # //   << +
#                     'REFERENCE',            # * // << +
#                     'questionmark',         # * // << +
#                     'exclamation',          # * //   << +
#                      'fullstop',
#                     'polarity',           # *
#                     'RT',                 # //
#                     'LAUGH',              # //
#                     'LOVE',               # * //
#                     'postags',              # * // << +
#                     'words',              # *
#                     'swn_score',
#                     's_word',               # * // << +
#                     't_similarity',
#                     'res',                  # <<
#                     'lin',
#                     'wup',
#                     'path',               # //
#                     'contains_'
#                     ]

selected_features =['__res__',                    # * <<

                    '__HT__',                     # * <<      +
                    '__HT_POS__',                 # * // <<   +
                    '__HT_NEG__',                 # * // <<   +
                    '__OH_SO__',                  # * // <<   +
                    '__DONT_YOU__',               # * // <<   +
                    '__AS_GROUND_AS_VEHICLE__',   # * // <<   +

                    '__CAPITAL__',                # * // <<   +
                    '__POS_SMILEY__',             # * // << +
                    '__NEG_SMILEY__',             # * // << +
                    '__NEGATION__',               # * // << +
                    '__postags__',              # * // << +
                    '__s_word__',               # * // << +
                    '__REFERENCE__',            # * // << +
                    '__questionmark__',           # * // << +
                    '__exclamation__',            # * // << +
                    '__fullstop__',
                    '__RT__',                 # //
                    '__LAUGH__',              # //
                    # '__LOVE__',               # //
                    # '__words__',              #
                    '__swn_score__',
                    '__path__',               # //
                    '__lin__',
                    '__wup__',
                    '__LINK__',               # //
                    '__contains_'
                    ]


class TrialTests(unittest.TestCase):

    def combinations_test(self):
        """
        This should lead to :29^29 combinations multiplied by 9 classifiers, so it is not viable.
        source: http://stackoverflow.com/questions/464864/python-code-to-pick-out-all-possible-combinations-from-a-list
        :return:
        :rtype:
        """
        g.logger.info(list(combinations(selected_features, len(selected_features))))
        for i in xrange(1, len(selected_features)+1):
            g.logger.info(list(combinations(selected_features, i)))
            print i

    def incrementally_add_features_test(self):
        """
        Incrementally add features
        :return:None
        :rtype:None
        """
        for i in range(1, len(selected_features)+1):
            print i
            trial_config = TrialConfig(selected_features, ds=g.DS_TYPE.Test)
            trial = Trial(trial_config)
            trial.classify()
            trial.save_results()

    def incrementally_add_and_remove_if_score_suffers(self):
        """
        Incrementally add features and if score decreases do not include the last feature in next run
        :return:None
        :rtype:None
        """
        previous_score = 0.0
        previous_accuracy = 0.0
        remove_indexes = []
        selected_features_mod = []
        for i in range(0, len(selected_features)-1):
            selected_features_mod = copy.copy(selected_features[:i+1])
            for each in remove_indexes:
                selected_features_mod.remove(each)
            print selected_features_mod
            trial_config = TrialConfig(selected_features_mod, ds=g.DS_TYPE.Test)
            trial_config.cls_cfg.cls_type = g.CLASSIFIER_TYPE.SVMStandalone
            trial = Trial(trial_config)
            trial.classify()
            trial.save_results()
            if previous_score > trial.basic_measures.cosine_similarity:
                print previous_score, trial.basic_measures.cosine_similarity, previous_accuracy, trial.basic_measures.accuracy
                print "going to remove {0}".format(selected_features[i])
                remove_indexes.append(selected_features[i])
            else:
                previous_score = trial.basic_measures.cosine_similarity
                previous_accuracy = trial.basic_measures.accuracy

        print "FINAL COMBINATION:", selected_features_mod

    def one_by_one_features_test(self):
        """
        Incrementally add features
        :return:None
        :rtype:None
        """
        for i in range(0, len(selected_features)):
            print i
            trial_config = TrialConfig(selected_features, ds=g.DS_TYPE.Test)
            trial = Trial(trial_config)
            trial.classify()
            trial.save_results()

    def by_15_features_test(self):
        """
        Combinations by 15
        :return:None
        :rtype:None
        """
        combinations_list = list(combinations(selected_features, 15))
        print len(combinations_list)
        for combination in combinations_list:
            print combination
            trial_config = TrialConfig(selected_features, ds=g.DS_TYPE.Test)
            trial = Trial(trial_config)
            trial.classify()
            trial.save_results()
