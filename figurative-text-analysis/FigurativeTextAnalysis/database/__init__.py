# coding: utf-8
import traceback

__author__ = 'maria'

import MySQLdb
import pypyodbc
from pymongo import MongoClient
from bson.objectid import ObjectId
import redis
from bson.objectid import ObjectId
# from FigurativeTextAnalysis.helpers.globals import g

__author__ = 'maria'

# Establish connection to DB
# conn = pypyodbc.connect('DRIVER={MySQL ODBC 5.1 Driver};SERVER=localhost;DATABASE=SentiFeed;UID=root;PWD=myPass', ansi=True)


class DatabaseConnector(object):
    def __init__(self):
        pass

    def execute_query(self, query):
        pass

    def update(self, query):
        pass

    def close_conn(self):
        pass


class MySQLDatabaseConnector(DatabaseConnector):
    def __init__(self):
        super(MySQLDatabaseConnector, self).__init__()
        self.database = "SentiFeed"
        try:
            self.conn = MySQLdb.connect(host="127.0.0.1",
                                        user="root",
                                        passwd="",
                                        db=self.database)

            self.query = ''
            self.cur = self.conn.cursor()
        except:
            traceback.print_exc()
            print "Could not connect to MySQL, check if server is running!"

    def lastrowid(self):
        if type(self.conn) is MySQLdb:
            return self.cur.lastrowid  # _lastrowid
        else:
            return self.cur._lastrowid  # _lastrowid

    def execute_query(self, query):
        self.query = query
        self.cur.execute(query)
        return self.cur.fetchall()

    def update(self, query):
        self.query = query
        self.cur.execute(self.query)
        last_row_id = self.cur.lastrowid
        self.conn.commit()
        return last_row_id

    def close_conn(self):
        self.conn.close()


class SQLServerDbConnector(DatabaseConnector):
    def __init__(self):
        super(SQLServerDbConnector, self).__init__()
        self.conn = pypyodbc.connect('DRIVER={SQL Server};SERVER=87.203.107.11:1433;DATABASE=TweetFeed;UID=gen_purpose_user;PWD=')  # 291119851$Mk
        self.query = ''
        self.cur = self.conn.cursor()

    def execute_query(self, query):
        self.query = query
        self.cur.execute(query)
        rows = self.cur.fetchall()
        return rows

    def update(self,query):
        self.query = query
        test = self.cur.execute(self.query)
        return test

    def close_conn(self):
        self.conn.close()


class MongoDBConnector(DatabaseConnector):
    '''
    http://api.mongodb.org/python/current/tutorial.html
    '''
    def __init__(self, db, test_collection):
        super(MongoDBConnector, self).__init__()
        self.client = MongoClient('localhost', 27017)
        self.db_names = self.client.database_names()
        print self.db_names
        try:
            self.db = self.client[db]
            self.collection = self.db.test_collection

        except:
            print "DB does not exist! %s" % db
        '''if db in self.db_names:
            self.db = self.db_names[self.db_names.index(db)]
        else:
            pass'''

    def get_collection(self, collection):
        self.collection = self.db[collection]
        print self.collection

    def save_document_in_collection(self, document, collection_name):
        self.document = document
        self.document_id = self.db[collection_name].insert(self.document)
        return self.document_id

# ObjectId method is used to convert string id to object id
    def retrieve_document_by_id(self,id, collection_name):
        self.document = self.db[collection_name].find_one({"_id": ObjectId(id)})
        print self.document
        return self.document

# item must be properly formated in JSON
    def retrieve_document_by(self,item, collection_name):
        self.document = self.db[collection_name].find_one(item)
        print self.document
        return self.document


class RedisConnector(DatabaseConnector):
    def __init__(self):
        super(RedisConnector, self).__init__()
        try:
            self.pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
            self.redis_server = redis.Redis(connection_pool=self.pool)
            self.tweet_q = 'tweet_queue'
            self.CHANNEL = "demo"
            self.pub_sub_client = redis.StrictRedis()
        except redis.ConnectionError:
            traceback.print_exc()
            g.logger.error("Could not connect to Redis...:{0}".format(redis.ConnectionError))

    def get_variable(self, variable_name):
        response = self.redis_server.get(variable_name)
        return response

    def set_variable(self, variable_name, variable_value):
        self.redis_server = redis.Redis(connection_pool=self.pool)
        self.redis_server.set(variable_name, variable_value)

    def store_tweet_first_in_q(self, tweet):
        redis.Redis.lpush(self.redis_server, self.tweet_q, tweet)

    def send_message(self, message):
       message = message.encode('utf8')
       return self.pub_sub_client.publish(self.CHANNEL, message)

    def receive_message(self):
        # client = redis.StrictRedis()
        # pubsub = client.pubsub()
        self.pub_sub = self.pub_sub_client.pubsub()
        self.pub_sub.subscribe(self.CHANNEL)
        message = ""
        for event in self.pub_sub.listen():
            if event['type'] == 'message':
                message = event['data'].decode('utf8')
                break
        self.pub_sub.unsubscribe()

        return message
    def get_tweet_last_in_q(self):
        return redis.Redis.rpop(self.redis_server, self.tweet_q)

    def get_tweet_q_len(self):
        return redis.Redis.llen(self.redis_server,self.tweet_q)

###############################################################################