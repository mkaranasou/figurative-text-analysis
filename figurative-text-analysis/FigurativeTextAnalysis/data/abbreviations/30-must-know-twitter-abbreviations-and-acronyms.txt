<3	Love	This less than three- acronym is used to be a crudely-shaped heart and is used to signify the word love-
2	To/Too/Two	Short way to say to-, too-, or two- that conserves characters
4	For	Short way to say for- that conserves characters
B4	Before	Short way to say before- that conserves characters
BC	Because 
B/C	Because	Short way to say because- that conserves characters
BTW	By The Way  Short way to say by the way- that conserves characters
CHK	Check	Short way to say check- that conserves characters
EM	Email	Short way to say email- that conserves characters
EML	Email	Short way to say email- that conserves characters
FB 	Facebook	Short way to say Facebook- that conserves characters
GR8	Great	Short way to say great- that conserves characters
HT	Hat Tip	This acronym is used when someone wants to virtually give credit to, or tip their hat at someone
IDC	I Don’t Care	Short way to say I don’t care- that conserves characters
IDK	I Don’t Know	Short way to say I don’t know- that conserves characters
IMO	In My Opinion	Short way to say in my opinion- that conserves characters
JK	Just Kidding	Short way to say just kidding- that conserves characters
LI	LinkedIn	Short way to say LinkedIn- that conserves characters
LOL	Laugh Out Loud	People put this acronym when they want to show that they laughed in response
NSFW	Not Safe For Work	This acronym means that the link contains adult material and is like it says, not safe to view at work
PPL	People	Short way to say People- that conserves characters
#RE	Reply	Short way to show that someone is replying to another’s tweet
S/O	Shout Out	This acronym is used when someone wants to give praise or a shout-out to another person on Twitter
TMB	Tweet Me Back	Put in tweets when someone wants others to respond to their tweet
TY	Thank You	Short way to say thank you- that conserves characters
U	You	Short way to say you- that conserves characters
YT	YouTube	Short way to say YouTube- that conserves characters
YW	You’re Welcome	Short way to say you’re welcome- that conserves characters