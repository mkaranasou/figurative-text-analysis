UPDATE SentiFeed.TwitterCorpusV2, SentiFeed.TwitterCorpus
SET SentiFeed.TwitterCorpusV2.InitialAspect = SentiFeed.TwitterCorpus.Aspect
WHERE SentiFeed.TwitterCorpusV2.TweetId = SentiFeed.TwitterCorpus.id;

UPDATE SentiFeed.CalculationMethod
SET
Accuracy = {0},
CombinedAccuracy = <{CombinedAccuracy: 0}>,
AspectAccuracy = <{AspectAccuracy: 0}>,
AspectScoreAccuracy = <{AspectScoreAccuracy: 0}>,
DateLastEvaluated = <{DateLastEvaluated: }>
WHERE id = <{expr}>;


CREATE TABLE SentiFeed.ManualTwitterCorpus
(
    id int PRIMARY KEY NOT NULL,
    TweetId bigint  NOT NULL,
    TweetText varchar (140) NOT NULL,
    CreateDate varchar (150) NOT NULL,
    Query varchar (100) NOT NULL,
    User varchar (100) NOT NULL
);
ALTER TABLE SentiFeed.ManualTwitterCorpus ADD CONSTRAINT unique_id UNIQUE (id);
ALTER TABLE SentiFeed.ManualTwitterCorpus
CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT ;


CREATE TABLE TwitterCorpusNoEmoticons (
  id int(11) NOT NULL AUTO_INCREMENT,
  TweetId bigint(20) NOT NULL,
  TweetText varchar(140) NOT NULL,
  CreateDate varchar(150) NOT NULL,
  Query varchar(100) NOT NULL,
  User varchar(100) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY unique_id (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO SentiFeed.Source (SourceTable) VALUES ('TwitterCorpus');
INSERT INTO SentiFeed.Source (SourceTable) VALUES ('ManualTwitterCorpus');
INSERT INTO SentiFeed.Source (SourceTable) VALUES ('TwitterCorpusNoEmoticons');


CREATE TABLE SentiFeed.ProcessedTweets (
  id INT NOT NULL AUTO_INCREMENT,
  TweetId BIGINT NOT NULL,
  TweetInitial VARCHAR(150) NULL,
  TweetProcessed VARCHAR(150) NULL,
  Hashtags VARCHAR(150) NULL,
  Capitals VARCHAR(150) NULL,
  Negations VARCHAR(150) NULL,
  PRIMARY KEY (id, TweetId));

CREATE TABLE SlangTranslation (
  Id int(11) NOT NULL AUTO_INCREMENT,
  SlangTerm varchar(150) NOT NULL,
  SlangTranslation varchar(1000) DEFAULT NULL,
  SlangScore float DEFAULT NULL,
  PRIMARY KEY (Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

############ Convert SWN Scale ################
UPDATE SentiFeed.SentiWordNetTemp
SET ConvertedScale = (((SentimentAssesment- 0.0)*5.0)/1.0)+ -5.0


###############################################
CREATE TABLE `BasicMeasures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accuracy` float DEFAULT NULL,
  `precision` float DEFAULT NULL,
  `recall` float DEFAULT NULL,
  `f_score` float DEFAULT NULL,
  `cosine_similarity` float DEFAULT NULL,
  `mse` float DEFAULT NULL,
  `classification_report` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

CREATE TABLE `SelectedFeatures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `OH_SO` tinyint(1) DEFAULT NULL,
  `DONT_YOU` tinyint(1) DEFAULT NULL,
  `AS_GROUND_AS_VEHICLE` tinyint(1) DEFAULT NULL,
  `CAPITAL` tinyint(1) DEFAULT NULL,
  `HT` tinyint(1) DEFAULT NULL,
  `HT_POS` tinyint(1) DEFAULT NULL,
  `HT_NEG` tinyint(1) DEFAULT NULL,
  `LINK` tinyint(1) DEFAULT NULL,
  `POS_SMILEY` tinyint(1) DEFAULT NULL,
  `NEG_SMILEY` tinyint(1) DEFAULT NULL,
  `NEGATION` tinyint(1) DEFAULT NULL,
  `REFERENCE` tinyint(1) DEFAULT NULL,
  `questionmark` tinyint(1) DEFAULT NULL,
  `exclamation` tinyint(1) DEFAULT NULL,
  `fullstop` tinyint(1) DEFAULT NULL,
  `polarity` tinyint(1) DEFAULT NULL,
  `RT` tinyint(1) DEFAULT NULL,
  `LAUGH` tinyint(1) DEFAULT NULL,
  `LOVE` tinyint(1) DEFAULT NULL,
  `postags` tinyint(1) DEFAULT NULL,
  `words` tinyint(1) DEFAULT NULL,
  `swn_score` tinyint(1) DEFAULT NULL,
  `s_word` tinyint(1) DEFAULT NULL,
  `t_similarity` tinyint(1) DEFAULT NULL,
  `res` tinyint(1) DEFAULT NULL,
  `lin` tinyint(1) DEFAULT NULL,
  `wup` tinyint(1) DEFAULT NULL,
  `path` tinyint(1) DEFAULT NULL,
  `contains_` tinyint(1) DEFAULT NULL,
  `pos_position` tinyint(1) DEFAULT NULL,
  `multiple_chars_in_a_row` tinyint(1) DEFAULT NULL,
  `punctuation_percentage` tinyint(1) DEFAULT NULL,
  `hashtag_lexicon_sum` tinyint(1) DEFAULT NULL,
  `is_metaphor` tinyint(1) DEFAULT NULL,
  `synset_length` tinyint(1) DEFAULT NULL,
  `lemma_word` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1079 DEFAULT CHARSET=utf8;


CREATE TABLE `Trial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `selected_features_id` int(11) NOT NULL,
  `basic_measures_id` int(11) NOT NULL,
  `discretization` float DEFAULT NULL,
  `classifier` varchar(200) DEFAULT NULL,
  `vectorizer` varchar(200) DEFAULT NULL,
  `dataset` varchar(100) DEFAULT NULL,
  `trial_set_len` int(11) DEFAULT NULL,
  `test_set_len` int(11) DEFAULT NULL,
  `labels_range` longtext,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_idx` (`selected_features_id`),
  KEY `basic_measures_idx` (`basic_measures_id`),
  CONSTRAINT `basic_measures` FOREIGN KEY (`basic_measures_id`) REFERENCES `BasicMeasures` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `selected_features` FOREIGN KEY (`selected_features_id`) REFERENCES `SelectedFeatures` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1055 DEFAULT CHARSET=utf8;

CREATE TABLE `SentiFeed`.`TopFeatures` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `trial_id` INT NOT NULL,
  `coefficient` FLOAT NULL,
  `feature` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `trial_idx` (`trial_id` ASC),
  CONSTRAINT `trial`
    FOREIGN KEY (`trial_id`)
    REFERENCES `SentiFeed`.`Trial` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);

CREATE TABLE `ClassificationReport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trial_id` int(11) NOT NULL,
  `class` varchar(200) NOT NULL,
  `precision` float NOT NULL,
  `recall` float NOT NULL,
  `f_score` float NOT NULL,
  `support` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trial_idx` (`trial_id`),
  CONSTRAINT `report_to_trial` FOREIGN KEY (`trial_id`) REFERENCES `Trial` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;
