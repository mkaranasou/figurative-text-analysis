# coding: utf-8
import traceback
import goslate

__author__ = 'maria'

###############################--GOSLATE TEST--#########################################################################
def translate_goslate(text):
    gs = goslate.Goslate()
    try:
        languages = goslate.Goslate().get_languages()
        for key in languages.keys():
            print key

        '''for language in languages:
            print(language)'''
    except:
        traceback.print_exc()
    print gs._detect_language(text)
    print gs._detect_language("καλημέρα")
    print goslate.Goslate().translate('Hi!', 'zh-CN')
    return gs.translate(text, 'en')

########################################################################################################################
#test goslate
print translate_goslate("καλημέρα")

