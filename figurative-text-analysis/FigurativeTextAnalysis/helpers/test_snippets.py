import ast
from decimal import Decimal
import re

# import requests
from FigurativeTextAnalysis.helpers.globals import g
from FigurativeTextAnalysis.models.Classifier import Classifier

__author__ = 'maria'

word_list = ['juuusttt', 'a', 'testyy', 'looooveee', 'tweet']
'''counter = 0
duplicates_in_word = 0

for word in list:
    counter = 0
    duplicates_in_word = 0
    final_word = ''
    print "initial word = "+word
    for i in range(0, word.__len__()-1):  #for each character in this word
        print i, word[i]

        if word[i] == word[i+1]:#if the i-th character is equal with the previous
            if counter <= 2:# and no duplicates till now
                print word[i], word[i+1]
                final_word +=word[i+1] #then it's ok to keep all characters till now
            if (i == word.__len__()-2):
                if word[i]== word.__len__()-1:
                    print "blah",i
                    final_word += word[i+1]
        else:
            final_word += word[i]
            counter += 1                #increase duplicate counter

    print final_word
'''
'''        else:                           # else if i-th character is not equal to the previous
            if counter > 1 :            #check if counter indicates that before i there were more than 2 letters in row
                print "duplicate indicator, do sth"
                print "dupl counter:", duplicates_in_word
                duplicates_in_word+=1   # this counter indicates how many unique duplicates in word, e.g. in juuuustttt there are two 'u' and 't'

                if duplicates_in_word >1:# if it is the second unique
                    final_word = word[i-counter-2:]
                    print "dupl >1"
                else:
                    for i in range(counter-2, counter+1):
                        final_word = word[:i-counter+1] + word[counter+1:]
                    print "dupl <1 "

                print "dupl counter:", duplicates_in_word
            else:
                final_word = word
            counter = 0                 #reset counter and go for the next letter in word'''

def remove_multiples():
    """
    assume that two characters in a row are valid. If not they should be corrected in spell-checking
    :return:
    """
    counter = 0
    final_word = ''
    print word_list.__len__()
    #for l in range(0, word_list.__len__()):
    for word in word_list:
        for i in range(1, word.__len__()):
            if word[i - 1] == word[i]:
                counter += 1
            else:
                if counter > 2:
                    print "duplicate indicator, do sth"
                    for j in range(counter - 2, counter + 1):
                        print word[:j - counter + 1] + word[counter + 1:-1]
                        final_word += word[:j - counter + 1] + word[counter + 1:-1]
                        print final_word
                counter = 0


def remove_multiples_v2(word_list):
    for word in word_list:
        multi_counter = 0
        index_list = []
        l_word = list(word)
        if l_word.__len__() > 2:  # It doesn' t make sense to look for multiples in words with less than 3 characters
            for i in range(0, l_word.__len__()):
                try:
                    # if this letter equals the next letter then increment multi_counter
                    if l_word[i] == l_word[i+1]:
                        multi_counter += 1
                    else:
                        multi_counter = 0

                except IndexError:
                    if l_word[i] == l_word[i-1] and l_word[i] == l_word[i-2]:
                        multi_counter += 1
                    continue
                try:
                    # if more than 2 duplicates, keep the index of letter to be removed
                    if multi_counter > 1:
                        print 'multiples {0} {1}'.format(l_word[i+1], i+1)
                        index_list.append(i+1)
                except IndexError:
                    if l_word[i] == l_word[i-1] and multi_counter > 1:
                        index_list.append(i)
                    pass
        # in order to avoid index errors, start removing from the end of the word
        reverse_indexes = sorted(index_list, reverse=True)
        for each in reverse_indexes:
            l_word.remove(l_word[each])

        final_word = ''.join(l_word)
        print final_word

def removeRT(text):
    rt = re.findall('^RT\s{0,1}(@\w+:){0,1}', text)
    if rt!=[]:
        print rt
        text = re.sub('^RT:{0,1}', '', text)
        print(text)

#removeRT('RT: @someone:test etbwAESJBF GRDFG NNNnubga ret rt')

def laughter(text):
    found_haha = re.findall(r'((o(m)*(g)*)|o-m-g|o\sm\sg)', text)
    print found_haha

######################################################################

def streaming():
    s = requests.Session()

    headers = {'connection': 'keep-alive', 'content-type': 'application/json', 'x-powered-by': 'Express', 'transfer-encoding': 'chunked'}
    req = requests.Request("GET",'http://127.0.0.1:8000/static/data/big.txt',
                           headers=headers).prepare()

    resp = s.send(req, stream=True)

    for line in resp.iter_lines():
        if line:
            yield line


def read_stream():

    for line in streaming():
        print line


def test_swn_rank():
    final = 0.0
    rank = 0.0
    from globals import g
    q = g.sum_query_figurative_scale_equals_rank_test().format("love", "and Category = 'v' ")
    print q
    score = g.mysql_conn.execute_query(q)
    print score
    for each in score:
        rank_ = int(each[1])
        final += (each[0]/rank_)
        rank += float(1.0/rank_)
    print final/rank
# test_swn_rank()


def update_features():
    q_trial = '''select id, feature_dict from SentiFeed.TweetTestData2''';
    q_test = '''select id, feature_dict from SentiFeed.TweetFinalTestData2''';

    q_trial_upd = '''update SentiFeed.TweetTestData2 set feature_dict = "{0}" where id={1}'''
    q_test_upd = ''' update SentiFeed.TweetFinalTestData2 set feature_dict = "{0}" where id={1}'''

    trial_data = g.mysql_conn.execute_query(q_trial)
    test_data = g.mysql_conn.execute_query(q_test)

    for row in trial_data:
        dicta = ast.literal_eval(row[1])
        temp_dict = {}
        for key, val in dicta.items():
            temp_dict[correct(key, val)] = val

        if len(dicta.items()) != len(temp_dict.items()):
            print "SOS", len(dicta.items()), len(temp_dict.items())
        g.mysql_conn.update(q_trial_upd.format(str(temp_dict).replace("\"", "'"), row[0]))

    for row in test_data:
        dicta = ast.literal_eval(row[1])
        temp_dict = {}
        for key, val in dicta.items():
            temp_dict[correct(key, val)] = val

        if len(dicta.items()) != len(temp_dict.items()):
            print "SOS", len(dicta.items()), len(temp_dict.items())
        g.mysql_conn.update(q_test_upd.format(str(temp_dict).replace("\"", "'"), row[0]))

matches = ['OH_SO',
           'DONT_YOU',
            'AS_GROUND_AS_VEHICLE',
            'CAPITAL',
            'HT',
            'HT_POS',
            'HT_NEG',
            'LINK',
            'POS_SMILEY',
            'NEG_SMILEY',
            'NEGATION',
            'REFERENCE',
            'questionmark',
            'exclamation',
            'fullstop',
            'polarity',
            'RT',
            'LAUGH',
            'LOVE',
            # 'postags',
            'swn_score',
            # 's_word',
            'res',
            'lin',
            'wup',
            'path']


def correct(key, value):

    if key in matches:
        return "__{0}__".format(key)

    if value in g.VERBS or value in g.ADJECTIVES or value in g.ADVERBS or value in g.NOUNS or value in g.OTHERS:
        return key

    if "contains_" in key:
        return "__" + key
    if "s_word" in key:
        return "__s_word__" + key.replace("s_word-", "")

    else:
        print key, value
        return key


def compare_feat_dicts():
    q_trial = '''select id, feature_dict from SentiFeed.TweetTestData''';
    q_test = '''select id, feature_dict from SentiFeed.TweetTestData2''';

    data_trial = g.mysql_conn.execute_query(q_trial)
    data_test = g.mysql_conn.execute_query(q_test)

    for i in range(0, len(data_test)):
        dict_a = ast.literal_eval(data_trial[i][1])
        dict_b = ast.literal_eval(data_test[i][1])

        for k, v in dict_b.items():
            # print "__{0}__".format(k), dict_b.keys()
            if k in dict_a and v != dict_a[k]:
                print k, dict_a[k], " =======> ", v
            if "__{0}__".format(k) in dict_a.keys() and v != dict_a["__{0}__".format(k)]:
                print k, dict_a["__{0}__".format(k)], " =======> ", v

    # for i in range(0, len(data_test)):
    #     dicta_trial = ast.literal_eval(data_trial[i][1])
    #     dicta_test = ast.literal_eval(data_test[i][1])
    #     # dicts = dicta_trial, dicta_test
    #     # a = dict(set.intersection(*(set(d.iteritems()) for d in dicts)))
    #     # for k, v in a.items():
    #     #     print k, v
    #
    #     for k,v in dicta_trial.items():
    #         if k in dicta_test:
    #             if v != dicta_test[k]:
    #                 print k, v, dicta_test[k]


def compare_feat_removed():
    lines = []
    weirdlines = []
    with open("test_original_weird.txt", "rb") as original:
        lines = original.readlines()

    with open("test_weird.txt", "rb") as weird:
        weirdlines = weird.readlines()

    b = set(weirdlines)

    c = [a for a in lines if a not in b]
    for each in c:
        print each


def draw_tree(sentence):
    # http://stackoverflow.com/questions/31936026/drawing-a-flatten-nltk-parse-tree-with-np-chunks
    import nltk
    default_tagger = nltk.data.load(nltk.tag._POS_TAGGER)
    train_model = g.train_model
    tagger = nltk.tag.UnigramTagger(model=train_model, backoff=default_tagger)
    tagged_sentence = tagger.tag(sentence)
    pattern = """NP: {<DT>?<JJ>*<NN>}
    VBD: {<VBD>}
    IN: {<IN>}"""  # "NP: {<DT>?<JJ>*<NN>}"
    NPChunker = nltk.RegexpParser(pattern)
    result = NPChunker.parse(tagged_sentence)
    result.draw()


def test_tfidf():
    tweet_a = {'__POS_SMILEY__': 'False', '__REFERENCE__': 'False', '__OH_SO__': 'False', 'word-6': 'irony', '__DONT_YOU__': 'False', 'an': 'DT', '__s_word-5': 0.93, '__HT_NEG__': 'False', '__AS_GROUND_AS_VEHICLE__': 'False', '__s_word-4': 1.01, '__s_word-6': 1.0, '__s_word-1': 1.0, '__s_word-0': 0.88, '__s_word-3': 1.0, '__s_word-2': 1.02, '__fullstop__': 'True', '__HT_POS__': 'False', 'cup': 'NN', 'irony': 'NN', 'nap': 'NN', '__LOVE__': 'False', 'take': 'VB', '__HT__': 'True', '__CAPITAL__': 'False', '__exclamation__': 'False', '__LINK__': 'False', 'coffee': 'NN', 'finish': 'VBP', '__swn_score__': 0.485, 'word-1': 'finish', 'word-0': 'an', 'word-3': 'coffee', 'word-2': 'cup', 'word-5': 'nap', '__NEGATION__': 'False', '__questionmark__': 'False', '__res__': 0.0, '__lin__': 0.0, '__path__': 0.18055555555555555, '__RT__': 'False', '__NEG_SMILEY__': 'False', '__wup__': 0.5226190476190476, '__LAUGH__': 'False', 'word-4': 'take'}
    tweet_b = {'__POS_SMILEY__': 'False', '__REFERENCE__': 'False', '__OH_SO__': 'False', 'word-6': 'you', '__DONT_YOU__': 'False', '__s_word-5': 1.0, '__HT_NEG__': 'False', '__AS_GROUND_AS_VEHICLE__': 'False', '__s_word-4': 0.75, '__s_word-6': 1.03, '__s_word-1': 1.0, '__s_word-0': 1.0, '__s_word-3': 1.0, '__s_word-2': 1.0, '__fullstop__': 'True', '__HT_POS__': 'False', 'id': 'PRP', 'to': 'TO', '__LOVE__': 'False', '__HT__': 'False', '__CAPITAL__': 'False', '__exclamation__': 'False', '__LINK__': 'False', 'you': 'PRP', 'speak': 'VBP', '__swn_score__': 0.485, 'word-1': 'speak', 'word-0': 'don', 'word-3': 'id', 'word-2': 'Spanish', 'word-5': 'to', '__NEGATION__': 'True', '__questionmark__': 'False', '__res__': 0.0, '__lin__': 0.0, 'don': 'VBP', '__path__': 0.17142857142857143, '__RT__': 'False', '__NEG_SMILEY__': 'False', '__wup__': 0.3431372549019608, '__LAUGH__': 'False', 'Spanish': 'NNP', 'reply': 'VB', 'word-4': 'reply'}
    from FigurativeTextAnalysis.models.Config import TrialConfig
    trial_cfg = TrialConfig([])
    cls = Classifier(trial_cfg.cls_cfg)

    cls.set_y_train([0])
    cls.set_y_trial([1])
    cls.set_x_train([tweet_a])
    cls.set_x_trial([tweet_b])

def test_redis():
    try:
        while 1:
            print g.redis_conn.receive_message()
    except KeyboardInterrupt:
        print "exit!"

def simple_bow_bayes(inputFile, split=80):
    from FigurativeTextAnalysis.models.Config import TrialConfig
    trial_cfg = TrialConfig([])
    trial_cfg.cls_cfg.cls_type = g.CLASSIFIER_TYPE.NBayes
    trial_cfg.cls_cfg.vec_type = g.VECTORIZER.Count
    cls = Classifier(trial_cfg.cls_cfg)
    # simple
    # x_train = ["hate=negative", "loath=negative", "love=positive", "hug=positive", "miss=positive", "rain=negative"]
    # y_train = [0, 0, 1, 1, 1, 0]
    # x_test = ["listen=positive", "right=positive", "waste=negative"]
    # y_test = [1, 1, 0]

    # many words - string
    # x_train = [["hate=negative", "loath=negative"], ["love=positive", "hug=positive", "miss=positive",], ["rain=negative"]]
    # y_train = [0, 1, 0]
    # x_test = [["listen=positive", "right=positive", "waste=negative"]]
    # y_test = [1]

    # many words - float
    x_train = [["hate=0.1", "loath=0.2"], ["love=1.7", "hug=1.5", "miss=1.1"], ["rain=0.8"]]
    y_train = [0, 1, 0]
    x_test = [["listen=1.2", "right=1.5", "waste=0.5"]]
    y_test = [1]

    cls.set_y_train(y_train)
    cls.set_x_train(x_train)
    cls.set_x_trial(x_test)
    cls.set_y_trial(y_test)

    cls.train()
    predictions = cls.predict()

    for i in xrange(0, len(predictions)):
        print x_test[i], "initial:", y_test[i], "predicted", predictions[i]


if __name__ == "__main__":
    test_redis()
    # simple_bow_bayes('')
    # test_tfidf()
    # update_features()
    # compare_feat_dicts()
    # compare_feat_removed()
    # draw_tree(['life', 'not', 'fair', 'appreciate', 'testing', 'this', 'your', 'phone', 'on', 'vacation', 'Hawaii', 'so', 'right'])
    # draw_tree(['Oh', 'life', 'is', 'not', 'fair'])
    # draw_tree(['Oh life is not fair', 'I appreciate you texting me this from your iPhone while on vacation in Hawaii', 'You re so right'])
