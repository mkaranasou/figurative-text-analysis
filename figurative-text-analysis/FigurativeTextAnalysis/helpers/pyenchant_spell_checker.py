import enchant
from enchant.checker import SpellChecker

__author__ = 'maria'
__reference__ = 'https://pypi.python.org/pypi/pyenchant/1.6.5'


class EnchantSpellChecker(object):
    """
    Wrapper class for PyEnchant SpellChecker functionality
    """
    def __init__(self):
        self.d = None  # for the current dictionary
        self.spell_checker = None
        self.errors_in_text = []
        self.correct_list = []

    def dict_exists(self, dict_lang):
        """
        Checks if a dictionary for the language requested exists and initializes a SpellChecker object
        :param dict_lang:
        :return:
        """
        if dict_lang == 'en':
            dict_lang = 'en_US'
            if enchant.dict_exists(dict_lang):
                self.d = enchant.request_dict(dict_lang)
                self.spell_checker = SpellChecker(self.d.tag)
                return True
        else:
            print "No available dictionary"
            return False

    def spell_checker_for_text(self, text):
        """
        Given a text, perform spellcheck and return a list of the errors found.
        :param text:
        :return:
        """
        self.spell_checker.set_text(text)
        for err in self.spell_checker:
            self.errors_in_text.append(err.word)
        return self.errors_in_text

    def correct_list(self):
        """
        Given a list of words that are assumed to be misspelled, return a list that contains
        the lists of suggestions for each word
        :param word:
        :return:
        """
        for word in self.errors_in_text:
            self.correct_list.append(self.spell_checker.correct(word))
        return self.correct_list

    def correct_word(self, word):
        """
        Given a word that is assumed to be misspelled, return any suggestions for correction
        :param word:
        :return:
        """
        self.spell_checker.set_text(word)
        self.spell_checker.word = word
        return self.spell_checker.suggest()

    def clean_up(self):
        """
        Resets the spellchecker
        so as not to create a new spellchecker object for every tweet
        :return:
        """
        self.d = None  # for the current dictionary
        self.spell_checker = None
        self.errors_in_text = []
        self.correct_list = []

    def spell_checker_for_word(self, word):
        """
        Returns the errors in a word if any exist, else None
        :param word:
        :return:
        """
        error = None
        self.spell_checker.set_text(word)
        for err in self.spell_checker:
            error = err.word
        return error

    def is_correct(self, word):
        try:
            return self.spell_checker.check(word)
        except:
            from FigurativeTextAnalysis.helpers.globals import g
            g.logger.error("Problem spellchecking:\t{0}".format(word))
            return False

#######################################################################################################################
#test
#print enchant.list_languages()

'''d = EnchantSpellChecker()
if d.dict_exists("en")==True:
    #d.spell_checker_for_text("this is jsts a testd heeellooo")
    #print d.correct_word("heellloooo")
    #print d.spell_checker_for_word('heeeelloooo')
    #d.spell_checker_for_word('this')
    print d.spell_checker_for_word('hemp')'''