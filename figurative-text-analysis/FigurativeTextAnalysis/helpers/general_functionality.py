import re
import string
import traceback
from FigurativeTextAnalysis.helpers.globals import g
from FigurativeTextAnalysis.models.TweetBO import Tweet

__author__ = 'maria'

##############################################clean text##############################################
def remove_non_printable_characters(text):
    clean_list = []
    assert isinstance(text, string);
    return filter(lambda x: x in string.printable, str(text))
    assert isinstance(text, list);
    for t in text:
        clean_list.append(filter(lambda x: x in string.printable, str(t)))
    return clean_list


#print remove_non_printable_characters("this is  \t")
#print remove_non_printable_characters(["this", "is", "\t"])
#######################################################################################################
def properly_format_swn_data():
    global insert, swn, row, list_row5, split_terms, term, q
    insert = '''INSERT INTO SentiFeed.SentiWordNet
(Category, SentiWordNetTempId, Pos, Neg, SysTerms, Description, SentimentAssesment, SentimentAssesmentText, ConvertedScale)
VALUES
("{0}","{1}","{2}","{3}","{4}","{5}","{6}","{7}","{8}");
'''
    swn = g.mysql_conn.execute_query("Select * From SentiWordNetTemp")
    for row in swn:
        list_row5 = re.sub('\d', "", row[5])
        split_terms = (list_row5.strip(' ')).split("#")
        print split_terms
        for term in split_terms:
            if term != '':
                q = insert.format(row[1], row[2], row[3], row[4], term.strip(" "), re.escape(row[6]), row[7], row[8],
                                  row[9])
                g.mysql_conn.update(q)

def find_score(id, list):
    for i in list:
        if i[0] == id:
            return i[1]
    return 0

def update_trial_data(file_id_score, file_id_text_score):
    list_without_text = []
    final_list = []
    with open(file_id_score, 'rb') as trial_file_init:
        for line in trial_file_init.readlines():
            split_line = line.replace("\r\n", "").replace("\"", "'").split("\t")
            if(len(split_line)>1):
                list_without_text.append(split_line)
    with open(file_id_text_score, 'rb') as trial_file:
        for line in trial_file.readlines():
            try:
                split_line = line.replace("\r\n", "").replace("\"", "'").split("\t")
                tweet = Tweet(id_=split_line[0].encode('utf-8'),
                              text=(str(filter(lambda x: x in string.printable, (split_line[1])).encode('utf-8'))),
                              initial_score=split_line[2].encode('utf-8') if len(split_line) > 2 else find_score(split_line[0].encode('utf-8'), list_without_text),
                              train=0)
                final_list.append(tweet)
                print tweet.text, tweet.initial_score, tweet.train
                save(tweet)
            except UnicodeDecodeError:
                traceback.print_exc()
                print "ERROR", line
def save( tweet):
        qi = g.update_tweet_bo.format(
                                str(tweet.initial_score).replace("\"", "'"),
                                str(tweet.clean_text).replace("\"", "'"),
                                str(tweet.tagged_text).replace("\"", "'"),
                                str(tweet.pos_tagged_text).replace("\"", "'"),
                                str(tweet.tags).replace("\"", "'"),
                                str(tweet.sentences).replace("\"", "'"),
                                str(tweet.words).replace("\"", "'"),
                                str(tweet.links).replace("\"", "'"),
                                str(tweet.corrected_words).replace("\"", "'"),
                                str(tweet.smileys_per_sentence).replace("\"", "'"),
                                str(tweet.uppercase_words_per_sentence).replace("\"", "'"),
                                str(tweet.reference).replace("\"", "'"),
                                str(tweet.hash_list).replace("\"", "'"),
                                str(tweet.non_word_chars_removed).replace("\"", "'"),
                                str(tweet.negations).replace("\"", "'"),
                                str(tweet.swn_score_dict).replace("\"", "'"),
                                tweet.swn_score,
                                str(tweet.feature_dict).replace("\"", "'"),
                                0,
                                tweet.train,
                                tweet.id)
        try:
            g.mysql_conn.update(qi)
        except:
            g.logger.error("Qi:"+qi)

def update_train_by_id(file_id_score):
    list_without_text = []
    q = '''update sentifeed.tweetbo set train=0 where id ="{0}";'''
    with open(file_id_score, 'rb') as trial_file_init:
        for line in trial_file_init.readlines():
            split_line = line.replace("\r\n", "").replace("\"", "'").split("\t")
            if(len(split_line)>1):
                list_without_text.append(split_line)

    for tweet in list_without_text:
        print tweet[0]
        g.mysql_conn.update(q.format(tweet[0]))

a = '../data/figurative_lang_data/trial-data.csv'
b = '../data/figurative_lang_data/task-11-trial_data_.csv'
# update_trial_data(a, b)
# update_train_by_id(a)
