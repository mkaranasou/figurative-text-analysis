# coding: utf-8
import traceback

__author__ = 'maria'

import urllib
import re
import json
from bs4 import BeautifulSoup
import globals as g

import socket
socket.setdefaulttimeout(10)

cache = {}

filesIn = [ '../data/figurative_lang_data/task-11-training-data-integer_.csv',
          '../data/figurative_lang_data/trial-data.csv']
filesOut = ['task-11-training-data-integer_.csv', 'task-11-trial_data_.csv']
other_files = ['../data/figurative_lang_data/test.csv']

training_non_perishable = ['../data/figurative_lang_data/newid_weightedtweetdata.tsv']
out_new = ['../data/figurative_lang_data/newid_weightedtweetdata_retrieved.csv']

tables = ['FigurativeTrainingData', 'FigurativeTrialData']
missing = []

insert = '''
INSERT INTO {4}
(TweetId, TweetText, EmotionN, EmotionT)
VALUES
("{0}", "{1}", "{2}", "{3}");
'''

update_gold_q = '''
    UPDATE tweetbo4000test SET initial_score = '{0}' where id='{1}';
'''


def update_gold():
    fileGold = open('/Users/maria/figurative-text-anaysis/FigurativeTextAnalysis/data/figurative_lang_data/TestGoldID.csv', 'r')

    for line in fileGold.readlines():
        split_line = line.replace("\n", "").split("\t")

        print split_line
        if len(split_line) > 1:
            g.mysql_conn.update(update_gold_q.format(split_line[1], split_line[0]))

def get_missing():
    global missing
    print len(filesIn)
    initial_sids = [[] for i in range(len(filesIn))]
    saved_sids = [[]for i in range(len(filesOut))]
    for f in filesIn:
        with open(f, 'r') as ff:
            for line in ff.readlines():
                # get all sids possible
                try:
                    initial_sids[filesIn.index(f)].append(line.split('\t')[0])
                except Exception:
                    traceback.print_exc()
    for f in filesOut:
        with open(f,'r') as ff:
            for line in ff.readlines():
                # get all sids possible
                saved_sids[filesOut.index(f)].append(line.split('\t')[0])

    for each in initial_sids:
        expected = set(each)
        diff = expected.difference(saved_sids[initial_sids.index(each)])

        print diff
        missing.append(list(diff))
        print len(diff)
    problem = 0
    if missing.__len__() > 0:
        for each in missing:
            i = 0
            with open(filesOut[missing.index(each)],'a') as fout:  # open filesOut in order and append results
                for sid in each:                                   # for twitter id in each list of missing
                    tweet = None
                    text = "Not Available"
                    try:
                        f = urllib.urlopen("http://twitter.com/intent/retweet?tweet_id=%s" % (sid))
                        html = f.read().replace("</html>", "") + "</html>"
                        soup = BeautifulSoup(html)

                        jstt = soup.find_all("div", "tweet-text")
                        tweets = list(set([x.get_text() for x in jstt]))
                        if (len(tweets)) > 1:
                            continue

                        text = tweets[0]
                        cache[sid] = tweets[0]
                        for j in soup.find_all("input", "json-data", id="init-data"):
                            js = json.loads(j['value'])
                            if (js.has_key("embedData")):
                                tweet = js["embedData"]["status"]
                                text = js["embedData"]["status"]["text"]
                                cache[sid] = text
                                print sid
                                break
                    except Exception:
                        print "problem with sid %s" % sid
                        problem += 1
                        #traceback.print_exc()
                        continue

                    try:
                        if (tweet != None and tweet["id_str"] != sid):
                            text = "Not Available"
                            cache[sid] = "Not Available"
                        text = text.replace('\n', ' ', )
                        text = re.sub(r'\s+', ' ', text)
                        final = sid + '\t' + text + '\n'
                                #+ fields[1] + '\n'
                        #fout.write(json.dumps(tweet, indent=2))
                        fout.write(final.encode('utf-8'))
                        i += 1
                        print i, final.encode('utf-8')
                        #print "\t".join(fields + [text]).encode('utf-8')
                    except Exception:
                        print sid
                        traceback.print_exc()
                        continue
            fout.close()
            print "NOT FOUND", problem

def read_sid_and_get_tweet():
    global i, f, fout, line, fields, sid, tweet, text, html, soup, jstt, x, tweets, j, js, final
    i = 0
    for f in training_non_perishable:
        fout = open(out_new[0], 'wb')
        for line in open(f):
            fields = line.rstrip('\n').split('\t')
            sid = fields[1]
            # uid = fields[1]

            #url = 'http://twitter.com/intent/retweet?tweet_id=%s' % (sid)
            #print url

            tweet = None
            text = "Not Available"
            if cache.has_key(sid):
                text = cache[sid]
            else:
                try:
                    f = urllib.urlopen("http://twitter.com/intent/retweet?tweet_id=%s" % (sid))
                    #Thanks to Arturo
                    #Modified by Aniruddha!
                    html = f.read().replace("</html>", "") + "</html>"
                    soup = BeautifulSoup(html)

                    jstt = soup.find_all("div", "tweet-text")
                    tweets = list(set([x.get_text() for x in jstt]))
                    #print tweets
                    #print len(tweets)
                    #print tweets
                    if (len(tweets)) > 1:
                        continue

                    text = tweets[0]
                    cache[sid] = tweets[0]
                    for j in soup.find_all("input", "json-data", id="init-data"):
                        js = json.loads(j['value'])
                        if (js.has_key("embedData")):
                            tweet = js["embedData"]["status"]
                            text = js["embedData"]["status"]["text"]
                            cache[sid] = text
                            print sid
                            break
                except Exception:
                    continue

                try:
                    if (tweet != None and tweet["id_str"] != sid):
                        text = "Not Available"
                        cache[sid] = "Not Available"
                    text = text.replace('\n', ' ', )
                    text = re.sub(r'\s+', ' ', text)
                    final = sid + '\t' + text + '\t' + fields[0] +'\t' +  fields[2] + '\n'
                    #fout.write(json.dumps(tweet, indent=2))
                    fout.write(final.encode('utf-8'))
                    i += 1
                    print i, final.encode('utf-8')
                    #print "\t".join(fields + [text]).encode('utf-8')
                except Exception:
                    print sid
                    traceback.print_exc()
                    continue
        fout.close()
        f.close()

def insert_in_db():
    with open(filesOut[0], 'r') as out_file:
        i = 0
        for line in out_file.readlines():
            i+1
            split_line = line.replace("\"", "'").split('\t')
            q = insert.format(split_line[0],split_line[1], split_line[2],
                                              ("neutral" if int(split_line[2]) == 0
                                                else ("positive" if int(split_line[2])>0
                                                else "negative")),
                                              tables[0])
            print q
            g.mysql_conn.update(q)
        print i

def get_missing2():
    global missing
    print len(filesIn[0])
    initial_sids = [[] for i in range(len(filesIn))]
    saved_sids = [[]for i in range(len(other_files))]
    with open(filesIn[0], 'r') as ff:
            for line in ff.readlines():
                # get all sids possible
                try:
                    initial_sids[0].append(line.split('\t')[0])
                except Exception:
                    traceback.print_exc()
    for f in other_files:
        with open(f,'r') as ff:
            for line in ff.readlines():
                # get all sids possible
                saved_sids[0].append(line.split('\t')[0])

    expected = set(initial_sids[0])
    diff = expected.difference(saved_sids[0])

    print diff
    missing.append(list(diff))
    print len(diff)

# read_sid_and_get_tweet()
# get_missing()
# insert_in_db()
# read_sid_and_get_tweet()
# update_gold()