import ast
import copy
import string
import traceback

from FigurativeTextAnalysis.models.Classifier import Classifier
from FigurativeTextAnalysis.models.Exceptions import LabelNotFound
from FigurativeTextAnalysis.models.TweetBO import Tweet
from FigurativeTextAnalysis.helpers.globals import g
from FigurativeTextAnalysis.helpers.scale_converter import ScaleConverter

# Metrics
# http://streamhacker.com/2010/05/17/text-classification-sentiment-analysis-precision-recall/
# http://scikit-learn.org/stable/modules/classes.html#classification-metrics

__author__ = 'maria'

figurative_data_train_file ='../data/figurative_lang_data/newid_weightedtweetdata_retrieved.csv'
figurative_data_test_file = '../data/figurative_lang_data/task-11-trial_data_.csv'


class TweetProcessor(object):
    """
    Handles Tweet preprocessing and classification.

    @train_file: csv with train tweets (~8000) as downloaded by script provided by SemEval
    @test_file: csv with test tweets (~1000) as downloaded by script provided by SemEval
    @selected_features: list of string representing the features that will be included in feature dictionaries
                        and will be used for classification
    @classifier_type: enum: the type of classifier to be used (see globals for possible values)
    @helper_classifier: enum: the type of classifier to be used as helper classifier (see globals for possible values)
                        - old implementation -
    @discretization: float: this number is the step with which continuous labels will be partitioned to discrete labels.
                    e.g. labels=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5] with discretization 0.5 the final label range
                    will be: labels=[-5to-4.5, -4.5to-4.0, -4.0to-3.5, .... 3.5to4.0, 4.0to4.5, 4.5to5]
    """
    def __init__(self, config):
        self.config = config
        self.classifier = Classifier(self.config.cls_cfg)
        self.discrete_labels = {}
        self.train_set = []
        self.test_set = []
        self.feature_list_train = []
        self.score_list_train = []
        self.feature_list_test = []
        self.score_list_test = []
        self.feature_score_dict = {}
        self.discrete_labels = {}
        self.initial_range = [-5.0, -4.0, -3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
        self.positive_labels = {}
        self.negative_labels = {}
        self.predictions = []
        self.sentiment_discretization_values = ["positive", "negative", "somewhat_positive", "somewhat_negative", "neutral"]
        self._prepare_labels()

    # Older implementation to import tweets in db
    # def read_train__set_from_file(self):
    #     # read test set file into tweets and fill train set list with objects
    #     with open(self.train_file, 'rb') as trf:
    #         for each in trf.readlines():
    #             try:
    #                 split_line = each.strip('\n').split('\t')
    #                 tweet = Tweet(split_line[0].encode('utf-8'),
    #                               (self.remove_non_ascii_chars(split_line[1])).encode('utf-8'),
    #                               initial_score=split_line[3].encode('utf-8'), train=1)
    #                 self.train_set.append(tweet)
    #             except UnicodeDecodeError:
    #                 traceback.print_exc()
    #                 print "ERROR", each
    #
    # def read_test__set_from_file(self):
    #     # read test set file into tweets and fill train set list with objects
    #     with open(self.test_file, 'rb') as trf:
    #         for each in trf.readlines():
    #             try:
    #                 split_line = each.strip('\n').split('\t')
    #                 tweet = Tweet(split_line[0].encode('utf-8'),
    #                               (self.remove_non_ascii_chars(split_line[1])).encode('utf-8'),
    #                               initial_score=0,
    #                               train=0)
    #                 # self.test_set.append(tweet)
    #                 self.save(tweet)
    #             except UnicodeDecodeError:
    #                 traceback.print_exc()
    #                 print "ERROR", each

    def get_train_set_from_db(self):
        """
        Retrieve the Tweet objects to be used for the training of the classifier.
        :return:None
        """
        q = ""
        if self.config.ds == g.DS_TYPE.Final:
            q = "select id, text, initial_score, feature_dict, train, clean_text " \
                "from SentiFeed.TweetTestData;"
        else:
            q = "select id, text, initial_score, feature_dict, train, clean_text " \
                "from SentiFeed.TweetTestData where train=1;"

        train_data = g.mysql_conn.execute_query(q)

        for id, text, initial_score, feature_dict, train, clean_text in train_data:
            try:
                tweet = Tweet(id,
                              text,
                              initial_score=initial_score,                      # handcoded score
                              train=train,                                      # it is a train set
                              feature_dict=ast.literal_eval(feature_dict),      # feature dict --> processing has already taken place
                              clean_text=clean_text
                              # pos_tagged_text=ast.literal_eval(row[5]),       # processing has already taken place
                              # words=ast.literal_eval(row[6])
                              # corrected_words=ast.literal_eval(row[8])
                              )
                self.train_set.append(tweet)
            except:
                g.logger.error("Problem with test tweet:\t{0}".format(id))
                pass

    def get_test_set_from_db(self):
        """
        Retrieve the Tweet objects to be used for the test of the classifier.
        :return:None
        """
        q = ""
        if self.config.ds == g.DS_TYPE.Final:
            q = "select id, text, initial_score, feature_dict, train, clean_text " \
                "from SentiFeed.TweetFinalTestData;"
        else:
            q = "select id, text, initial_score, feature_dict, train, clean_text " \
                 "from SentiFeed.TweetTestData where train=0;"

        test_data = g.mysql_conn.execute_query(q)
        print "Test Data Length:", len(test_data)
        for id, text, initial_score, feature_dict, train, clean_text in test_data:
            try:
                tweet = Tweet(id,
                              text,
                              initial_score=initial_score,                  # handcoded score
                              feature_dict=ast.literal_eval(feature_dict),  # processing has already taken place
                              train=train,                                  # it is a trial/ test set
                              clean_text=clean_text
                              # pos_tagged_text=ast.literal_eval(row[5]),   # processing has already taken place
                              # words=ast.literal_eval(row[6])
                              # corrected_words=ast.literal_eval(row[8])
                              )
                self.test_set.append(tweet)
            except:
                g.logger.error("Problem with train tweet:\t{0}".format(id))
                pass

    def process_tweet_set(self, list_of_tweets):
        """
        Given a list of Tweet objects
        :param list_of_tweets:
        :return:
        """
        i = 0
        for tweet in list_of_tweets:
            g.logger.debug("#########################################################################################")
            g.logger.debug("{0}\t{1}".format(list_of_tweets.index(tweet), tweet.text))
            tweet.tag()
            tweet.clean()
            # tweet.spellcheck()
            # tweet.fix_words()
            tweet.pos_tag()
            tweet.get_swn_score()
            tweet.feature_dict['res'] = tweet.get_semantic_similarity('res')
            tweet.feature_dict['lin'] = tweet.get_semantic_similarity('lin')
            tweet.feature_dict['path'] = tweet.get_semantic_similarity('path')
            tweet.feature_dict['wup'] = tweet.get_semantic_similarity('wup')
            # tweet.get_words_to_swn_score_dict()
            tweet.gather_dicts()
            # self.save(tweet)
            i += 1
            if i % 100 == 0:
                print(i)

    def classify(self):
        """
        1. Retrieve the train and the test sets
        2. Configure the classifier
        3. Predict
        Returns a list of predictions (scores)
        :return:list
        """
        self._get_feature_trainset()
        self._get_feature_testset()
        self.classifier.set_y_train(self.score_list_train)
        self.classifier.set_x_train(self.feature_list_train)
        self.classifier.set_x_trial(self.feature_list_test)
        self.classifier.set_y_trial(self.score_list_test)
        self.classifier.train()
        self.predictions = self.classifier.predict()
        if isinstance(self.predictions[0], str):
            self.predictions = [round(self._restore_modified_score(x),0) for x in self.predictions]
            self.score_list_test = [round(self._restore_modified_score(x),0) for x in self.score_list_test]
        else:
            self.predictions = [round(x,0) for x in self.predictions]
            self.score_list_test = [round(x,0) for x in self.score_list_test]
        match = 0

        for i in range(0, len(self.predictions)):
            # calculate accuracy
            if self.predictions[i] == self.score_list_test[i]:
                match += 1
            g.logger.debug("id\t{0}\ttext:\t{1}\tpredicted:\t{2}\tactual:\t{3}".format(self.test_set[i].id,
                                                                                        self.test_set[i].text,
                                                                                        self.predictions[i],
                                                                                        self.score_list_test[i]))

        results = "{0}/{1} PERCENTAGE: {2}% \n".format(match,
                                                     len(self.score_list_test),
                                                     (float(match)/float(len(self.score_list_test)))*100)
        g.logger.info(results)
        print results
        try:
            metrics = self.classifier.get_metrics()
            g.logger.info(metrics)
        except:
            pass

    def save(self, tweet):
        qi = g.update_tweet_bo().format(
                                str(tweet.clean_text).replace("\"", "'"),
                                str(tweet.tagged_text).replace("\"", "'"),
                                str(tweet.pos_tagged_text).replace("\"", "'"),
                                str(tweet.tags).replace("\"", "'"),
                                str(tweet.initial_score).replace("\"", "'"),
                                str(tweet.sentences).replace("\"", "'"),
                                str(tweet.words).replace("\"", "'"),
                                str(tweet.links).replace("\"", "'"),
                                str(tweet.corrected_words).replace("\"", "'"),
                                str(tweet.smileys_per_sentence).replace("\"", "'"),
                                str(tweet.uppercase_words_per_sentence).replace("\"", "'"),
                                str(tweet.reference).replace("\"", "'"),
                                str(tweet.hash_list).replace("\"", "'"),
                                str(tweet.non_word_chars_removed).replace("\"", "'"),
                                str(tweet.negations).replace("\"", "'"),
                                str(tweet.swn_score_dict).replace("\"", "'"),
                                tweet.swn_score,
                                str(tweet.feature_dict).replace("\"", "'"),
                                0,
                                int(tweet.train),
                                "True",
                                str(tweet.words_to_swn_score_dict).replace("\"", "'"),
                                tweet.id)

        try:
            g.mysql_conn.update(qi)
            g.logger.debug("Qi:"+qi)
        except:
            g.logger.error("error in Qi:"+qi)

    @staticmethod
    def remove_non_ascii_chars(text):
        """
        Removes non printable characters from string
        :return:
        """
        return str(filter(lambda x: x in string.printable, text))

    def fix_problematic(self, problematic_file):
        q = '''select id, text, feature_dict, train from TweetTestData where id = "{0}" '''
        with open(problematic_file, 'r') as problematic:
            for line in problematic.readlines():
                split_line = line.split('\t')
                data = g.mysql_conn.execute_query(q.format(split_line[1].replace("\n", "")))
                for row in data:
                    tweet = Tweet(row[0],                   # id
                                  row[1],                   # text
                                  initial_score=row[2],     # handcoded score
                                  train=1,                  # it is a train set
                                  feature_dict=row[3])      # feature dict --> processing has already taken place
                    self.train_set.append(tweet)
        self.process_tweet_set(self.train_set)

    def _get_feature_trainset(self):
        """
        For each tweet in train_set, get the appropriate feature dictionary and score
        in order to be used in the classification process
        NOTE: _get_feature_trainset and _get_feature_testset could be refactored to one function but it is done
        this way for the sake of readability and debugging
        :return: None
        """
        for tweet in self.train_set:
            try:
                if self.config.cls_cfg.vec_type == g.VECTORIZER.Count:
                    if self.config.cls_cfg.vec_opts == "original":
                        self.feature_list_train.append(tweet.text)
                    else:
                        if len(tweet.clean_text) > 0:
                            self.feature_list_train.append(tweet.clean_text)
                        else:
                            self.feature_list_train.append("__test__ ")
                            # raise Exception("Empty clean_text, tweet is ignored")
                else:
                    self.feature_list_train.append(self._post_process(copy.deepcopy(tweet.feature_dict)))
                self.score_list_train.append(self._get_modified_score(tweet.initial_score))
            except Exception:
                g.logger.error("PROBLEM WITH Train tweet:\t%s" % tweet.id, exc_info=True)

    def _get_feature_testset(self):
        """
        For each tweet in test_set, get the appropriate feature dictionary and score
        in order to be used in the classification process
        NOTE: _get_feature_trainset and _get_feature_testset could be refactored to one function but it is done
        this way for the sake of readability and debugging
        :return: None
        """
        for tweet in self.test_set:
            try:
                if self.config.cls_cfg.vec_type == g.VECTORIZER.Count:
                    if self.config.cls_cfg.vec_opts == "original":
                        self.feature_list_test.append(tweet.text)
                    else:
                        if len(tweet.clean_text) > 0:
                            self.feature_list_test.append(tweet.clean_text)
                        else:
                            self.feature_list_test.append("__test__")
                            # raise Exception("Empty cleane_text, tweet is ignored")
                else:
                    self.feature_list_test.append(self._post_process(copy.deepcopy(tweet.feature_dict)))
                self.score_list_test.append(self._get_modified_score(tweet.initial_score))
            except:
                g.logger.error("PROBLEM WITH:\t%s", tweet.id, exc_info=True)

    def _get_modified_label_for(self, initial_score):
        """
        Given a initial_score that is numeric, find in what range score belongs (according to descretization settings)
        and return the corresponding string key
        :param initial_score: numeric
        :return: string
        """
        for key, value in self.discrete_labels.items():
            if initial_score == 0.0:
                    return "zero"
            elif initial_score > 0.0 and value[0] >= 0.0:
                if value[0] < initial_score <= value[1]:
                    return str(key)
            elif initial_score < 0.0 and value[1] <= 0.0:
                if value[0] <= initial_score < value[1]:
                    return str(key)

        raise LabelNotFound("Problem with label {0}".format(initial_score))

    def _post_process(self, dicta):
        """
        Given a feature dictionary:
        iterate keys and if not excluded, apply conditionally post processing.
        Excluded keys are removed.
        :param dicta: dictionary
        :return: dictionary
        """
        for k in dicta.keys():
            if not self._excluded(k, str(dicta[k])):
                if self.config.post_process:
                    if 's_word-' in k or 'swn_score' in k:
                        dicta[k] = "positive" if dicta[k] > 1.2 else "negative" if dicta[k] < 0.2 else "neutral" \
                                    if 0.95 <= dicta[k] <= 1.05\
                                    else "somewhat_positive" if 1.2 >= dicta[k] > 1.05 \
                                    else "somewhat_negative"
                    elif dicta[k] in g.VERBS:
                        dicta[k] = "VB"
                    elif dicta[k] in g.NOUNS:
                        dicta[k] = "NN"
                    elif dicta[k] in g.ADJECTIVES:
                        dicta[k] = "ADJ"
                    elif dicta[k] in g.ADVERBS:
                        dicta[k] = "RB"
                    elif 'contains_' in k:
                        dicta[k] = "positive" if dicta[k] > 1.2 else "negative" if dicta[k] < 0.2 else "neutral" \
                                    if 0.95 < dicta[k] < 1.05\
                                    else "somewhat_positive" if 1.2 > dicta[k] > 1.05 else "somewhat_negative"
                    elif k in ["__res__", "__lin__", "__wup__", "__path__"] or k in ["res", "lin", "wup", "path"] \
                            and isinstance(k, float):
                        dicta[k] = round(dicta[k], 1)
            else:
                # print "EXCLUDED", k, dicta[k]
                del dicta[k]
        return dicta

    def _get_modified_score(self, initial_score):
        """
        Return either the appropriate form of score depending on descretization and classifier type
        :param initial_score: numeric
        :return:
        """
        if self.config.cls_cfg.cls_type != g.CLASSIFIER_TYPE.SVR:  # if not a regressor
            if self.config.discretization == 1.0:                 # and discretization is the default
                return round(initial_score, 0)                    # return integer
            if self.config.discretization < 1.0:                  # else
                return self._get_modified_label_for(round(initial_score, 1)) # return string label
        if self.config.cls_cfg.cls_type == g.CLASSIFIER_TYPE.SVR: # if cls is a regressor which means it can handle continuous values
            # return float(round(initial_score, 0))
            if self.config.discretization == 1.0:                 # and discretization is the default
                return round(initial_score, 1)                    # return float score - as it is
            if self.config.discretization < 1.0:                  # else
                return self._restore_modified_score(self._get_modified_label_for(initial_score)) # return string label
        return round(initial_score, 0)                            # default is integer

    def _restore_modified_score(self, modified):
        """
        In case of discetization, the modified label is string, e.g. "-5.0To-4.5".
        Metrics cannot be calculated on string labels, thus a conversion is needed.
        For example, given modified = "-5.0To-4.5", this method will return
        (-5 + -4.5 )/ 2.0 = -4.25
        :param modified: string
        :return: float
        """
        if self.config.discretization < 1.0:
            if modified in self.discrete_labels:
                labels_scores = self.discrete_labels[modified]
                return (labels_scores[0] + labels_scores[1])/2.0
            else:
                # print modified
                raise LabelNotFound("{0} is not a valid label".format(modified))
        return modified

    def _excluded(self, k, dicta_k_value):
        """
        Check if feature that consists of a key and a value should be excluded (True) or not (False), given a list
        of selected features
        :param k: string: featrure key
        :param dicta_k_value: any: feature value
        :return:
        """
        if 's_word' in k:                                   # if k is a sentiwordnet feature
            if '__s_word__' not in self.config.selected_features:      # if swn is not wanted
                return True                                 # remove
            return False
        if 'contains_' in k:                                # if k is a sentiwordnet feature
            if '__contains_' not in self.config.selected_features:   # if swn is not wanted
                return True                                 # remove
            return False
        if 'swn_score' in k:                                # if k is sentiwordnet total score
            if '__swn_score__' not in self.config.selected_features:   # if swn is not wanted
                return True                                 # remove
            return False
        if dicta_k_value in g.VERBS\
                or dicta_k_value in g.ADJECTIVES \
                or dicta_k_value in g.ADVERBS \
                or dicta_k_value in g.NOUNS:
            if '__postags__' not in self.config.selected_features:
                return True
            return False
        if 'word-' in dicta_k_value:
            if '__words__' not in self.config.selected_features or 'word' not in self.config.selected_features:
                return True
            return False
        if 't-similarity' in k:
            if '__t_similarity__' not in self.config.selected_features:
                return True
            return False
        if 'res' == k or k == '__res__':
            if '__res__' not in self.config.selected_features:
                return True
            return False
        if 'lin' == k  or k == '__lin__':
            if '__lin__' not in self.config.selected_features:
                return True
            return False
        if 'path' == k or k == '__path__':
            if '__path__' not in self.config.selected_features:
                return True
            return False
        if 'wup' == k or k == '__wup__':
            if '__wup__' not in self.config.selected_features:
                return True
            return False
        if k[0] == "_":
            if k not in self.config.selected_features:
                return True
            # if dicta_k_value not in [True, False, "True", "False", 1, 0]:
            #     return True
            return False
        else:
            if "__{0}__".format(k) not in self.config.selected_features:
                return True
            # if dicta_k_value not in [True, False, "True", "False", 1, 0]:
            #     return True
            return False

        # if (k not in self.config.selected_features and not ("_" not in k  and "__{0}__".format(k) in self.config.selected_features))\
        #     and dicta_k_value not in [True, False, "True", "False", 1, 0]:  #
        #     # print "excluded:", k, dicta_k_value
        #     return True
        # return False

    def _prepare_labels(self):
        """
        Given an initial range of scores [-5, 5], and a discretization configuration <=1.0,
        calculate the text representation of labels with step equal to discretization value
        Example:
        if discretization  == 1.0:
            for i in arange(-5, 5, step:1.0)
                discrete_labels[iToi+i] = [i, i+1]
                which means for i = -5: discrete_labels["-5To-4.5"] = [-5, -4.5]
        :return:None
        """

        from numpy import arange
        for each in arange(self.initial_range[0], self.initial_range[-1], self.config.discretization):
            from_ = round(each,1)
            to_ = round(each + self.config.discretization, 1)
            self.discrete_labels['{0}To{1}'.format(from_, to_)] = [from_, to_]
        self.discrete_labels['zero'] = [0.0, 0.0]
        print "Label range is: ", self.discrete_labels.keys()
