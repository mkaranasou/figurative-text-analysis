__author__ = 'maria'

import re
import traceback
from HashTagHandler import HashTagHandler

__author__ = 'maria'
from FigurativeTextAnalysis.helpers.globals import g

"""
Todo:
1. Tag All corpuses and use them to train svm - ok
2. Keep a data part for test
3. When classifying new tweets:
    Clean them and tag them.
4. add positive HT and negative HT
"""


class TextTagger(object):
    """
    TextTagger is responsible for applying identified tags to a cleaned text.
    The result should be:
    {REFERENCE} {LINK}
    """
    _link_pattern = g.LINK_PATTERN
    _pos_smiley_pattern = g.POS_SMILEY_PATTERN
    _neg_smiley_pattern = g.NEG_SMILEY_PATTERN
    _negation_pattern = g.NEGATIONS_PATTERN
    _reference_pattern = g.REFERENCE_PATTERN
    _ht_pattern = g.HT_PATTERN
    _rt_pattern = g.RT_PATTERN
    _laugh_pattern = g.LAUGH_PATTERN
    _love_pattern = g.LOVE_PATTERN
    _capital_pattern = g.CAPITALS_PATTERN
    _oh_so_pattern = g.OH_SO_PATTERN
    _dont_you_pattern = g.DONT_YOU_PATTERN
    _as_ground_as_vehicle_pattern = g.AS_GROUND_AS_VEHICLE_PATTERN
    _questionmark_pattern = r'\?'
    _fullstop_pattern = r'\.'
    _exclamation_pattern = r'!'
    _patterns_ = {
        g.TAGS.__CAPITAL__: _capital_pattern,
        g.TAGS.__HT__: _ht_pattern,
        g.TAGS.__LINK__: _link_pattern,
        g.TAGS.__POS_SMILEY__: _pos_smiley_pattern,
        g.TAGS.__NEG_SMILEY__: _neg_smiley_pattern,
        g.TAGS.__NEGATION__: _negation_pattern,
        g.TAGS.__REFERENCE__: _reference_pattern,
        g.TAGS.__RT__: _rt_pattern,
        g.TAGS.__LAUGH__: _laugh_pattern,
        g.TAGS.__LOVE__: _love_pattern,
        g.TAGS.__OH_SO__: _oh_so_pattern,
        g.TAGS.__DONT_YOU__: _dont_you_pattern,
        g.TAGS.__AS_GROUND_AS_VEHICLE__: _as_ground_as_vehicle_pattern,
        g.TAGS.__questionmark__: _questionmark_pattern,
        g.TAGS.__fullstop__: _fullstop_pattern,
        g.TAGS.__exclamation__: _exclamation_pattern,
    }
    _tags = {
        g.TAGS.name[g.TAGS.__CAPITAL__]: "False",
        g.TAGS.name[g.TAGS.__HT__]: "False",
        g.TAGS.name[g.TAGS.__HT_POS__]: "False",
        g.TAGS.name[g.TAGS.__HT_NEG__]: "False",
        g.TAGS.name[g.TAGS.__LINK__]: "False",
        g.TAGS.name[g.TAGS.__POS_SMILEY__]: "False",
        g.TAGS.name[g.TAGS.__NEG_SMILEY__]: "False",
        g.TAGS.name[g.TAGS.__NEGATION__]: "False",
        g.TAGS.name[g.TAGS.__REFERENCE__]: "False",
        g.TAGS.name[g.TAGS.__RT__]: "False",
        g.TAGS.name[g.TAGS.__LAUGH__]: "False",
        g.TAGS.name[g.TAGS.__LOVE__]: "False",
        g.TAGS.name[g.TAGS.__OH_SO__]: "False",
        g.TAGS.name[g.TAGS.__DONT_YOU__]: "False",
        g.TAGS.name[g.TAGS.__AS_GROUND_AS_VEHICLE__]: "False",
        g.TAGS.name[g.TAGS.__questionmark__]: "False",
        g.TAGS.name[g.TAGS.__fullstop__]: "False",
        g.TAGS.name[g.TAGS.__exclamation__]: "False"
    }

    def __init__(self):
        self.initial_text = ""
        self.tagged_text = ""
        self.tag_pattern = " {%s} "
        self.ht_handler = HashTagHandler()

    def tag_text(self, text):
        """
        replaces text_to_tag tag part with appropriate tag label
        e.g. http://... ==> {LINK}
        """
        self.ht_handler.reset()
        self.initial_text = text
        self.tagged_text = text
        self.reset()
        for key in self._patterns_:
            try:
                if key == g.TAGS.__CAPITAL__:
                    match = re.findall(self._patterns_[key], self.initial_text)
                    if match.__len__() > 0:
                        self.tagged_text += self.tag_pattern % self._tag(key)
                        self._tags[g.TAGS.name[key]] = "True"
                elif key == g.TAGS.__HT__:
                    # find all hashtags
                    ht = re.findall(self._patterns_[key], self.initial_text)
                    if None != ht and [] != ht:                         # if found
                        ht_key = ''
                        if type(ht) == list:                            # if more than one
                            for each in ht:                             # for each in found hashtags
                                ht_key = self.ht_handler.handle(each)   # find if it is a positive or negative or neutral
                        else:                                           # not a list
                            ht_key = self.ht_handler.handle(ht)         # find if it is a positive or negative or neutral
                        self.tagged_text = re.sub(self._patterns_[key], self.tag_pattern % self._tag(key), self.tagged_text.lower())
                        self._tags[g.TAGS.name[ht_key]] = "True"   # replace the final verdict
                                                                              # of Hashtag handler with "True"
                elif key == g.TAGS.__HT_NEG__ or key == g.TAGS.__HT_POS__:
                    pass
                else:
                    if re.findall(self._patterns_[key], self.initial_text):
                        self._tags[g.TAGS.name[key]] = "True"
                        self.tagged_text = re.sub(self._patterns_[key], self.tag_pattern % self._tag(key), self.tagged_text.lower())
                        if key == g.TAGS.__LINK__:
                            self.initial_text = re.sub(self._patterns_[key], "", self.initial_text)
                    if re.findall(self._patterns_[key], self.initial_text.lower()):
                        self._tags[g.TAGS.name[key]] = "True"
                        self.tagged_text = re.sub(self._patterns_[key], self.tag_pattern % self._tag(key), self.tagged_text.lower())
                        if key == g.TAGS.__LINK__:
                            self.initial_text = re.sub(self._patterns_[key], "", self.initial_text)
            except:
                print key
                print traceback.print_exc()
        return self.tagged_text

    def get_tags(self):
        return self._tags

    def _tag(self, tag):
        return g.TAGS.name[tag]

    def reset(self):
        for key in self._tags.keys():
            self._tags[key] = "False"

