import sklearn
from sklearn.metrics import classification_report
from sklearn.utils.multiclass import unique_labels
from FigurativeTextAnalysis.helpers.globals import g
from FigurativeTextAnalysis.models.Exceptions import InvalidConfiguration
from FigurativeTextAnalysis.models.Scorer import Scorer
from sklearn.metrics import mean_squared_error, f1_score, recall_score, precision_score, accuracy_score
__author__ = 'maria'


class BasicMeasures(object):
    """
    BasicMeasures has all the methods required to retrieve and save the following measures:
    Accuracy, Precision, Recall, F-Score, Cosine Similarity, MSE
    """
    def __init__(self, predictions, ground_truth):
        self.predictions = predictions
        self.ground_truth = ground_truth
        self.accuracy = 0.0
        self.precision = 0.0
        self.recall = 0.0
        self.f_score = 0.0
        self.cosine_similarity = 0.0
        self.mse = 0.0
        self.classification_report = 0.0
        self.classification_report_as_list = []
        if len(predictions) == len(ground_truth) and len(predictions) > 0:
            self.get_measures()
        else:
            raise InvalidConfiguration("Predicted and actual lists are not valid")

    def get_measures(self):
        self.accuracy = self.get_accuracy()
        self.precision = self.get_precision()
        self.recall = self.get_recall()
        self.f_score = self.get_f_measure()
        self.cosine_similarity = self.get_cosine_similarity()
        self.mse = self.get_mse()
        self.classification_report = self.get_classification_report()

    def get_accuracy(self):
        return accuracy_score(self.ground_truth, self.predictions)

    def get_precision(self):
        return precision_score(self.ground_truth, self.predictions, average="weighted")

    def get_recall(self):
        return recall_score(self.ground_truth, self.predictions, average="weighted")

    def get_f_measure(self):
        return f1_score(self.ground_truth, self.predictions, average="weighted")

    def get_mse(self):
        return mean_squared_error(self.ground_truth, self.predictions)

    def get_cosine_similarity(self):
        scorer = Scorer(self.ground_truth, self.predictions)
        return scorer.cosine_()

    def get_classification_report(self):
        return classification_report(self.ground_truth, self.predictions)

    def save_classification_report(self, trial_id):
        labels = unique_labels(self.ground_truth, self.predictions)

        precision, recall, f_score, support = sklearn.metrics.precision_recall_fscore_support(self.ground_truth,
                                                                                              self.predictions,
                                                                                              labels=labels,
                                                                                              average=None)

        for i in xrange(0, len(labels)):
            self.classification_report_as_list.append([labels[i],
                                                       round(precision[i],2),
                                                       round(recall[i],2),
                                                       round(f_score[i],2),
                                                       support[i]])
            q = self._save_cls_report_q().format(trial_id, labels[i], precision[i], recall[i], f_score[i], support[i])
            g.mysql_conn.update(q)

    def save(self):
        q = self._save_q().format(self.accuracy,
                                  self.precision,
                                  self.recall,
                                  self.f_score,
                                  self.cosine_similarity,
                                  self.mse,
                                  str(self.classification_report))

        return g.mysql_conn.update(q)

    def precision_recall_f_score_support(self):
        return sklearn.metrics.precision_recall_fscore_support(self.ground_truth, self.predictions, average="weighted")

    @staticmethod
    def _save_q():
        return '''
        INSERT INTO `SentiFeed`.`BasicMeasures`
                (`accuracy`,
                `precision`,
                `recall`,
                `f_score`,
                `cosine_similarity`,
                `mse`,
                `classification_report`)
                VALUES
                ({0},
                {1},
                {2},
                {3},
                {4},
                {5},
                '{6}');

            '''

    @staticmethod
    def _save_cls_report_q():
        return '''
        INSERT INTO `SentiFeed`.`ClassificationReport`
                    (`trial_id`,
                     `class`,
                     `precision`,
                     `recall`,
                     `f_score`,
                     `support`)
                    VALUES
                    ({0},
                     '{1}',
                     {2},
                     {3},
                     {4},
                     {5});

                '''
