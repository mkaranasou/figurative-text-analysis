__author__ = 'maria'


class Options(object):
    use_default = True

    __metaclass__ = ""
    def __init__(self):
        pass

    def set_defaults(self):
        pass

    def update_options(self, option):
        pass


class CleaningOptions(Options):
    use_default = True

    def __init__(self):
        super(CleaningOptions, self).__init__()

    def set_defaults(self):
        pass

    def update_options(self, option):
        pass


class FeatureExtractionOptions(Options):
    use_default = True

    def __init__(self):
        super(FeatureExtractionOptions, self).__init__()


class TweetOptions(Options):
    def __init__(self):
        super(TweetOptions, self).__init__()