import traceback

__author__ = 'maria'

from nltk.corpus import wordnet as wn
from itertools import product
from FigurativeTextAnalysis.helpers.globals import g
from nltk.corpus import wordnet_ic
brown_ic = wordnet_ic.ic('ic-brown.dat')
semcor_ic = wordnet_ic.ic('ic-semcor.dat')

# lin and res similarity http://www.nltk.org/howto/wordnet.html

'''
http://stackoverflow.com/questions/17296588/python-nltk-returning-odd-result-for-wordnet-similarity-measure
...a score denoting how similar two word senses are,
based on the depth of the two senses in the taxonomy
and that of their Least Common Subsumer (most specific ancestor node).'''


class SemanticAnalyzer(object):
    def __init__(self):
        self.sum_similarity = 0.0
        self.check_count = 0

    def get_similarity(self, word1, word2, pos, type_):
        if type_ == 'res':
            similarity = self.resnik_similarity(word1, word2)
        elif type_=='lin':
            similarity = self.lin_similarity(word1, word2)
        elif type_ == 'path':
            similarity = self.path_similarity(word1, word2)
        elif type_=='wup':
            similarity = self.wup_similarity(word1,word2)
        else:
            raise NotImplementedError("Similarity of Type: {0} is not implemented.".format(type_))
        return similarity

    def path_similarity(self, word1, word2):
        ss1 = wn.synsets(word1)
        ss2 = wn.synsets(word2)
        try:
            return max(s1.path_similarity(s2) for (s1, s2) in product(ss1, ss2))
        except Exception:
            g.logger.error("problem with similarity:{0}".format(ss1), exc_info=True)
            return 0

    def wup_similarity(self, word1, word2):
        ss1 = wn.synsets(word1)
        ss2 = wn.synsets(word2)
        try:
            return max(s1.wup_similarity(s2) for (s1, s2) in product(ss1, ss2))
        except Exception:
            g.logger.error("wup similarity problem:{0} {1}".format(word1, word2))
            return 0

    def resnik_similarity(self, word1, word2):
        try:
            ss1 = wn.synsets(word1)
            ss2 = wn.synsets(word2)
            # s1 = wn.synset("{0}.{1}.01".format(word1, pos))
            # s2 = wn.synset("{0}.{1}.01".format(word2, pos))
            # return s1.res_similarity(s2, brown_ic)
            # return ss1.res_similarity(ss2, brown_ic)
            return max(s1.res_similarity(s2, semcor_ic) for (s1, s2) in product(ss1, ss2))
        except Exception:
            g.logger.error("similarity:{0}".format(traceback.format_exc()))
            return 0

    def lin_similarity(self, word1, word2):
        ss1 = wn.synsets(word1)
        ss2 = wn.synsets(word2)
        try:
            return max(s1.lin_similarity(s2, brown_ic) for (s1, s2) in product(ss1, ss2))
        except Exception:
            # g.logger.error("similarity:{0}".format(traceback.print_exc()))
            return 0


if __name__ == "__main__":
    sim = SemanticAnalyzer()

    s1 = wn.synset("{0}.{1}.01".format('appeal', 'n'))
    s2 = wn.synset("{0}.{1}.01".format('good', 'n'))
    print 'appeal', 'good'
    print sim.get_similarity('appeal', 'good', '', 'path')
    print "resnik", s1.res_similarity(s2, brown_ic)
    print "path", s1.path_similarity(s2)

    s1 = wn.synset("{0}.{1}.01".format('appeal', 'n'))
    s2 = wn.synset("{0}.{1}.01".format('dysentery', 'n'))
    print 'appeal', 'dysentery'
    print sim.get_similarity('appeal', 'dysentery', '', 'path')
    print "resnik", s1.res_similarity(s2, semcor_ic)
    print "path", s1.path_similarity(s2)