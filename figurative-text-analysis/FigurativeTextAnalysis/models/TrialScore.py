from FigurativeTextAnalysis.helpers.globals import g
__author__ = 'maria'


class TrialScore(object):
    """
        Gets a tweet_test_list and a predictions_list : one corresponding to other
        and stores results of Trial to TrialScores table
    """
    def __init__(self, tweet_test_list, predictions_list):
        self.tweet_test_list = tweet_test_list
        self.predictions_list = predictions_list
        self.insert_q = '''INSERT INTO SentiFeed.TrialScores (trial_id, tweet_id, predicted, initial_score)
        VALUES ("{0}", "{1}", "{2}", "{3}");'''

    def store_scores_for_trial(self, id):
        for tweet in self.tweet_test_list:
            q = self.insert_q.format(id, tweet.id, self.predictions_list[self.tweet_test_list.index(tweet)], tweet.initial_score)
            g.mysql_conn.update(q)

