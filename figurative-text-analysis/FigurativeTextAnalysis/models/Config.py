from FigurativeTextAnalysis.helpers.globals import g


class Config(object):
    def __init__(self):
        pass

    def set_defaults(self):
        pass


class TrialConfig(Config):
    def __init__(self, selected_features,
                 cls_type=g.CLASSIFIER_TYPE.SVMStandalone,
                 vec_type=g.VECTORIZER.Dict,
                 discretization=g.DISCRETIZATION.ONE,
                 ds=g.DS_TYPE.Final,
                 feedback=False):
        super(TrialConfig, self).__init__()
        self.train_file = ""
        self.test_file = ""
        self.selected_features = selected_features
        self.discretization = discretization
        self.ds = ds
        self.feedback = feedback
        self.post_process=True
        self.cls_cfg = ClsConfig()

    def __str__(self):
        return "".format()

    def set_defaults(self):
        pass


class ClsConfig(Config):
    def __init__(self, cls_type=g.CLASSIFIER_TYPE.SVMStandalone,
                    vec_type=g.VECTORIZER.Dict, use_tf=True,
                    use_idf=False, use_cosine=False,
                    use_hashing=False, use_k_best=False,
                    select_percent=0):
        super(ClsConfig, self).__init__()
        self.cls_type = cls_type
        self.vec_type = vec_type
        self.vec_opts = "original"
        self.use_tf = use_tf
        self.use_idf = use_idf
        self.use_cosine = use_cosine
        self.use_k_best = use_k_best
        self.select_percent = select_percent
        self.use_hashing = use_hashing
