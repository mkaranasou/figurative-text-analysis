import ast
import copy
import string

import nltk
from numpy.ma import transpose
import scipy
from scipy.spatial.distance import pdist
import sklearn
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer, HashingVectorizer
from sklearn.feature_selection import VarianceThreshold, chi2, f_classif, SelectPercentile
from sklearn.feature_selection import SelectKBest
from sklearn.linear_model import SGDClassifier, Perceptron
from sklearn.metrics.pairwise import linear_kernel
from sklearn.multiclass import OneVsRestClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.pipeline import make_pipeline
from sklearn.svm import NuSVC
from sklearn.tree import DecisionTreeClassifier
from FigurativeTextAnalysis.helpers.globals import g
from sklearn.ensemble import RandomForestRegressor
from nltk.metrics import precision, recall, f_measure
from sklearn.metrics import classification_report
# pairwise cosine= http://stackoverflow.com/questions/12118720/python-tf-idf-cosine-to-find-document-similarity
from FigurativeTextAnalysis.models.Exceptions import InvalidConfiguration

__author__ = 'maria'


class ClassifierBase(object):
    def __init__(self):
        pass

    def predict(self, *args):
        pass

    def save_model(self, *args):
        pass

    def load_model(self, *args):
        pass


class Classifier(ClassifierBase):
    """
    Wrapper class for scikit-learn classifiers.
    """
    def __init__(self, cls_config):
        super(Classifier, self).__init__()
        if cls_config is None:
            raise InvalidConfiguration("No classification configuration found!")
        self.cls_config = cls_config
        self.estimator = sklearn.svm.LinearSVC()
        self.cls_type = self.cls_config.cls_type
        self.cls = self._set_up_classifier(self.cls_config.cls_type)
        self.vectorizer_type = self.cls_config.vec_type
        self.vec = self._set_up_vectorizer(self.vectorizer_type)
        self.use_pairwise_cosine = self.cls_config.use_cosine
        self.use_tf_transformer = self.cls_config.use_tf
        self.x_train = None
        self.y = None
        self.x_test = None
        self.y_trial = None
        self.predictions = []
        self.x_train_copy = None
        self.tfidf_vectorizer = self._set_up_vectorizer(g.VECTORIZER.Idf)
        self.tfidf_transformer = TfidfTransformer(use_idf=self.cls_config.use_idf)
        self.count_vectorizer = self._set_up_vectorizer(g.VECTORIZER.Count)
        self.top_features = []
        self._non_coef_cls_types = [g.CLASSIFIER_TYPE.DecisionTree, g.CLASSIFIER_TYPE.RandomForestRegressor, g.CLASSIFIER_TYPE.SVR,
                                    g.CLASSIFIER_TYPE.SVMRbf]

    def _set_up_classifier(self, cls_type):
        """
        Given a type, return an instance
        :param cls_type:Enum
        :return:cls
        """
        if cls_type == g.CLASSIFIER_TYPE.NBayes:
            # SVM classification object model = GaussianNB()
            return MultinomialNB()
        elif cls_type == g.CLASSIFIER_TYPE.SVM:
            return OneVsRestClassifier(estimator=sklearn.svm.LinearSVC())
        elif cls_type == g.CLASSIFIER_TYPE.DecisionTree:
            return DecisionTreeClassifier(random_state=100)
        elif cls_type == g.CLASSIFIER_TYPE.SVMStandalone:
            return sklearn.svm.LinearSVC()  # , penalty="l1", dual=False
        elif cls_type == g.CLASSIFIER_TYPE.SVC:
            # Implementation of Support Vector Machine classifier using libsvm:
            # the kernel can be non-linear but its SMO algorithm does not scale
            # to large number of samples as LinearSVC does. Furthermore SVC multi-class mode
            #  is implemented using one vs one scheme while LinearSVC uses one vs the rest.
            # It is possible to implement one vs the rest with SVC by using the
            # sklearn.multiclass.OneVsRestClassifier wrapper. Finally SVC can fit dense data without memory copy
            # if the input is C-contiguous. Sparse data will still incur memory copy though.
            # return sklearn.svm.SVC(kernel='linear',C=1, gamma=0.001)
            return sklearn.svm.SVC(kernel='rbf',C=1, gamma=0.001)
        elif cls_type == g.CLASSIFIER_TYPE.SVMRbf:
            return sklearn.svm.SVC(kernel="rbf", C=1, gamma=0.001)
        elif cls_type == g.CLASSIFIER_TYPE.SVR:
            return sklearn.svm.SVR(kernel='linear', max_iter=500)  # >> 150 550
        elif cls_type == g.CLASSIFIER_TYPE.NuSVR:
            return sklearn.svm.NuSVR(kernel='linear', max_iter=10)
        elif cls_type == g.CLASSIFIER_TYPE.RandomForestRegressor:
            return RandomForestRegressor(n_estimators=2, max_depth=50)
        elif cls_type == g.CLASSIFIER_TYPE.SGD:
            # loss = Defaults to 'hinge', which gives a linear SVM.
            # The 'log' loss gives logistic regression, a probabilistic classifier.
            return SGDClassifier(loss='log', n_iter=150)
        elif cls_type == g.CLASSIFIER_TYPE.Perceptron:
            return Perceptron(penalty="l2", n_iter=150, n_jobs=1)
        else:
            raise NotImplementedError("The type of classifier: {0} is not implemented".format(cls_type))

    def _set_up_vectorizer(self, vectorizer_type):
        """
        Given a vectorizer type return an instance of the corresponding class
        :param vectorizer_type:Enum
        :return:cls instance
        """
        if vectorizer_type == g.VECTORIZER.Dict:
            return DictVectorizer(sparse=True)
        elif vectorizer_type == g.VECTORIZER.Count:
            return CountVectorizer(analyzer='word',
                                   input='content',
                                   lowercase=True,
                                   min_df=1,
                                   binary=False,
                                   stop_words='english'
                                   )
        elif vectorizer_type == g.VECTORIZER.Idf:
            return TfidfVectorizer(use_idf=True, stop_words='english')
        elif vectorizer_type == g.VECTORIZER.Tf:
            return TfidfVectorizer(use_idf=False, stop_words='english')
        else:
            raise NotImplementedError

    def set_x_train(self, train_list_of_dicts):
        """
        Apply the appropriate transformations to a list of feature sets, in order to create an input x_train
        for the classifier. The features list is transformed to a vector list.
        :param train_list_of_dicts:list
        :return:None
        """
        if type(self.vec) == CountVectorizer:
            self.x_train = self.vec.fit_transform([self.remove_non_ascii(str(a)) for a in train_list_of_dicts])
        else:
            self.x_train = self.vec.fit_transform(train_list_of_dicts)

        g.logger.debug(train_list_of_dicts[0])
        g.logger.debug(self.x_train[0:1])

        if self.use_tf_transformer:
            self.x_train = self.tfidf_transformer.fit_transform(self.x_train)

        if self.use_pairwise_cosine:
            self.x_train_copy = copy.copy(self.x_train)
            self.x_train = sklearn.metrics.pairwise.pairwise_distances(self.x_train[0:1], self.x_train, metric='cosine', n_jobs=1)

        # self.Xtrain = euclidean_distances(self.Xtrain)

        if not type(self.vec) is CountVectorizer and not type(self.vec) is TfidfVectorizer:
            g.logger.debug("feature_names: %s" % self.vec.feature_names_)
            g.logger.debug("vocabulary_: %s" % self.vec.vocabulary_)
        # g.logger.debug("Xtrain: %s" % self.x_train)

    def set_y_train(self, handcoded_score_list):
        """
        Set the known scores/labels of the tweets
        :param handcoded_score_list: list
        :return: None
        """
        self.y = handcoded_score_list
        g.logger.debug("Y:: %s" % self.y)

    def train(self):
        """
        Train the classifier
        :return:
        """
        if self.use_pairwise_cosine:
            self.cls.fit(transpose(self.x_train), self.y)    # transpose is used when cosine similarities are calculated
        else:
            self.cls.fit(self.x_train, self.y)

    def set_x_trial(self, trial_list_of_dicts):
        """
        Apply the appropriate transformations to a list of feature sets, in order to create an input x_train
        for the classifier. The features list is transformed to a vector list.
        :param train_list_of_dicts: list
        :return:None
        """
        if type(self.vec) == CountVectorizer:
            self.x_test = self.vec.transform([self.remove_non_ascii(str(a)) for a in trial_list_of_dicts])
        else:
            self.x_test = self.vec.transform(trial_list_of_dicts)

        if self.use_tf_transformer:
            self.x_test = self.tfidf_transformer.transform(self.x_test)

        if self.use_pairwise_cosine:
            self.x_test = sklearn.metrics.pairwise.pairwise_distances(self.x_train_copy[0:1],
                                                                      self.x_test,
                                                                      metric='cosine',
                                                                      n_jobs=-1)
        # print "after pairwise cosine", self.x_test[0]

        g.logger.debug("x_trial: %s" % self.x_test)

    def set_y_trial(self, trial_scores):
        """
        Set the expected scores/labels of tweets - used only in result evaluation
        :param trial_scores:
        :return:
        """
        self.y_trial = trial_scores

    def predict(self):
        """
        Predicts the labels for each tweet in x_test
        :return: list
        """
        if self.use_pairwise_cosine:
            self.predictions = self.cls.predict(transpose(self.x_test)).tolist()
        else:
            self.predictions = self.cls.predict(self.x_test).tolist()
        if not type(self.vec) is CountVectorizer and type(self.cls) is not sklearn.svm.SVR:
            print classification_report(self.y_trial, self.predictions)
        return self.predictions

    def get_metrics(self, vec):
        """
        Get metrics
        :param vec:
        :return:
        """
        self.get_most_informant_features(self.cls, vec)
        # self.sklearn_precision_score = sklearn.metrics.precision_score(self.YTrial,
        #                                                                self.predictions,
        #                                                                # labels=self.YTrial,
        #                                                                average='weighted')
        # print self.sklearn_precision_score

    def get_most_informant_features(self, classifier, vec, n=10):
        """
        If the classifier has the notion of coefficient, then get the features and the corresponding coefficients
        soreted by coefficients descending
        :param classifier:instance
        :param vec:instance
        :param n:int
        :return:None
        """
        print "\nIMPORTANT FEATURES:"
        print "======================"

        feature_names = vec.get_feature_names()
        if self.cls_type not in self._non_coef_cls_types and not\
                (self.cls_type==g.CLASSIFIER_TYPE.SVC and self.cls.kernel != "linear"):
            coefs_with_fns = sorted(zip(classifier.coef_[0], feature_names))
            top = zip(coefs_with_fns[:n], coefs_with_fns[:-(n + 1):-1])
            for (coef_1, fn_1), (coef_2, fn_2) in top:
                important_feature = "\t%.4f\t%-15s\n\t%.4f\t%-15s" % (coef_1, fn_1, coef_2, fn_2)
                print important_feature
                g.logger.debug("important_feature %s" % important_feature)
                self.top_features.append([coef_1, fn_1])
                self.top_features.append([coef_2, fn_2])

    def save_features(self, trial_id):
        """
        Save most important features.
        :param trial_id:int
        :return:None
        """
        for coef_1, fn_1 in self.top_features:
            q1 = self.save_q().format(trial_id, coef_1, fn_1)
            g.mysql_conn.execute_query(q1)

    @staticmethod
    def save_q():
        return '''
                    INSERT INTO `SentiFeed`.`TopFeatures`
                                    ( `trial_id`,
                                      `coefficient`,
                                      `feature`)
                                    VALUES
                                    ( {0},
                                      {1},
                                      '{2}' );

                '''

    def remove_non_ascii(self, text):
        """
        Remove non printable characters.
        TODO: duplicate function, move it somewhere to be visible from all classes that use it.
        :param text: string
        :return: string
        """
        return str(filter(lambda x: x in string.printable, text))
