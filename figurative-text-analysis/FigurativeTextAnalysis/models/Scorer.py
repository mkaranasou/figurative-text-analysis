import math

__author__ = 'maria'


class Scorer(object):
    def __init__(self, gold, predictions):
        self.gold = gold
        self.predictions = predictions
        self.final_score = 0.0
    def get_cosine(self):
        self.dict1 = self.get_data(self.gold)
        self.dict2 = self.get_data(self.predictions)
        self.results = self.cosine(self.dict1, self.dict2)
        self.final_score = self.results[1] * self.results[0]  # cos * ratio
        print('Cosine Similarity Score:: %0.4f\r\nPenalty:: %0.4f\r\nFinal Score:: %0.4f'
              % (self.results[1], (1 - self.results[0]), self.final_score))


    def get_data(self, data_list):
        dict = {};
        for row in data_list:
            id = int(row[0])
            label = float(row[1])
            if label > 5:
                print('Warning: %0.4f is out of range, the maximum value is 5. Skipping this input. ' % label);
                # label = 5;
                continue
            if label < -5:
                print('Warning: %0.4f is out of range, the minimum value is -5. Skipping this input.' % label);
                # label = -5
                continue
            if label != math.floor(label + 0.5):
                print('Warning: %0.4f is not an integer, will be converted to %d.' % (label, math.floor(label + 0.5)))
                label = math.floor(label + 0.5)
            if id in dict:
                print('duplicate tweet id: ' + str(id))
            else:
                dict[id] = label;
        return dict

    def cosine(self, dict1, dict2):

        keys1 = set(dict1.keys())
        keys2 = set(dict2.keys())
        keys_union = keys1 | keys2
        keys_intersection = keys1 & keys2
        # print('union len: ' + str(len(keys_union)) + '; intersection len: ' + str(len(keys_intersection)));
        print('Number of Gold entries:: %d\r\nNumber of Submitted entries:: %d' % (len(keys1), len(keys_intersection)))
        ratio = float(len(keys_intersection)) / len(keys1)
        sum_prod = 0
        mod1 = 0
        mod2 = 0
        for id in keys_intersection:
            sum_prod += dict1[id] * dict2[id]
            mod1 += dict1[id] * dict1[id]
            mod2 += dict2[id] * dict2[id]
        mod1 = math.sqrt(mod1)
        mod2 = math.sqrt(mod2)
        cos = sum_prod / (mod1 * mod2)
        return (ratio, cos)

    def cosine_(self):
        sum_prod = 0
        mod1 = 0
        mod2 = 0
        for i in range(0, len(self.gold)):
            sum_prod += self.gold[i] * self.predictions[i]
            mod1 += self.gold[i] * self.gold[i]
            mod2 += self.predictions[i] * self.predictions[i]
        mod1 = math.sqrt(mod1)
        mod2 = math.sqrt(mod2)

        return sum_prod / (mod1 * mod2)