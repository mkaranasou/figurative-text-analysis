import re
import datetime
from nltk.corpus import wordnet
from FigurativeTextAnalysis.helpers.pyenchant_spell_checker import EnchantSpellChecker
from FigurativeTextAnalysis.models.POSTagger import POSTagger
from FigurativeTextAnalysis.models.SemanticAnalyser import SemanticAnalyzer
from FigurativeTextAnalysis.models.TextCleaner import TextCleaner
from FigurativeTextAnalysis.models.TextTagger import TextTagger
from FigurativeTextAnalysis.helpers.globals import g

__author__ = 'maria'

class Tweet(object):
    """

    """
    def __init__(self, id_, text, train, clean_text=None, tagged_text=None, pos_tagged_text=None, tags=None, initial_score=None,
                 feature_dict=None, pnn=None, words=None, corrected_words=None):
        self.id = id_
        self.text = text
        self.clean_text = clean_text
        self.tagged_text = tagged_text if tagged_text is not None else ""
        self.pos_tagged_text = pos_tagged_text if (pos_tagged_text != "" or pos_tagged_text is not None) else {}
        self.tags = tags if tags is not None else {}
        self.initial_score = initial_score
        self.spellchecker = EnchantSpellChecker()
        self.spellchecker.dict_exists('en')
        self.sentences = []
        self.words = words if words is not None else []
        self.links = []
        self.corrected_words = corrected_words if corrected_words is not None else []
        self.smileys_per_sentence = []
        self.uppercase_words_per_sentence = []
        self.reference = []
        self.hash_list = []
        self.non_word_chars_removed = []
        self.negations = []
        self.swn_score_dict = {}
        self.swn_score = 0.0
        self.similarity = 0.0
        self.feature_dict = feature_dict if feature_dict is not None else {}
        self.pnn = pnn
        self.words_dict = {}
        self.words_to_swn_score_dict = {}
        self.train = train
        ######## Helpers #########
        self.tagger = TextTagger()
        self.pos_tagger = POSTagger()
        self.semantic_analyser = SemanticAnalyzer()

    def tag(self):
        """
        Feature Extraction process: Retrieve features that will be lost in cleaning process,
        such as CAPITALIZED words, punctuation etc.
        :return:
        """
        self.tagged_text = self.tagger.tag_text(self.text)
        self.tags = self.tagger.get_tags()

    def clean(self):
        """
        Cleaning Process: Remove unneccessary information from tweet_text
        :return:
        """
        # get stopwords from db
        stopwords = g.mysql_conn.execute_query(g.select_from_stop_words())
        # init text cleaner
        cleaner = TextCleaner(self, stopwords)
        clean_words = cleaner.clean_tweet()
        self.clean_text = ' '.join(clean_words)

    def spellcheck(self):
        """
        Spell check all words
        :return:
        """
        for word in self.words:
            if self.contains_errors(word):
                corrected = self.spellchecker.correct_word(word)
                self.words[self.words.index(word)] = corrected[0].replace("'", "\\'") if len(corrected) > 0 else word

    def pos_tag(self):
        """
        Perform Part-of-Speech tagging
        :return:None
        """
        self.pos_tagged_text = self.pos_tagger.pos_tag_tweet(self)
        g.logger.debug("POS-TAGS: %s" % self.pos_tagged_text)

    def get_simple_pos_for_swn(self, word):
        """
        Given a word, find in which major category [NOUN, VERBS, ADJECTIVES, ADVERBS] it belongs
        :param word:string
        :return:string
        """
        if self.pos_tagged_text[word] in g.NOUNS:
            return ' and Category = "n" '
        elif self.pos_tagged_text[word] in g.VERBS:
            return ' and Category = "v" '
        elif self.pos_tagged_text[word] in g.ADVERBS:
            return ' and Category = "r" '
        elif self.pos_tagged_text[word] in g.ADJECTIVES:
            return ' and Category = "a" '
        return ""

    def fix_words(self):
        """
        Perform stemming in all words
        :return:None
        """
        for word in self.words:
            self.words[self.words.index(word)] = self.pos_tagger.lancaster_stemmer.stem(word)

    def fix_pos_tagging(self):
        for each in self.pos_tagged_text.keys():
            temp = self.pos_tagged_text[each]
            stemmed = self.pos_tagger.lancaster_stemmer.stem(each)
            del self.pos_tagged_text[each]
            self.pos_tagged_text[stemmed] = temp

    def get_swn_score_by_rank(self):
        """
            Retrieves the SentiWordNet score for each word with more than one character length,
            taking synset ranking into account when calculating score
        """

        final = 0.0
        excluded = 0
        for word in self.words:
            rank = 0.0
            final_ = 0.0
            if len(word) > 1:
                word = word.encode('utf-8')
                try:
                    # first try get equals score with category
                    score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_equals_rank_test()
                                                        .format(word, self.get_simple_pos_for_swn(word)))

                except:
                    g.logger.error(g.sum_query_figurative_scale_equals_rank_test().format(word, self.get_simple_pos_for_swn(word)))
                    # if that failed, try get equals without category
                    score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_equals_rank_test().format(word, ""))
                    g.logger.debug("tried equals for {0} and got {1}".format(word, score))

                if len(score) == 0:
                    # if that failed, try get like with category
                    stemmed_word = self.pos_tagger.lancaster_stemmer.stem(word)
                    try:
                        score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like_rank_test()
                                                           .format(stemmed_word, self.get_simple_pos_for_swn(word)))
                        g.logger.debug("tried like for {0} and category got {1}".format(stemmed_word, score))
                    except:
                        if len(score) == 0:
                            # if that failed also, try get like without category
                            score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like_rank_test()
                                                               .format(stemmed_word, ""))
                            g.logger.debug("tried like for {0} and got {1}".format(stemmed_word, score))
                    if len(score) == 0:
                            # if that failed also, try get like without category
                            score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like_rank_test()
                                                               .format(stemmed_word, ""))
                            g.logger.debug("tried like for {0} and got {1}".format(stemmed_word, score))
                # if score[0] is not None and score[0] <= 2.0:
                if len(score) > 0:
                    for each in score:
                        rank_ = int(each[1])
                        final_ += (each[0]/rank_)
                        rank += float(1.0/rank_)

                    finalfinal = final_/rank
                    g.logger.debug(finalfinal)

                    self.swn_score_dict['s_word-'+str(self.words.index(word))] = round(finalfinal, 2)
                    final += finalfinal
                else:
                    self.swn_score_dict['s_word-'+str(self.words.index(word))] = 1
                    excluded += 1
            else:
                excluded += 1
        if self.words.__len__() > excluded and self.words > 0:
            self.swn_score = round(final/float(self.words.__len__()-excluded), 2)
        else:
            if len(self.words) > 0:
                self.swn_score = round(final/float(self.words.__len__()), 2)
            else:
                self.swn_score = 1.0  # neutral

        if self.tags[g.TAGS.name[g.TAGS.__NEGATION__]] or\
           self.tags[g.TAGS.name[g.TAGS.__AS_GROUND_AS_VEHICLE__]]:
            self.swn_score = self.swn_score - 1.0 if self.swn_score > 1 else self.swn_score * 0.5

        # if self.tags[g.TAGS.reverse_mapping[g.TAGS.POS_SMILEY]] or\
        # self.tags[g.TAGS.reverse_mapping[g.TAGS.HT_POS]]:
        #     self.swn_score = self.swn_score + 1.0 if self.swn_score < 1 else self.swn_score

    def get_swn_score(self):
        """
        Retrieves the SentiWordNet score for each word with more than one character length.
        :return:None
        """

        final = 0.0
        excluded = 0
        for word in self.words:
            word = word.encode('utf-8')
            if len(word) > 1:
                try:
                    # first try get equals score with category
                    score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_equals()
                                                        .format(word, self.get_simple_pos_for_swn(word)))
                except:
                    g.logger.error(g.sum_query_figurative_scale_equals().format(word, self.get_simple_pos_for_swn(word)))
                    # if that failed, try get equals without category
                    score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_equals().format(word, ""))
                    g.logger.debug("tried equals for {0} and got {1}".format(word, score[0][0]))

                if score[0][0] > 2.0:
                    # if that failed, try get like with category
                    # stemmed_word = self.pos_tagger.lancaster_stemmer.stem(word)
                    stemmed_word = word
                    try:
                        score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like()
                                                           .format(stemmed_word, self.get_simple_pos_for_swn(word)))
                        g.logger.debug("tried like for {0} and category got {1}".format(stemmed_word, score[0][0]))
                    except:
                        if score[0][0] > 2.0:
                            # if that failed also, try get like without category
                            score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like()
                                                               .format(stemmed_word, ""))
                            g.logger.debug("tried like for {0} and got {1}".format(stemmed_word, score[0][0]))
                if score[0][0] > 2.0:
                            # if that failed also, try get like without category
                            score = g.mysql_conn.execute_query(g.sum_query_figurative_scale_like()
                                                               .format(stemmed_word, ""))
                            g.logger.debug("tried like for {0} and got {1}".format(stemmed_word, score[0][0]))
                if score[0][0] is not None and score[0][0] <= 2.0:
                    self.swn_score_dict['s_word-'+str(self.words.index(word))] = round(score[0][0], 2)
                    final += score[0][0]
                else:
                    self.swn_score_dict['s_word-'+str(self.words.index(word))] = 1
                    excluded += 1
            else:
                excluded += 1
        if self.words.__len__() > excluded and self.words > 0:
            self.swn_score = round(final/float(self.words.__len__()-excluded), 2)
        else:
            if len(self.words) > 0:
                self.swn_score = round(final/float(self.words.__len__()), 2)
            else:
                self.swn_score = 1.0  # neutral

        if self.tags[g.TAGS.name[g.TAGS.__NEGATION__]] or\
           self.tags[g.TAGS.name[g.TAGS.__AS_GROUND_AS_VEHICLE__]]:
            self.swn_score = self.swn_score - 1.0 if self.swn_score > 1 else self.swn_score * 0.5
        #
        # if self.tags[g.TAGS.reverse_mapping[g.TAGS.POS_SMILEY]] or\
        # self.tags[g.TAGS.reverse_mapping[g.TAGS.HT_POS]]:
        #     self.swn_score = self.swn_score + 1.0 if self.swn_score < 1 else self.swn_score

    def get_semantic_similarity(self, type_):
        """
        Split tweet words into four major POS categories: NOUNS, VERBS, ADJ, ADV
        and then for each couple, get the semantic text similarity of the type_ that is requested
        :param type_:string that indicates either 'lin' or 'wup' or 'path' or 'resnik'
        :return: decimal score that indicates the average tweet semantic similarity
        """
        similarity = 0.0
        nouns = []
        verbs = []
        adj = []
        adv = []
        for each in self.pos_tagged_text.items():
            if each[1] in g.NOUNS:
                nouns.append((each[0], wordnet.NOUN))
            elif each[1] in g.VERBS:
                verbs.append((each[0], wordnet.VERB))
            elif each[1] in g.ADJECTIVES:
                adj.append((each[0], wordnet.ADJ))
            elif each[1] in g.ADVERBS:
                adv.append((each[0], wordnet.ADV))

        final_list = [nouns, verbs, adj, adv]
        length = 0.0
        for each in final_list:
            if len(each) > 1:
                for i in range(1, len(each)):
                    temp_similarity = self.semantic_analyser.get_similarity(each[i - 1][0],
                                                                            each[i][0],
                                                                            each[i][1], type_)
                    if temp_similarity is not None:
                        similarity += temp_similarity
                        length += 1.0

        if length > 0:
            g.logger.debug("length %s" % length)
            self.similarity = float(similarity/length)
        else:
            g.logger.debug("length 0 similarity %s" % similarity)
            self.similarity = 1.0
        return self.similarity

    def get_words_to_swn_score_dict(self):
        """
        'someword' : 'positive', 'someotherword' : 'negative' ...
        :return:
        """
        for word in self.words:
            lemmatized_word = self.pos_tagger.lemmatizer.lemmatize(word, self.pos_tagged_text[word])
            self.words_to_swn_score_dict[lemmatized_word] = self.swn_score_dict['s_word-'+str(self.words.index(word))]

    def get_words_dict(self):
        for word in self.words:
            self.words_dict['word-'+str(self.words.index(word))] = word
        return self.words_dict

    def gather_dicts(self):
        """
        Get all neccessary information into feature_dict
        :return: None
        """
        self.feature_dict = dict(self.swn_score_dict.items() +
                                 self.tags.items() +
                                 self.pos_tagged_text.items() +
                                 self.get_words_dict().items() +
                                 self.words_to_swn_score_dict.items())
        self.feature_dict["t-similarity"] = self.similarity
        self.feature_dict["swn_score"] = self.swn_score

    def contains_errors(self, word):
        """
        Uses PyEnchant spellchecker to tell if a word contains spelling mistakes or not
        :param word: string: word to be examined for errors
        :return:True if word contains errors False otherwise
        """
        return self.spellchecker.spell_checker_for_word(word) is not None
        
    def insert(self):
        q = g.insert_in_tweet_bo.format(self.id,
                                str(self.text).replace("\"", '\\"'),
                                str(self.initial_score).replace("\"", '\\"'),
                                str(self.clean_text).replace("\"", '\\"'),
                                str(self.tagged_text).replace("\"", '\\"'),
                                str(self.pos_tagged_text).replace("\"", '\\"'),
                                str(self.tags).replace("\"", '\\"'),
                                str(self.sentences).replace("\"", '\\"'),
                                str(self.words).replace("\"", '\\"'),
                                str(self.links).replace("\"", '\\"'),
                                str(self.corrected_words).replace("\"", '\\"'),
                                str(self.smileys_per_sentence).replace("\"", '\\"'),
                                str(self.uppercase_words_per_sentence).replace("\"", '\\"'),
                                str(self.reference).replace("\"", '\\"'),
                                str(self.hash_list).replace("\"", '\\"'),
                                str(self.non_word_chars_removed).replace("\"", '\\"'),
                                str(self.negations).replace("\"", '\\"'),
                                str(self.swn_score_dict).replace("\"", '\\"'),
                                self.swn_score,
                                str(self.feature_dict).replace("\"", '\\"'), 0,
                                self.train)
        g.logger.debug(q)
        try:
            g.mysql_conn.update(q)
        except:
            g.logger.error("Q:"+q)

    def update(self):
        qi = g.update_tweet_bo().format(
                        str(self.clean_text).replace("\"", "'"),
                        str(self.tagged_text).replace("\"", "'"),
                        str(self.pos_tagged_text).replace("\"", "'"),
                        str(self.tags).replace("\"", "'"),
                        str(self.initial_score).replace("\"", "'"),
                        str(self.sentences).replace("\"", "'"),
                        str(self.words).replace("\"", "'"),
                        str(self.links).replace("\"", "'"),
                        str(self.corrected_words).replace("\"", "'"),
                        str(self.smileys_per_sentence).replace("\"", "'"),
                        str(self.uppercase_words_per_sentence).replace("\"", "'"),
                        str(self.reference).replace("\"", "'"),
                        str(self.hash_list).replace("\"", "'"),
                        str(self.non_word_chars_removed).replace("\"", "'"),
                        str(self.negations).replace("\"", "'"),
                        str(self.swn_score_dict).replace("\"", "'"),
                        self.swn_score,
                        str(self.feature_dict).replace("\"", "'"),
                        0,
                        self.train,
                        datetime.datetime.now(),
                        str(self.words_to_swn_score_dict).replace("\"", "'"),
                        self.id, self.table)
        try:
            g.mysql_conn.execute_query(qi)
            g.logger.debug("Qi:"+qi)
        except:
            g.logger.error("error in Qi:"+qi)