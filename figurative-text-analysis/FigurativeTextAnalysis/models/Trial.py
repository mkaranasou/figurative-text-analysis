from datetime import datetime
import nltk

from FigurativeTextAnalysis.models.BasicMeasures import BasicMeasures
from FigurativeTextAnalysis.models.Config import TrialConfig, ClsConfig
from FigurativeTextAnalysis.models.Scorer import Scorer
from FigurativeTextAnalysis.models.SelectedFeatures import SelectedFeatures
from FigurativeTextAnalysis.processors.TweetProcessor import TweetProcessor
from FigurativeTextAnalysis.helpers.globals import g
from TrialScore import TrialScore

__author__ = 'maria'


class Trial(object):
    """
    The main class that represents and controls an experiment.
    """
    def __init__(self, trial_config):
        self.trial_config = trial_config
        self.tweet_processor = TweetProcessor(self.trial_config)
        self._id = None
        self._basic_measures_id = None
        self.discretization = self.trial_config.discretization
        self.selected_features = SelectedFeatures(self.trial_config.selected_features)
        self.selected_features_id = None
        self.classifier = self.trial_config.cls_cfg.cls_type
        self.basic_measures = None
        self.predictions = []
        self.x_train = []
        self.y_train = []
        self.x_test = []
        self.y_test = []
        self.trial_set_len = 0
        self.test_set_len = 0
        self.match = 0.0
        self.accuracy = 0.0
        self.precision = None
        self.recall = None
        self.f_measure = None
        self.cosine_similarity = None
        self.labels_range = None
        self.date_created = datetime.today()

    @staticmethod
    def insert_q():
            return '''INSERT INTO SentiFeed.Trial
            ( selected_features_id, basic_measures_id, discretization, classifier, vectorizer, dataset,
              trial_set_len, test_set_len, labels_range, date_created)
            VALUES
            ("{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", "{7}", "{8}", "{9}");'''    \

    @staticmethod
    def insert_basic_measures_q():
            return '''INSERT INTO `SentiFeed`.`BasicMeasures`
                        (`accuracy`,
                        `precision`,
                        `recall`,
                        `f_score`,
                        `cosine_similarity`,
                        `mse`,
                        `classification_report`)
                        VALUES
                        (
                          {0},
                          {1},
                          {2},
                          {3},
                          {4},
                          {5},
                          {6});
                    '''

    def classify(self):
        """
        The process of classification.
        1. Get data (train and test)
        2. Classify
        :return:
        """
        self.tweet_processor.get_train_set_from_db()
        self.tweet_processor.get_test_set_from_db()
        self.tweet_processor.classify()

    def preprocess(self):
        """
        The pre-processing of tweets process:
        1. Get data (train and test)
        2. Pre-process and save (train and test)
        :return:
        """
        self.tweet_processor.get_train_set_from_db()
        self.tweet_processor.get_test_set_from_db()
        self.tweet_processor.process_tweet_set(self.tweet_processor.train_set)
        self.tweet_processor.process_tweet_set(self.tweet_processor.test_set)

    def save_results(self):
        """
        Gets metrics and data sets and saves Trial and Score results
        :return:
        :rtype:
        """
        # get measures
        self._gather_basic_data()

        # save selected features and keep id
        self.selected_features_id = self.selected_features.save()
        self._insert_basic_measures()
        # save Trial
        q = self.insert_q().format(self.selected_features_id,
                                   self._basic_measures_id,
                                   self.discretization,
                                   g.CLASSIFIER_TYPE.name[self.classifier],
                                   g.VECTORIZER.name[self.trial_config.cls_cfg.vec_type],
                                   g.DS_TYPE.name[self.trial_config.ds],
                                   self.trial_set_len,
                                   self.test_set_len,
                                   self.labels_range,
                                   self.date_created)
        self._id = g.mysql_conn.update(q)
        self.basic_measures.save_classification_report(self._id)
        self.tweet_processor.classifier.save_features(self._id)
        # Save individual scores
        trial_score = TrialScore(self.tweet_processor.test_set, self.predictions)
        trial_score.store_scores_for_trial(self._id)

    def _insert_basic_measures(self):
        if self.trial_config.discretization !=1.0:
            # round scores
            self.basic_measures = BasicMeasures([round(x, 0) for x in self.predictions], [round(x, 0) for x in self.y_test])
        else:
            self.basic_measures = BasicMeasures(self.predictions, self.y_test)
        self._basic_measures_id = self.basic_measures.save()

    def _calculate_cosine_similarity(self, test_set, predictions):
        """
        Given the test_set and the corresponding predictions, cosine similarity is calculated using Scorer.
        :param test_set: The list of TweetBO's provided to the chosen classifier for prediction.
        :type test_set: list of TweetBO objects
        :param predictions: The classifier's results (correspond one by one with tweets in test_set)
        :type predictions: list of float numbers
        :return: total cosine similarity
        :rtype: float
        """
        gold_list = []
        predictions_list = []
        for tweet in test_set:
            gold_list.append([tweet.id, round(tweet.initial_score, 0)])
            if type(predictions[0]) == str:
                i = test_set.index(tweet)
                predictions_list.append([tweet.id, round(((self.tweet_processor.discrete_labels[predictions[i]][0] +
                                                    self.tweet_processor.discrete_labels[predictions[i]][1])/2.0), 0)])

            else:
                predictions_list.append([tweet.id, predictions[test_set.index(tweet)]])
        scorer = Scorer(gold_list, predictions_list)
        scorer.get_cosine()
        return scorer.final_score

    def _calculate_match(self):
        """
        :return: Percentage of correct predictions
        :rtype: float
        """
        if len(self.y_test) > 0:
            return float(self.match) / float(len(self.y_test))
        return 0

    def _gather_basic_data(self):
        """
        Get cosine similarity, accuracy, precision, recall, f_score
        :return:
        """
        self.predictions = self.tweet_processor.predictions
        self.x_train = self.tweet_processor.feature_list_train
        self.x_test = self.tweet_processor.feature_list_test
        self.y_train = self.tweet_processor.score_list_train
        self.y_test = self.tweet_processor.score_list_test
        self.trial_set_len = len(self.tweet_processor.train_set)
        self.test_set_len = len(self.tweet_processor.test_set)
        self.match = self._calculate_match()
        self.labels_range = str(self.tweet_processor.discrete_labels).replace("'", "\\'")
        self.cosine_similarity = self._calculate_cosine_similarity(self.tweet_processor.test_set, self.predictions)
        self.accuracy = nltk.metrics.accuracy(self.y_test, self.predictions)
        print "accuracy", self.accuracy
        print self.tweet_processor.classifier.get_most_informant_features(self.tweet_processor.classifier.cls,
                                                                          self.tweet_processor.classifier.vec)

# ============================================== TEST ================================================================ #


if __name__ == "__main__":
    # * : final combination of features
    selected_features =[
                        '__OH_SO__',                  # * // <<   +
                        '__DONT_YOU__',               # * // <<   +
                        '__AS_GROUND_AS_VEHICLE__',   # * // <<   +
                        '__CAPITAL__',                # * // <<   +
                        '__HT__',                     # * <<      +
                        '__HT_POS__',                 # * // <<   +
                        '__HT_NEG__',                 # * // <<   +
                        # # '__LINK__',               # //
                        '__POS_SMILEY__',             # * // << +
                        '__NEG_SMILEY__',             # * // << +
                        '__NEGATION__',               # * // << +
                        '__REFERENCE__',            # * // << +
                        '__questionmark__',           # * // << +
                        '__exclamation__',            # * // << +
                        # '__fullstop__',
                        # # # '__RT__',                 # //
                        # # # '__LAUGH__',              # //
                        '__postags__',              # * // << +
                        # # # '__words__',              #
                        # '__swn_score__',
                        '__s_word__',               # * // << +
                        '__res__',                  # * <<
                        # '__lin__',
                        # '__wup__',
                        #  '__path__',               # //
                        # '__contains_'
                        ]
    trial_config = TrialConfig(selected_features, ds=g.DS_TYPE.Test)
    trial_config.discretization = g.DISCRETIZATION.ONE
    trial_config.post_process=True
    trial_config.cls_cfg.cls_type = g.CLASSIFIER_TYPE.SVMStandalone
    trial_config.cls_cfg.vec_type = g.VECTORIZER.Dict
    trial_config.cls_cfg.use_cosine = False
    trial_config.cls_cfg.use_tf = True
    trial = Trial(trial_config)
    trial.classify()
    trial.save_results()