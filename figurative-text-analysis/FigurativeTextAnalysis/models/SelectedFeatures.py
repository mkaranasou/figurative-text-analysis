import ast

__author__ = 'maria'

from FigurativeTextAnalysis.helpers.globals import g

class SelectedFeatures(object):
    _selected_features = {
        '__OH_SO__': 0,
        '__DONT_YOU__': 0,
        '__AS_GROUND_AS_VEHICLE__': 0,
        '__CAPITAL__': 0,
        '__HT__': 0,
        '__HT_POS__': 0,
        '__HT_NEG__': 0,
        '__LINK__': 0,
        '__POS_SMILEY__': 0,
        '__NEG_SMILEY__': 0,
        '__NEGATION__': 0,
        '__REFERENCE__': 0,
        '__questionmark__': 0,
        '__fullstop__': 0,
        '__exclamation__': 0,
        '__polarity__': 0,
        '__RT__': 0,
        '__LAUGH__': 0,
        '__LOVE__': 0,
        '__postags__': 0,
        '__words__': 0,
        '__swn_score__': 0,
        '__s_word__': 0,
        '__t_similarity__': 0,
        '__res__': 0,
        '__lin__': 0,
        '__wup__': 0,
        '__path__': 0,
        '__contains_': 0
    }
    selected_features_init = ['__OH_SO__', '__DONT_YOU__', '__AS_GROUND_AS_VEHICLE__', '__CAPITAL__', '__HT__', '__HT_POS__', '__HT_NEG__', '__LINK__',
                     '__POS_SMILEY__', '__NEG_SMILEY__', '__NEGATION__', '__REFERENCE__', '__questionmark__', '__exclamation__', '__fullstop__',
                     '__polarity__', '__RT__', '__LAUGH__', '__LOVE__',  '__postags__', '__words__', '__swn_score__',  '__s_word__', '__t_similarity__',
                     '__res__', '__lin__', '__wup__', '__path__', '__contains_']

    def __init__(self, list_of_selected_features):
        self.selected_features = list_of_selected_features
        self.insert_q = '''INSERT INTO SentiFeed.SelectedFeatures
        ( OH_SO, DONT_YOU, AS_GROUND_AS_VEHICLE, CAPITAL, HT, HT_POS, HT_NEG, LINK, POS_SMILEY,
        NEG_SMILEY, NEGATION, REFERENCE, questionmark, exclamation, fullstop, polarity, RT, LAUGH, LOVE, postags, words,
        swn_score, s_word, t_similarity, res, lin, wup, path, contains_)
        VALUES
        ("{__OH_SO__}", "{__DONT_YOU__}","{__AS_GROUND_AS_VEHICLE__}","{__CAPITAL__}","{__HT__}","{__HT_POS__}","{__HT_NEG__}","{__LINK__}",
        "{__POS_SMILEY__}","{__NEG_SMILEY__}","{__NEGATION__}","{__REFERENCE__}","{__questionmark__}","{__exclamation__}","{__fullstop__}",
        "{__polarity__}","{__RT__}","{__LAUGH__}", "{__LOVE__}", "{__postags__}", "{__words__}", "{__swn_score__}", "{__s_word__}", "{__t_similarity__}",
        "{__res__}", "{__lin__}", "{__wup__}", "{__path__}", "{__contains_}");
        '''
        # ("{0}", "{1}","{2}","{3}","{4}","{5}","{6}","{7}",
        #     "{8}","{9}","{10}","{11}","{12}","{13}","{14}",
        # "{15}","{16}","{17}", "{18}", "{19}", "{20}", "{21}", "{22}", "{23}");'''


    def save(self):
        q = self.insert_q
        a = '{'
        for each in self.selected_features_init:
            if each in self.selected_features:
                # v = exec("%s = %s" % (each, 1))
                a += "'" + each + '\':1, '
            else:
                a += "'" + each + '\':0, '

        a += "}"
        lastrow = g.mysql_conn.update(q.format(**ast.literal_eval(a)))
        return lastrow
