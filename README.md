# README #

### What is this repository for? ###

* Msc Thesis Repository - "Network Oriented Systems" - University of Piraeus
* SemEval 2015 Task 11

#### More details ####
http://alt.qcri.org/semeval2015/cdrom/pdf/SemEval120.pdf

### Modules break-down ###

* FigurativeTextAnalysis: The system built for SemEval 2015 Task 11
* TweetUtils: The library built for tweet feature extraction and cleaning for my Thesis
* ui: a Django UI for representation of system results (Figurative Text Analysis)

### How do I get set up? ###

* Summary of set up, Database configuration, General Configuration is in Source: `figurative-text-analysis/FigurativeTextAnalysis/documentation/HOWTO.txt`
* Dependencies: libraries used along with some info about installation are in Source`figurative-text-analysis/FigurativeTextAnalysis/documentation/requirements.txt`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Karanasou Maria: karanasou@gmail.com